# Progetto MLG per esame Reti di Calcolatori
Il progetto è stato realizzato da 
*Gioele Zacchia*
*Mila Allerhand*
*Lorenzo Saba*
Per il corso di Reti di Calcolatori

Il progetto realizzato è un applicazione web che permette, una volta autenticati, di accedere al database di libri di google books che l'utente potrà usare per aggiungerli ai propri preferiti
Grazie all'integrazione con i social network di Facebook e Twitter l'applicazione permette di visualizzare i preferiti degli amici così da poter scoprire nuove letture.
E' inoltre presente una chat tematica che gli utenti posso utilizzare.

La documentazione tecnica è presenta nella [wiki del repository](https://gitlab.com/reti-mlg/reti/-/wikis/home)

Il metodo più semplice per provare l'applicazione è utilizzare la versione live caricata su un server publico [https://progettoreti.zaxnet.it](https://progettoreti.zaxnet.it)

## Utilizzo

Il progetto può essere lanciato in locale con l'utilizzo di docker compose oppure acceduto direttamente ad una versione live [https://progettoreti.zaxnet.it](https://progettoreti.zaxnet.it)

Per eseguire il progetto in locale
- Clonare il repository 
    ```
        git clone https://gitlab.com/reti-mlg/reti.git
        cd reti
    ```
- Modificare il file `docker-compose.yaml` per configurare il server 
 - Le righe 51,52,54,55 e 59 Vanno necessariamente configurate con le API key di Facebook, Twitter e Google necessarie per il funzionamento dell'app
    ```
      - FB_APP_ID=<FB_APP_ID>
      - FB_SECRET=<FB_SECRET>

      - TWITTER_CONSUMER_KEY=<TWITTER_CONSUMER_KEY>
      - TWITTER_CONSUMER_SECRET=<TWITTER_CONSUMER_SECRET>
      - TWITTER_CALLBACK_URI=http://localhost/twitterLanding
      - TWITTER_CONNECT_CALLBACK_URI=http://localhost/twitterLandingConnect

      - GOOGLE_API_KEY=<GOOGLE_API_KEY>
    ```
 - Modificare le altre righe di configurazione in base alle necessità (Per maggiore info guardare il file `backend/README.md`)
- Modificare il file `frontend/src/Config.ts`
 - La riga 26 Va modificata con l'api key per facebook, da questo file è anche possibile configurare l'URL del server backend ed gli indirizzi di callback per il processo OAUTH di facebook
- Avviare l'applicazione lanciando `docker compose up --build`

### Uso con HTTPS
 Per configurare l'applicazione per utilizzare il HTTPS eseguire le segeunti operazioni
 - Sostituire il file `docker-compose.yaml` con il file `docker-compose.tls.yaml`
 - Sostituire il file `frontend/nginx.conf` con il file `frontend/nginx-tls.conf`
 - Eseguire le operazioni di configurazioni del backend e frontend descritte sopra, avendo cura di impostare gli indirizzi del server e dei callback con gli indirizzi dell'host su cui è installato il server.
 - Modificare il file `frontend/nginx.conf` impostando il valore di `server_name` sul corretto nome DNS del server
 - Nel root del progetto (La cartella dove si trova il file docker compose) creare una cartella `data` e all'interno di essa una cartella `certs`
 - Di default il server andrà a cercare due file chiamati `fullchain.pem` e `privkey.pem` all'interno di questa cartella e li utilizzerà rispettivamente come certificato del server e chiave privata del certificato
 - E' possibile modificare il path e i nomi dove cercare i file configurando opportunamento il docker-compose.yaml e il file di configurazione di nginx nginx.conf
 - E' ora possibile avviare il server lanciando `docker compose up --build`

## Docs
Project documentation is avelable in the GitLab wiki

To update the project wiki follow the following steps
- *Clone* wiki repo to root folder `git clone https://gitlab.com/reti-mlg/reti.wiki.git` OR if already cloned *pull* any changes to the repo
- reti.wiki folder should be in the same folder as `backend` and `frontend`
- Compile backend docs by running `npm run doc` from backend folder
- reti.wiki git repo can then be pushed to gitlab to update the wiki
- The doc creates a page for each api group but it does not update the index. If creating a new api group update il manually by adding a link to the new file created
- Looks like images can be embeded in the wiki fairly easly. To comply with gitlab automatic file upload create for each image create a subfolder of the `uploads` folder. Add the image in the directory and embed it in the markdown file using something like this `![NOME FOTO](uploads/PATH/TO/FOTO.jpg)`
- Make sure to `PULL COMMIT PUSH` at every modification since i'm not sure how pull conflicts will be dealt in the wiki repo
- The wiki can also be edited from the gitlab editor which is nice. Don't modify any files in the doc/ directory since they will get overwritten by apidoc

This is realy junky, let's hope it does not brake :)
