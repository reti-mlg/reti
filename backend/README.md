# Backend server
## NPM Commands
`npm install` Install dependency  
`npm start` Start development mode server (It will automaticly reload the server upon modification of src/... files)
`npm run build` Compile typescirpt to javascript (Best practice is to compile before git commit)


`npm install --production` Install only the dependency that are needed for production run
`npm run prodRun` Run the server from the production build

To setup test DB using docker launch the following command
`docker run --name postgres-mlg -p5432:5432 -e POSTGRES_PASSWORD=mysecretpassword -e POSTGRES_USER=mlguser -e POSTGRES_DB=mlgdb -d postgres`

to setup the rabbitmq server using docker launch the following command
`docker run -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3-management`


## Enviroment variables
- *PORT* Port on witch the server listens to (Default 8080)
- *VERSION* Version returned by the _/_ endpoint (0.1)
- *DB_URL* Url db (Default postgres://mlguser:mysecretpassword@localhost:5432/mlgdb)
- *DB_SSL* If set db connection will be secure (Default: do not set)
- *DB_SYNC* If set db connection will syncronize db table (Default: true)
- *DB_LOG* If set will enable db query loggin (Will spam the console realy hard) (Default: do not set)
- *JWT_SECRET* Secret key for JWT signing (Can be generated with require("crypto").randomBytes(30).toString("hex") on node console) 
- *JWT_TTL* JWT expiration time in seconds, if not defined token will not expire (Default: 1800)
- *FB_APP_ID* FB application id as gathered from the console
- *FB_SECRET* FB application secret
- *FB_REDIRECT_URI* Redirect uri for FB login (Default: http://localhost:3000/fbLanding)
- *FB_CONNECT_REDIRECT_URI* Redirect uri for FB connect (Default: http://localhost:3000/fbLandingConnect)
- *TWITTER_CONSUMER_KEY* Twitter consumer key as gathered from the console
- *TWITTER_CONSUMER_SECRET* Twitter consumer secret
- *TWITTER_CALLBACK_URI* Redirect uri for Twitter login (Default: http://localhost:3000/twitterLanding)
- *TWITTER_CONNECT_CALLBACK_URI* Redirect uri for Twitter connect (Default: http://localhost:3000/twitterLandingConnect)
- *IMAGES_DIR* Folder for storing uploaded images, needs to be persistent (Default: ./images/)
- *MAX_FILE_SIZE* Max allowed image size (Deafult 10485760)
  
- *CHAT_STARTING_PORT* Starting port for the websockets used for the chat (Default 10000)
- *CHAT_SECTIONS* Chatrooms to set up, (for example GLOBAL,FANTASY,CLASSIC)
- *CHAT_SERVER_HOST* Server hostname for websocket 
- *MAX_CLIENT_CONNECTED_IN_SOCKET* this limits the number of clients a websocket can be connected to
- *NUMBER_OF_OPEN_SERVERS* Number of websocket servers to instantiate (Default 2)
- *RABBITMQ_SERVER_URL* Url used to connect to the rabbitmq server (Default amqp://localhost)

- *GOOGLE_API_KEY* Api key for google books

- *USE_HTTPS* True to enable HTTPS
- *HTTPS_CERT_FILE* Path to https certificate
- *HTTPS_KEY_FILE* Path to https certificate key