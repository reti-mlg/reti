"use strict";
/*
 * Created on Wed May 12 2021
 *
 * Author: Gioele Zacchia
 */
var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var dotenv_1 = __importDefault(require("dotenv"));
var morgan_1 = __importDefault(require("morgan"));
var cors_1 = __importDefault(require("cors"));
var applicationContext_1 = require("./applicationContext");
var authTest_1 = require("./endpoint/test/authTest");
var errorHandlers_1 = require("./errors/errorHandlers");
var epAuthEndpoint_1 = require("./endpoint/auth/epAuthEndpoint");
var socialLoginEndpoints_1 = require("./endpoint/auth/socialLoginEndpoints");
var userEndpoint_1 = require("./endpoint/user/userEndpoint");
var chat_1 = require("./endpoint/chat/chat");
var fs_1 = __importDefault(require("fs"));
var bookEndpoint_1 = require("./endpoint/book/bookEndpoint");
var ConsoleColorEscape;
(function (ConsoleColorEscape) {
    ConsoleColorEscape["RESET"] = "\u001B[0m";
    ConsoleColorEscape["BLACK"] = "\u001B[30m";
    ConsoleColorEscape["YELLOW"] = "\u001B[33m";
    ConsoleColorEscape["BLUE"] = "\u001B[34m";
    ConsoleColorEscape["MAGENTA"] = "\u001B[35m";
    ConsoleColorEscape["RED"] = "\u001B[31m";
    ConsoleColorEscape["GREEN"] = "\u001B[32m";
    ConsoleColorEscape["CYAN"] = "\u001B[36m";
    ConsoleColorEscape["WHITE"] = "\u001B[37m";
})(ConsoleColorEscape || (ConsoleColorEscape = {}));
dotenv_1.default.config();
process.env.NODE_ENV || 'development';
var logFormat = ConsoleColorEscape.BLUE + ":date " + ConsoleColorEscape.MAGENTA + ":method " + ConsoleColorEscape.RESET + ":url --" + ConsoleColorEscape.MAGENTA + " :status " + ConsoleColorEscape.RESET + " Total time: :total-time ms";
//Add endpoints register here
var registers = [
    //    ExampleEndpointRegister,
    authTest_1.AuthTestEndpointProvider,
    epAuthEndpoint_1.EPAuthEndpointRegister,
    socialLoginEndpoints_1.SocialAuthEndpointRegister,
    userEndpoint_1.UserEndpointRegister,
    chat_1.ChatEndPoint,
    bookEndpoint_1.BookEndpointRegister
];
applicationContext_1.ProvideContext().then(function () {
    var e_1, _a;
    var app = express_1.default();
    var port = process.env.PORT || 8080;
    app.use(express_1.default.json());
    app.use(cors_1.default());
    app.use(morgan_1.default(logFormat));
    if (!fs_1.default.existsSync(process.env.IMAGES_DIR)) {
        fs_1.default.mkdirSync(process.env.IMAGES_DIR);
    }
    app.use("/static", express_1.default.static(process.env.IMAGES_DIR));
    try {
        for (var registers_1 = __values(registers), registers_1_1 = registers_1.next(); !registers_1_1.done; registers_1_1 = registers_1.next()) {
            var register = registers_1_1.value;
            console.log("Registring endpoint: ", register);
            register(app);
        }
    }
    catch (e_1_1) { e_1 = { error: e_1_1 }; }
    finally {
        try {
            if (registers_1_1 && !registers_1_1.done && (_a = registers_1.return)) _a.call(registers_1);
        }
        finally { if (e_1) throw e_1.error; }
    }
    app.use(errorHandlers_1.AppErrorHandler);
    app.listen(port, function () {
        console.log("Server listening at " + ConsoleColorEscape.GREEN + "http://localhost:" + port + " \uD83D\uDE0E" + ConsoleColorEscape.RESET);
    });
});
//# sourceMappingURL=app.js.map