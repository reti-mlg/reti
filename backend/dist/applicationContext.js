"use strict";
/*
 * Created on Wed May 12 2021
 *
 * Author: Gioele Zacchia
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProvideContext = exports.BookService = exports.TwitterService = exports.FBService = exports.UserService = exports.AuthService = exports.JWTService = exports.TwitterCredentialsRepository = exports.PreferitoRepository = exports.CommentoRepository = exports.UserRepository = exports.PersistenceProvider = exports.ExampleNestedService = exports.ExampleService = void 0;
var commento_entity_1 = require("./persistence/entities/commento.entity");
var preferito_entity_1 = require("./persistence/entities/preferito.entity");
var twitterCreds_entity_1 = require("./persistence/entities/twitterCreds.entity");
var user_entity_1 = require("./persistence/entities/user.entity");
var Persistence_1 = require("./persistence/Persistence");
var AuthService_1 = require("./service/AuthService");
var BookService_1 = require("./service/BookService");
var FBService_1 = require("./service/FBService");
var JWTService_1 = require("./service/JWTService");
var TwitterService_1 = require("./service/TwitterService");
var UserService_1 = require("./service/UserService");
var ProvideContext = function () { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                //We initialize each dependency explicitly
                //Sequence is important, if service B depends on service A then A needs to be initialized before B
                //    console.log("Initilizing Example Service...")
                //    ExampleService = await _ExampleService.getService()
                //    console.log("Initilizing Example Nested Service...")
                //    ExampleNestedService = await _ExampleNestedService.getService()
                console.log("Initilizing Persistence level dependencies...");
                return [4 /*yield*/, Persistence_1._PersistenceProvider.getService(Persistence_1._PersistenceProvider.getConfigFromEnv())];
            case 1:
                exports.PersistenceProvider = _a.sent();
                exports.UserRepository = exports.PersistenceProvider.getConnection().getRepository(user_entity_1.User);
                exports.CommentoRepository = exports.PersistenceProvider.getConnection().getRepository(commento_entity_1.Commento);
                exports.PreferitoRepository = exports.PersistenceProvider.getConnection().getRepository(preferito_entity_1.Preferito);
                exports.TwitterCredentialsRepository = exports.PersistenceProvider.getConnection().getRepository(twitterCreds_entity_1.TwitterCredentials);
                console.log("Initilizing servicies...");
                exports.JWTService = JWTService_1._JWTService.getService();
                exports.FBService = FBService_1._FBService.getService();
                exports.TwitterService = TwitterService_1._TwitterService.getService();
                return [4 /*yield*/, AuthService_1._AuthService.getService()];
            case 2:
                exports.AuthService = _a.sent();
                exports.UserService = UserService_1._UserService.getService();
                exports.BookService = BookService_1._BookService.getService();
                return [2 /*return*/];
        }
    });
}); };
exports.ProvideContext = ProvideContext;
//# sourceMappingURL=applicationContext.js.map