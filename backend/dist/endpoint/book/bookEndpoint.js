"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BookEndpointRegister = void 0;
var errorHandlers_1 = require("../../errors/errorHandlers");
var util_1 = require("../../util");
var express_validator_1 = require("express-validator");
var applicationContext_1 = require("../../applicationContext");
var appError_1 = require("../../errors/appError");
var BookEndpointRegister = function (app) {
    /**
     * @api {get} /book Search for a book in Google Books DB
     * @apiName BookQuery
     * @apiGroup Book
     *
     * @apiParam {String} q Query, minimum lenght: 4 charachters
     *
     *
     * @apiSuccess {GAPI_Book[]} result List of books from Google Books api
     */
    app.get("/book", express_validator_1.query("q").isString().isLength({ min: 4 }), errorHandlers_1.CombineWithErrorHandler(function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
        var errors, authToken, bookRes;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    errors = express_validator_1.validationResult(req);
                    if (!errors.isEmpty()) {
                        throw new appError_1.RequestValidationError(errors);
                    }
                    authToken = util_1.getTokenFromRequest(req);
                    return [4 /*yield*/, applicationContext_1.BookService.doQuery(authToken, req.query["q"])];
                case 1:
                    bookRes = _a.sent();
                    return [2 /*return*/, res.status(appError_1.HttpStatus.HTTP_OK).json({ result: bookRes })];
            }
        });
    }); }));
    /**
    * @api {get} /book/:bookId Get details about book
    * @apiName BookDetails
    * @apiGroup Book
    *
    * @apiParam {String} bookId Google book id of book
    *
    * @apiError 404 Book was not found on google books db
    *
    * @apiSuccess {BookInfo} result Book with details
    */
    app.get("/book/:bookId", errorHandlers_1.CombineWithErrorHandler(function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
        var authToken, book;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    authToken = util_1.getTokenFromRequest(req);
                    return [4 /*yield*/, applicationContext_1.BookService.getBook(authToken, req.params.bookId)];
                case 1:
                    book = _a.sent();
                    return [2 /*return*/, res.status(appError_1.HttpStatus.HTTP_OK).json({ result: book })];
            }
        });
    }); }));
    /**
    * @api {get} /pref/:bookId update pref for book
    * @apiName BookPrefUpd
    * @apiGroup Book
    *
    * @apiParam {String} bookId Google book id of book
    * @apiParam {Number} prefType value to update to (-1:none,0:interessato,1:sto_leggendo,2:ho_letto)
    *
    *
    * @apiSuccess {Preferito} result Updated prefeirto
    */
    app.post("/pref/:bookId", express_validator_1.body("prefType").isNumeric(), errorHandlers_1.CombineWithErrorHandler(function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
        var errors, authToken, prefRet;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    errors = express_validator_1.validationResult(req);
                    if (!errors.isEmpty()) {
                        throw new appError_1.RequestValidationError(errors);
                    }
                    authToken = util_1.getTokenFromRequest(req);
                    return [4 /*yield*/, applicationContext_1.BookService.setPref(authToken, req.params.bookId, req.body.prefType)];
                case 1:
                    prefRet = _a.sent();
                    return [2 /*return*/, res.status(appError_1.HttpStatus.HTTP_OK).json({ result: prefRet })];
            }
        });
    }); }));
};
exports.BookEndpointRegister = BookEndpointRegister;
//# sourceMappingURL=bookEndpoint.js.map