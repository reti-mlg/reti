"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChatEndPoint = void 0;
var errorHandlers_1 = require("../../errors/errorHandlers");
var util_1 = require("../../util");
var appError_1 = require("../../errors/appError");
var JWTService_1 = require("../../service/JWTService");
var ChatService_1 = __importDefault(require("../../service/ChatService"));
var ChatEndPoint = function (app) {
    var jwtservice = JWTService_1._JWTService.getService();
    var socketList = new Array();
    var startPort = Number(process.env.CHAT_STARTING_PORT);
    var numberOfServers = Number(process.env.NUMBER_OF_OPEN_SERVERS);
    var chatRooms = String(process.env.CHAT_SECTIONS).split(",");
    var serverAddress = "localhost";
    for (var i = 0; i < numberOfServers; i++) {
        socketList.push(new ChatService_1.default(startPort + i, chatRooms, i));
    }
    socketList.map(function (item) {
        item.start();
    });
    /**
     * @api {get} /chat/:section  Returns a websocket URL which the client will use to connect to the chatroom
     * @apiName chat
     * @apiGroup Chat
     *
     *@apiParam {String} section  name of the chatroom the client wants to connect to
     *
     *
     * @apiSuccess {String} weboscket section to connect to a chatroom
     */
    app.get("/chat/:section", errorHandlers_1.CombineWithErrorHandler(function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
        var authToken, sollicitator;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    authToken = util_1.getTokenFromRequest(req);
                    sollicitator = jwtservice.verify(authToken);
                    if (!sollicitator) return [3 /*break*/, 2];
                    return [4 /*yield*/, giveASocket(req.params.section)
                            .then(function (socketToConnectTo) {
                            return res.status(appError_1.HttpStatus.HTTP_OK).json(serverAddress.concat(":").concat(socketToConnectTo));
                        })
                            .catch(function (err) {
                            console.error(Error(err).message);
                            return res.status(appError_1.HttpStatus.HTTP_NOT_FOUND).json("This chat is unavailable right now");
                        })];
                case 1:
                    _a.sent();
                    _a.label = 2;
                case 2: return [2 /*return*/];
            }
        });
    }); }));
    /**
    * @api {get} /chatrooms Get a list containing all the available chatrooms in a string form
    * @apiName chatrooms
    * @apiGroup Chat
    *
    *
    * @apiSuccess {String} chatrooms list
    */
    app.get("/chatrooms", errorHandlers_1.CombineWithErrorHandler(function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
        var authToken, sollicitator;
        return __generator(this, function (_a) {
            authToken = util_1.getTokenFromRequest(req);
            sollicitator = jwtservice.verify(authToken);
            if (sollicitator)
                return [2 /*return*/, res.status(appError_1.HttpStatus.HTTP_OK).json(process.env.CHAT_SECTIONS)];
            return [2 /*return*/];
        });
    }); }));
    /**
     * Function that given a websocket finds an open websocket to the requested chat section the client wants to join to,
     * then retruns a promise to find an available open websocket
     * @param {*} ws websocket
     * @returns A promise returning the path clients will use to connect to the chat
     * @errors No more websockets available or connection issues with websockets
     */
    function giveASocket(chatroom) {
        return new Promise(function (resolve, reject) {
            socketList.map(function (item) {
                if (item.getCanReceiveClients(chatroom)) {
                    var socketToConnectTo = String(item.getPort())
                        .concat("/")
                        .concat(String(chatroom).concat(item.getServerId()));
                    console.log(socketToConnectTo);
                    resolve(socketToConnectTo);
                }
            });
            reject(new Error("No more sockets Available"));
        });
    }
};
exports.ChatEndPoint = ChatEndPoint;
//# sourceMappingURL=chat.js.map