"use strict";
/*
 * Created on Mon May 17 2021
 *
 * Author: Gioele Zacchia
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthTestEndpointProvider = void 0;
var express_validator_1 = require("express-validator");
var applicationContext_1 = require("../../applicationContext");
var appError_1 = require("../../errors/appError");
var errorHandlers_1 = require("../../errors/errorHandlers");
var mapDataUserToView = function (usr) {
    return {
        id: usr.id,
        email: usr.email,
        icon: usr.icon,
        friendly_name: usr.friendly_name,
        fb_id: usr.facebook_id,
        tw_id: usr.twitter_id,
    };
};
var AuthTestEndpointProvider = function (app) {
    if (process.env.NODE_ENV == "production") {
        return;
    }
    app.post("/test/fb", express_validator_1.body("code").isString().notEmpty(), errorHandlers_1.CombineWithErrorHandler(function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
        var errors, ao;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    errors = express_validator_1.validationResult(req);
                    if (!errors.isEmpty()) {
                        throw new appError_1.RequestValidationError(errors);
                    }
                    return [4 /*yield*/, applicationContext_1.FBService.handleFBLogin(req.body.code)];
                case 1:
                    ao = _a.sent();
                    return [2 /*return*/, res.status(appError_1.HttpStatus.HTTP_OK).send("Ao")];
            }
        });
    }); }));
    app.post("/test/login", express_validator_1.body("email").isEmail(), express_validator_1.body("password").isString(), errorHandlers_1.CombineWithErrorHandler(function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
        var errors, token;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    errors = express_validator_1.validationResult(req);
                    if (!errors.isEmpty()) {
                        throw new appError_1.RequestValidationError(errors);
                    }
                    return [4 /*yield*/, applicationContext_1.AuthService.loginEP(req.body.email, req.body.password)];
                case 1:
                    token = _a.sent();
                    return [2 /*return*/, res.status(appError_1.HttpStatus.HTTP_OK).json({ token: token })];
            }
        });
    }); }));
    app.post("/test/register", express_validator_1.body("email").isEmail(), express_validator_1.body("password").isLength({ min: 6 }), express_validator_1.body("friendly_name").isString(), errorHandlers_1.CombineWithErrorHandler(function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
        var errors, user;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    errors = express_validator_1.validationResult(req);
                    if (!errors.isEmpty()) {
                        throw new appError_1.RequestValidationError(errors);
                    }
                    return [4 /*yield*/, applicationContext_1.AuthService.createEPUser(req.body.email, req.body.password, req.body.friendly_name)
                        //return res.status(HttpStatus.HTTP_CREATED).json(mapDataUserToView(user))
                    ];
                case 1:
                    user = _a.sent();
                    return [2 /*return*/];
            }
        });
    }); }));
    /*app.get("/test/whoami",
        CombineWithErrorHandler(async (req,res)=>{
            const authToken = getTokenFromRequest(req)
            const tokenPayload = AuthService.verifyToken(authToken)
            return res.status(200).json(tokenPayload)
        }))*/
};
exports.AuthTestEndpointProvider = AuthTestEndpointProvider;
//# sourceMappingURL=authTest.js.map