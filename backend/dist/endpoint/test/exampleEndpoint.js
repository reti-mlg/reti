"use strict";
/*
 * Created on Wed May 12 2021
 *
 * Author: Gioele Zacchia
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExampleEndpointRegister = void 0;
var applicationContext_1 = require("../../applicationContext");
var ExampleEndpointRegister = function (app) {
    /**
     * @api {get} / Get software version
     * @apiName GetVersion
     * @apiGroup Test
     *
     *
     * @apiSuccess {String} version Software version
     */
    app.get("/", function (req, res) {
        var version = process.env.VERSION || "UNKNOWN";
        var responce = {
            version: version
        };
        res.send(JSON.stringify(responce));
    });
    /**
     * @api {get} /test Test example service
     * @apiName GetTest
     * @apiGroup Test
     *
     *
     * @apiSuccess {String} ao Eample service result
     */
    app.get("/test", function (req, res) {
        var responce = {
            ao: applicationContext_1.ExampleService.getSomething()
        };
        res.send(JSON.stringify(responce));
    });
    /**
     * @api {get} /nested Test example nested service
     * @apiName GetNested
     * @apiGroup Test
     *
     *
     * @apiSuccess {String} ao Example nested result
     */
    app.get("/nested", function (req, res) {
        var responce = {
            ao: applicationContext_1.ExampleNestedService.getSomething()
        };
        res.send(JSON.stringify(responce));
    });
};
exports.ExampleEndpointRegister = ExampleEndpointRegister;
//# sourceMappingURL=exampleEndpoint.js.map