"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserEndpointRegister = void 0;
var express_validator_1 = require("express-validator");
var multer_1 = __importDefault(require("multer"));
var applicationContext_1 = require("../../applicationContext");
var appError_1 = require("../../errors/appError");
var errorHandlers_1 = require("../../errors/errorHandlers");
var util_1 = require("../../util");
var UserEndpointRegister = function (app) {
    /**
     * @api {get} /user Get information about current user
     * @apiName UserSelf
     * @apiGroup User
     *
     *
     *
     *
     * @apiSuccess {UserPrivate} User informations
     */
    app.get("/user", errorHandlers_1.CombineWithErrorHandler(function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
        var authToken, user;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    authToken = util_1.getTokenFromRequest(req);
                    return [4 /*yield*/, applicationContext_1.UserService.getSelf(authToken)];
                case 1:
                    user = _a.sent();
                    return [2 /*return*/, res.status(appError_1.HttpStatus.HTTP_OK).json(user)];
            }
        });
    }); }));
    /**
     * @api {get} /user/:userId Get information about user by id
     * @apiName User
     * @apiGroup User
     *
     *@apiParam {String} userId userid to search for
     *
     *
     * @apiSuccess {UserPublic} User informations
     */
    app.get("/user/:userId", errorHandlers_1.CombineWithErrorHandler(function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
        var authToken, user;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    authToken = util_1.getTokenFromRequest(req);
                    return [4 /*yield*/, applicationContext_1.UserService.getUser(authToken, req.params.userId)];
                case 1:
                    user = _a.sent();
                    return [2 /*return*/, res.status(appError_1.HttpStatus.HTTP_OK).json(user)];
            }
        });
    }); }));
    /**
     * @api {put} /user/:userId Update informations about user (Only usable with own id)
     * @apiName PutUser
     * @apiGroup User
     *
     *@apiParam {String} friendly_name update the name parameter
     *
     *
     * @apiSuccess {UserPublic} User informations
     */
    app.put("/user/:userId", express_validator_1.body("friendly_name").optional().isString(), errorHandlers_1.CombineWithErrorHandler(function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
        var authToken, nName, user;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    authToken = util_1.getTokenFromRequest(req);
                    nName = req.body.friendly_name;
                    return [4 /*yield*/, applicationContext_1.UserService.updateFName(authToken, req.params.userId, nName)];
                case 1:
                    user = _a.sent();
                    return [2 /*return*/, res.status(appError_1.HttpStatus.HTTP_OK).json(user)];
            }
        });
    }); }));
    /**
     * @api {post} /fbConnect Connect FB account to user
     * @apiName FbConnect
     * @apiGroup User
     *
     *@apiParam {String} code as returned by facebook login
     *
     *
     * @apiSuccess {boolean} Ok true
     */
    app.post("/fbConnect", express_validator_1.body("code").isString().notEmpty(), errorHandlers_1.CombineWithErrorHandler(function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
        var authToken, code, ok;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    authToken = util_1.getTokenFromRequest(req);
                    code = req.body.code;
                    return [4 /*yield*/, applicationContext_1.FBService.connectFB(authToken, code)];
                case 1:
                    ok = _a.sent();
                    return [2 /*return*/, res.status(appError_1.HttpStatus.HTTP_OK).json({ ok: true })];
            }
        });
    }); }));
    /**
     * @api {post} /fbConnect Disconnect FB from current user
     * @apiName FbDisconnect
     * @apiGroup User
     *
     *
     *
     * @apiSuccess {boolean} Ok true
     */
    app.post("/fbDisconnect", errorHandlers_1.CombineWithErrorHandler(function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
        var authToken, ok;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    authToken = util_1.getTokenFromRequest(req);
                    return [4 /*yield*/, applicationContext_1.FBService.disconnectFB(authToken)];
                case 1:
                    ok = _a.sent();
                    return [2 /*return*/, res.status(appError_1.HttpStatus.HTTP_OK).json({ ok: true })];
            }
        });
    }); }));
    /**
     * @api {post} /twitterDisconnect Connect twitter account to user
     * @apiName TwitterConnect
     * @apiGroup User
     *
     *
     *
     * @apiSuccess {boolean} Ok true
     */
    app.post("/twitterDisconnect", errorHandlers_1.CombineWithErrorHandler(function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
        var authToken;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    authToken = util_1.getTokenFromRequest(req);
                    return [4 /*yield*/, applicationContext_1.TwitterService.twitterDisconnect(authToken)];
                case 1:
                    _a.sent();
                    return [2 /*return*/, res.status(appError_1.HttpStatus.HTTP_OK).json({ ok: true })];
            }
        });
    }); }));
    /**
     * @api {post} /twitterConnect Connect FB account to user
     * @apiName TwitterConnect
     * @apiGroup User
     *
     * @apiParam {String} token token as returned by TwitterToken endpoint
     * @apiParam {String} verifier verifier token returned by twitter login
     *
     *
     * @apiSuccess {boolean} Ok true
     */
    app.post("/twitterConnect", express_validator_1.body("token").isString().notEmpty(), express_validator_1.body("verifier").isString().notEmpty(), errorHandlers_1.CombineWithErrorHandler(function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
        var authToken;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    authToken = util_1.getTokenFromRequest(req);
                    return [4 /*yield*/, applicationContext_1.TwitterService.twitterConnect(authToken, req.body.token, req.body.verifier)];
                case 1:
                    _a.sent();
                    return [2 /*return*/, res
                            .status(appError_1.HttpStatus.HTTP_OK)
                            .json({ ok: true })];
            }
        });
    }); }));
    var upload = multer_1.default({
        storage: multer_1.default.memoryStorage(),
        limits: {
            fileSize: Number(process.env.MAX_FILE_SIZE)
        },
        fileFilter: function (req, file, cb) {
            if (file.mimetype === "image/png" || file.mimetype === "image/jpeg" || file.mimetype === "image/gif") {
                cb(null, true);
            }
            else {
                cb(null, false);
            }
        }
    });
    /**
     * @api {post} /avatar Upload new avatar
     * @apiName AvatarUpload
     * @apiGroup User
     *
     * @apiParam {File} avatar Image to use as avatar (Max MAX_FILE_SIZE byte, allowed type png,jpeg, gif)
     *
     *
     * @apiSuccess {boolean} Ok true
     */
    app.post("/avatar", upload.single("avatar"), errorHandlers_1.CombineWithErrorHandler(function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
        var authToken;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    authToken = util_1.getTokenFromRequest(req);
                    if (req.file.mimetype !== "image/png" && req.file.mimetype !== "image/jpeg" && req.file.mimetype !== "image/gif") {
                        res.status(appError_1.HttpStatus.HTTP_NOT_ACCEPTABLE).send({ message: "Invalid file type" });
                    }
                    return [4 /*yield*/, applicationContext_1.UserService.uploadAvatar(authToken, req.file.originalname, req.file.buffer)];
                case 1:
                    _a.sent();
                    return [2 /*return*/, res.status(appError_1.HttpStatus.HTTP_OK).send({ ok: true })];
            }
        });
    }); }));
};
exports.UserEndpointRegister = UserEndpointRegister;
//# sourceMappingURL=userEndpoint.js.map