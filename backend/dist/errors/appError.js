"use strict";
/*
 * Created on Tue May 18 2021
 *
 * Author: Gioele Zacchia
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.NotFoundError = exports.RequestValidationError = exports.AppError = exports.HttpStatus = void 0;
var HttpStatus;
(function (HttpStatus) {
    HttpStatus[HttpStatus["HTTP_OK"] = 200] = "HTTP_OK";
    HttpStatus[HttpStatus["HTTP_CREATED"] = 201] = "HTTP_CREATED";
    HttpStatus[HttpStatus["HTTP_NO_CONTENT"] = 204] = "HTTP_NO_CONTENT";
    HttpStatus[HttpStatus["HTTP_BAD_REQUEST"] = 400] = "HTTP_BAD_REQUEST";
    HttpStatus[HttpStatus["HTTP_UNAUTHORIZED"] = 401] = "HTTP_UNAUTHORIZED";
    HttpStatus[HttpStatus["HTTP_FORBIDDEN"] = 403] = "HTTP_FORBIDDEN";
    HttpStatus[HttpStatus["HTTP_NOT_FOUND"] = 404] = "HTTP_NOT_FOUND";
    HttpStatus[HttpStatus["HTTP_METHOD_NOT_ALLOWED"] = 405] = "HTTP_METHOD_NOT_ALLOWED";
    HttpStatus[HttpStatus["HTTP_NOT_ACCEPTABLE"] = 406] = "HTTP_NOT_ACCEPTABLE";
    HttpStatus[HttpStatus["HTTP_CONFLICT"] = 409] = "HTTP_CONFLICT";
    HttpStatus[HttpStatus["HTTP_GONE"] = 410] = "HTTP_GONE";
    HttpStatus[HttpStatus["HTTP_INTERNAL_SERVER_ERROR"] = 500] = "HTTP_INTERNAL_SERVER_ERROR";
    HttpStatus[HttpStatus["HTTP_NOT_IMPLEMENTED"] = 501] = "HTTP_NOT_IMPLEMENTED";
})(HttpStatus = exports.HttpStatus || (exports.HttpStatus = {}));
var AppError = /** @class */ (function () {
    function AppError(info, code) {
        if (code === void 0) { code = HttpStatus.HTTP_INTERNAL_SERVER_ERROR; }
        this.info = info;
        this.code = code;
    }
    return AppError;
}());
exports.AppError = AppError;
var RequestValidationError = /** @class */ (function (_super) {
    __extends(RequestValidationError, _super);
    function RequestValidationError(error) {
        return _super.call(this, { message: "Invalid request", errors: error.array() }, HttpStatus.HTTP_BAD_REQUEST) || this;
    }
    return RequestValidationError;
}(AppError));
exports.RequestValidationError = RequestValidationError;
var NotFoundError = /** @class */ (function (_super) {
    __extends(NotFoundError, _super);
    function NotFoundError(message) {
        if (message === void 0) { message = "Not Found"; }
        return _super.call(this, message, HttpStatus.HTTP_NOT_FOUND) || this;
    }
    return NotFoundError;
}(AppError));
exports.NotFoundError = NotFoundError;
//# sourceMappingURL=appError.js.map