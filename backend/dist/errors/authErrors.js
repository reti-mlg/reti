"use strict";
/*
 * Created on Tue May 18 2021
 *
 * Author: Gioele Zacchia
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConflictError = exports.ForbiddenError = exports.UnauthorizedError = void 0;
var appError_1 = require("./appError");
var UnauthorizedError = /** @class */ (function (_super) {
    __extends(UnauthorizedError, _super);
    function UnauthorizedError(message) {
        if (message === void 0) { message = "Unauthorized"; }
        return _super.call(this, message, appError_1.HttpStatus.HTTP_UNAUTHORIZED) || this;
    }
    return UnauthorizedError;
}(appError_1.AppError));
exports.UnauthorizedError = UnauthorizedError;
var ForbiddenError = /** @class */ (function (_super) {
    __extends(ForbiddenError, _super);
    function ForbiddenError(message) {
        if (message === void 0) { message = "Forbidden"; }
        return _super.call(this, message, appError_1.HttpStatus.HTTP_FORBIDDEN) || this;
    }
    return ForbiddenError;
}(appError_1.AppError));
exports.ForbiddenError = ForbiddenError;
var ConflictError = /** @class */ (function (_super) {
    __extends(ConflictError, _super);
    function ConflictError(message) {
        if (message === void 0) { message = "Conflict"; }
        return _super.call(this, message, appError_1.HttpStatus.HTTP_CONFLICT) || this;
    }
    return ConflictError;
}(appError_1.AppError));
exports.ConflictError = ConflictError;
//# sourceMappingURL=authErrors.js.map