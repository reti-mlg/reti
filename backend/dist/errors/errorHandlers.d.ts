import express from 'express';
export declare const AppErrorHandler: (error: any, req: express.Request, res: express.Response, next: express.NextFunction) => void | express.Response<any, Record<string, any>>;
declare type asyncHandler = (req: express.Request, res: express.Response) => Promise<any> | any;
export declare const CombineWithErrorHandler: (t: asyncHandler) => (req: express.Request, res: express.Response, next: express.NextFunction) => void;
export {};
//# sourceMappingURL=errorHandlers.d.ts.map