"use strict";
/*
 * Created on Tue May 18 2021
 *
 * Author: Gioele Zacchia
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.CombineWithErrorHandler = exports.AppErrorHandler = void 0;
var appError_1 = require("./appError");
var AppErrorHandler = function (error, req, res, next) {
    if (res.headersSent) {
        return next(error);
    }
    if (error instanceof appError_1.AppError) {
        return res.status(error.code).json({ status: error.code, info: error.info });
    }
    console.error(error);
    res.status(appError_1.HttpStatus.HTTP_INTERNAL_SERVER_ERROR).send({ status: appError_1.HttpStatus.HTTP_INTERNAL_SERVER_ERROR, info: "Unknown error", error: error });
};
exports.AppErrorHandler = AppErrorHandler;
var CombineWithErrorHandler = function (t) {
    return function (req, res, next) {
        var rt = t(req, res);
        if (rt instanceof Promise) {
            rt.catch(function (err) { return next(err); });
        }
    };
};
exports.CombineWithErrorHandler = CombineWithErrorHandler;
//# sourceMappingURL=errorHandlers.js.map