import { ConnectionOptions, Connection } from "typeorm";
export declare enum DBErrorCodes {
    INTEGRITY_CONSTRAINT_VIOLATION = "23000",
    RESTRICT_VIOLATION = "23001",
    NOT_NULL_VIOLATION = "23502",
    FOREIGN_KEY_VIOLATION = "23503",
    UNIQUE_VIOLATION = "23505",
    CHECK_VIOLATION = "23514",
    EXCLUSION_VIOLATION = "23P01"
}
export declare class _PersistenceProvider {
    private dbConn;
    static getService(config: ConnectionOptions): Promise<_PersistenceProvider>;
    static getConfigFromEnv(): import("typeorm/driver/postgres/PostgresConnectionOptions").PostgresConnectionOptions;
    private constructor();
    getConnection(): Connection;
}
//# sourceMappingURL=Persistence.d.ts.map