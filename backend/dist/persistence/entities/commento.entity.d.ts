import { User } from "./user.entity";
export declare class Commento {
    id?: string;
    senderId?: string;
    sender?: User;
    book_id: string;
    date: Date;
    content: string;
}
//# sourceMappingURL=commento.entity.d.ts.map