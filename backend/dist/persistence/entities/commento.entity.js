"use strict";
/*
 * Created on Mon May 17 2021
 *
 * Author: Gioele Zacchia
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Commento = void 0;
var typeorm_1 = require("typeorm");
var user_entity_1 = require("./user.entity");
var Commento = /** @class */ (function () {
    function Commento() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn("uuid"),
        __metadata("design:type", String)
    ], Commento.prototype, "id", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Commento.prototype, "senderId", void 0);
    __decorate([
        typeorm_1.ManyToOne(function () { return user_entity_1.User; }, {}),
        __metadata("design:type", user_entity_1.User)
    ], Commento.prototype, "sender", void 0);
    __decorate([
        typeorm_1.Column({
            nullable: false
        }),
        __metadata("design:type", String)
    ], Commento.prototype, "book_id", void 0);
    __decorate([
        typeorm_1.Column({
            nullable: false
        }),
        __metadata("design:type", Date)
    ], Commento.prototype, "date", void 0);
    __decorate([
        typeorm_1.Column({
            nullable: false
        }),
        __metadata("design:type", String)
    ], Commento.prototype, "content", void 0);
    Commento = __decorate([
        typeorm_1.Entity()
    ], Commento);
    return Commento;
}());
exports.Commento = Commento;
//# sourceMappingURL=commento.entity.js.map