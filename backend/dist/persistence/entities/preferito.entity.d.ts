import { User } from "./user.entity";
export declare enum PrefType {
    none = -1,
    interessato = 0,
    sto_leggendo = 1,
    ho_letto = 2
}
export declare class Preferito {
    userId: string;
    user?: User;
    bookId: string;
    date: Date;
    type: PrefType;
}
//# sourceMappingURL=preferito.entity.d.ts.map