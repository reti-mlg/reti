"use strict";
/*
 * Created on Mon May 17 2021
 *
 * Author: Gioele Zacchia
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Preferito = exports.PrefType = void 0;
var typeorm_1 = require("typeorm");
var user_entity_1 = require("./user.entity");
var PrefType;
(function (PrefType) {
    PrefType[PrefType["none"] = -1] = "none";
    PrefType[PrefType["interessato"] = 0] = "interessato";
    PrefType[PrefType["sto_leggendo"] = 1] = "sto_leggendo";
    PrefType[PrefType["ho_letto"] = 2] = "ho_letto";
})(PrefType = exports.PrefType || (exports.PrefType = {}));
var Preferito = /** @class */ (function () {
    function Preferito() {
    }
    __decorate([
        typeorm_1.Column({
            primary: true
        }),
        __metadata("design:type", String)
    ], Preferito.prototype, "userId", void 0);
    __decorate([
        typeorm_1.ManyToOne(function () { return user_entity_1.User; }, {}),
        __metadata("design:type", user_entity_1.User)
    ], Preferito.prototype, "user", void 0);
    __decorate([
        typeorm_1.Column({
            primary: true
        }),
        __metadata("design:type", String)
    ], Preferito.prototype, "bookId", void 0);
    __decorate([
        typeorm_1.Column({
            nullable: false
        }),
        __metadata("design:type", Date)
    ], Preferito.prototype, "date", void 0);
    __decorate([
        typeorm_1.Column({
            nullable: false,
            type: 'int'
        }),
        __metadata("design:type", Number)
    ], Preferito.prototype, "type", void 0);
    Preferito = __decorate([
        typeorm_1.Entity()
    ], Preferito);
    return Preferito;
}());
exports.Preferito = Preferito;
//# sourceMappingURL=preferito.entity.js.map