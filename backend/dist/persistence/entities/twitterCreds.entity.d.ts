import { User } from "./user.entity";
export declare class TwitterCredentials {
    id?: string;
    token_key: string;
    token_secret: string;
    verified?: boolean;
    user?: User;
    userId?: string;
}
//# sourceMappingURL=twitterCreds.entity.d.ts.map