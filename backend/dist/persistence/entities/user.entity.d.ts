/// <reference types="node" />
export declare class User {
    id?: string;
    email?: string;
    password?: Buffer;
    salt?: string;
    icon?: string;
    friendly_name?: string;
    facebook_id?: string;
    facebook_token?: string;
    twitter_id?: string;
}
//# sourceMappingURL=user.entity.d.ts.map