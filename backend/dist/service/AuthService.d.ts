export declare class _AuthService {
    private userRepo;
    private jwtService;
    static getService(): Promise<_AuthService>;
    secret: string;
    expiration?: number;
    private constructor();
    createEPUser(email: string, password: string, friendly_name: string): Promise<string>;
    loginEP(email: string, password: string): Promise<string>;
}
//# sourceMappingURL=AuthService.d.ts.map