import { Preferito, PrefType } from "../persistence/entities/preferito.entity";
interface GAPI_Inds_ids {
    type: string;
    identifier: string;
}
interface GAPI_Book {
    kind: string;
    id: string;
    etag: string;
    selfLink: string;
    searchInfo?: {
        textSnippet: string;
    };
    volumeInfo: {
        title: string;
        subtitle: string;
        authors: string[];
        publisher: string;
        publishedDate: string;
        description: string;
        industryIdentifiers: GAPI_Inds_ids[];
        readingModes: {
            text: boolean;
            image: boolean;
        };
        pageCount: number;
        printType: string;
        categories: string[];
        averageRating: number;
        ratingsCount: number;
        maturityRating: string;
        allowAnonLogging: boolean;
        contentVersion: string;
        panelizationSummary: {
            containsEpubBubbles: false;
            containsImageBubbles: false;
        };
        imageLinks: {
            smallThumbnail: string;
            thumbnail: string;
        };
        language: string;
        previewLink: string;
        infoLink: string;
        canonicalVolumeLink: string;
    };
}
interface BookInfo {
    book: GAPI_Book;
    ownPref: Preferito;
    friendPrefs: Preferito[];
}
export declare class _BookService {
    private prefRepo;
    private userService;
    private jwtService;
    private static readonly GAPI_BOOK_URL;
    static getService(): _BookService;
    private constructor();
    doQuery(sollicitatorToken: string, query: string): Promise<GAPI_Book[]>;
    getBook(sollicitatorToken: string, bookId: string): Promise<BookInfo>;
    setPref(sollicitatorToken: string, bookId: string, prefType: PrefType): Promise<Preferito>;
}
export {};
//# sourceMappingURL=BookService.d.ts.map