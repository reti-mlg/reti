"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports._BookService = void 0;
var axios_1 = __importDefault(require("axios"));
var applicationContext_1 = require("../applicationContext");
var appError_1 = require("../errors/appError");
var authErrors_1 = require("../errors/authErrors");
var preferito_entity_1 = require("../persistence/entities/preferito.entity");
var _BookService = /** @class */ (function () {
    function _BookService(prefRepo, userService, jwtService) {
        this.prefRepo = prefRepo;
        this.userService = userService;
        this.jwtService = jwtService;
    }
    _BookService.getService = function () {
        return new _BookService(applicationContext_1.PreferitoRepository, applicationContext_1.UserService, applicationContext_1.JWTService);
    };
    _BookService.prototype.doQuery = function (sollicitatorToken, query) {
        return __awaiter(this, void 0, void 0, function () {
            var t, url, ret;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        t = this.jwtService.verify(sollicitatorToken);
                        if (!t) {
                            throw new authErrors_1.UnauthorizedError();
                        }
                        url = _BookService.GAPI_BOOK_URL
                            + ("?key=" + encodeURI(process.env.GOOGLE_API_KEY))
                            + ("&q=" + encodeURI(query));
                        return [4 /*yield*/, axios_1.default.request({
                                method: "GET",
                                url: url,
                                validateStatus: function () { return true; }
                            })];
                    case 1:
                        ret = _a.sent();
                        if (ret.status != 200) {
                            throw new appError_1.AppError("Cannot connect to Google Api", appError_1.HttpStatus.HTTP_INTERNAL_SERVER_ERROR);
                        }
                        return [2 /*return*/, ret.data.items];
                }
            });
        });
    };
    _BookService.prototype.getBook = function (sollicitatorToken, bookId) {
        return __awaiter(this, void 0, void 0, function () {
            var t, url, ret, friends, prefs, ownPref, h;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        t = this.jwtService.verify(sollicitatorToken);
                        if (!t) {
                            throw new authErrors_1.UnauthorizedError();
                        }
                        url = _BookService.GAPI_BOOK_URL
                            + ("/" + encodeURI(bookId))
                            + ("?key=" + encodeURI(process.env.GOOGLE_API_KEY));
                        return [4 /*yield*/, axios_1.default.request({
                                method: "GET",
                                url: url,
                                validateStatus: function () { return true; }
                            })];
                    case 1:
                        ret = _a.sent();
                        if (ret.status == 404) {
                            throw new appError_1.NotFoundError("BookID not found");
                        }
                        if (ret.status != 200) {
                            throw new appError_1.AppError("Cannot connect to Google Api", appError_1.HttpStatus.HTTP_INTERNAL_SERVER_ERROR);
                        }
                        return [4 /*yield*/, this.userService.getFriendList(sollicitatorToken)];
                    case 2:
                        friends = _a.sent();
                        prefs = [];
                        if (!(friends.length > 0)) return [3 /*break*/, 4];
                        return [4 /*yield*/, this.prefRepo.createQueryBuilder("pref")
                                .where("pref.bookId = :bookId", { bookId: bookId })
                                .andWhere("pref.userId IN (:...friends)", { friends: friends.map(function (f) { return f.user.id; }) })
                                .getMany()];
                    case 3:
                        prefs = _a.sent();
                        _a.label = 4;
                    case 4: return [4 /*yield*/, this.prefRepo.findOne({
                            where: {
                                bookId: bookId,
                                userId: t.uid
                            }
                        })];
                    case 5:
                        ownPref = _a.sent();
                        h = {
                            book: ret.data,
                            ownPref: ownPref,
                            friendPrefs: prefs
                        };
                        return [2 /*return*/, h];
                }
            });
        });
    };
    _BookService.prototype.setPref = function (sollicitatorToken, bookId, prefType) {
        return __awaiter(this, void 0, void 0, function () {
            var sollicitator, url, ret, t;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        sollicitator = this.jwtService.verify(sollicitatorToken);
                        url = _BookService.GAPI_BOOK_URL
                            + ("/" + encodeURI(bookId))
                            + ("?key=" + encodeURI(process.env.GOOGLE_API_KEY));
                        return [4 /*yield*/, axios_1.default.request({
                                method: "GET",
                                url: url,
                                validateStatus: function () { return true; }
                            })];
                    case 1:
                        ret = _a.sent();
                        if (ret.status == 404) {
                            throw new appError_1.NotFoundError("BookID not found");
                        }
                        t = {
                            bookId: bookId,
                            userId: sollicitator.uid,
                            type: prefType,
                            date: new Date()
                        };
                        if (!(prefType == preferito_entity_1.PrefType.none)) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.prefRepo.delete({
                                userId: sollicitator.uid,
                                bookId: bookId
                            })];
                    case 2:
                        _a.sent();
                        return [2 /*return*/, t];
                    case 3: return [4 /*yield*/, this.prefRepo.save(t)];
                    case 4: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    _BookService.GAPI_BOOK_URL = "https://www.googleapis.com/books/v1/volumes";
    return _BookService;
}());
exports._BookService = _BookService;
//# sourceMappingURL=BookService.js.map