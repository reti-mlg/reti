import ws from "ws";
import { _RabbitService } from "./RabbitService";
interface WSLE {
    "chatroom": string;
    "ws": ws.Server;
}
export default class _ChatService {
    port: Number;
    chatRooms: string[];
    serverId: Number;
    wss: Map<string, WSLE>;
    maxClientInSocket: Number;
    rabbitService: _RabbitService;
    constructor(port: Number, chatRooms: string[], serverId: Number);
    /**
     * Method that returns a websockets port
     * @returns   {String}         Value of the port
     */
    getPort(): Number;
    /**
     * Returns number of clients connected to the socket of a designated chatroom
     * @param ChatRoom String containing the value of the chatroom
     * @returns  the number of connected clients to the websocket
     */
    getClients(ChatRoom: string): any;
    /**
     *
     * @returns Websocket server id number
     */
    getServerId(): Number;
    /**
     * Method used to check the availability of free socket for a specific chatroom
     * @param ChatRoom String containing the value of the chatroom
     * @returns true if new clients can connect to the selected chatroom, false otherwise
     */
    getCanReceiveClients(ChatRoom: string): boolean;
    /**
     * method used to instatiantiate websockets
     */
    CreateSockets(): Promise<void>;
    /**
     * Method that returns a promise to create an http server
     * @param {*} port Port the server will use to listen
     * @returns A promise
     */
    createHttpServer(port: Number): Promise<unknown>;
    /**
     * Methods that send a message to all the clients connected to the websockets
     * @param {*} message
     * @returns A promise
     */
    sendBroadcast(data: any, ChatRoom: string): Promise<void>;
    /**
     * Method that returns a promise to make websockets start listening for events
     * @param {*} server
     * @returns A promise
     */
    httpServerListen(server: any): Promise<void>;
    /**
     * Method use to start up the websocket server
     */
    start(): void;
}
export {};
//# sourceMappingURL=ChatService.d.ts.map