"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var http_1 = __importDefault(require("http"));
var ws_1 = __importDefault(require("ws"));
var url_1 = __importDefault(require("url"));
var RabbitService_1 = require("./RabbitService");
var _ChatService = /** @class */ (function () {
    function _ChatService(port, chatRooms, serverId) {
        this.port = port;
        this.chatRooms = chatRooms;
        this.serverId = serverId;
        this.wss = new Map();
        this.maxClientInSocket = Number(process.env.MAX_CLIENT_CONNECTED_IN_SOCKET) || 1;
        this.rabbitService = RabbitService_1._RabbitService.getService();
    }
    /**
     * Method that returns a websockets port
     * @returns   {String}         Value of the port
     */
    _ChatService.prototype.getPort = function () {
        return this.port;
    };
    /**
     * Returns number of clients connected to the socket of a designated chatroom
     * @param ChatRoom String containing the value of the chatroom
     * @returns  the number of connected clients to the websocket
     */
    _ChatService.prototype.getClients = function (ChatRoom) {
        return this.wss.get(ChatRoom).ws.clients.size;
    };
    /**
     *
     * @returns Websocket server id number
     */
    _ChatService.prototype.getServerId = function () {
        return this.serverId;
    };
    /**
     * Method used to check the availability of free socket for a specific chatroom
     * @param ChatRoom String containing the value of the chatroom
     * @returns true if new clients can connect to the selected chatroom, false otherwise
     */
    _ChatService.prototype.getCanReceiveClients = function (ChatRoom) {
        if (this.getClients(ChatRoom) < this.maxClientInSocket)
            return true;
        else
            return false;
    };
    /**
     * method used to instatiantiate websockets
     */
    _ChatService.prototype.CreateSockets = function () {
        var self = this;
        return new Promise(function (resolve) {
            self.chatRooms.map(function (item) {
                var temp = {
                    "chatroom": item,
                    "ws": new ws_1.default.Server({ noServer: true })
                };
                self.wss.set(item, temp);
                self.wss.get(item).ws.setMaxListeners(self.maxClientInSocket);
                //console.log(self.wss.get(item).ws)
            });
            resolve();
        });
    };
    /**
     * Method that returns a promise to create an http server
     * @param {*} port Port the server will use to listen
     * @returns A promise
     */
    _ChatService.prototype.createHttpServer = function (port) {
        return new Promise(function (resolve, reject) {
            var server = http_1.default.createServer();
            server.listen(port);
            server.on("listening", function () {
                resolve(server);
            });
            server.on("error", function (err) {
                reject(err);
            });
        });
    };
    /**
     * Methods that send a message to all the clients connected to the websockets
     * @param {*} message
     * @returns A promise
     */
    _ChatService.prototype.sendBroadcast = function (data, ChatRoom) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.wss.get(ChatRoom).ws.clients.forEach(function (client) {
                if (client !== ws_1.default /* && client.readyState === WebSocket.OPEN */) {
                    client.send(data);
                    client.on("error", function (err) {
                        reject(err);
                    });
                }
            });
            resolve();
        });
    };
    /**
     * Method that returns a promise to make websockets start listening for events
     * @param {*} server
     * @returns A promise
     */
    _ChatService.prototype.httpServerListen = function (server) {
        var self = this;
        return new Promise(function (resolve, reject) {
            server.on("upgrade", function upgrade(request, socket, head) {
                var pathname = url_1.default.parse(request.url).pathname;
                self.wss.forEach(function (item) {
                    if (pathname === "/".concat(item.chatroom).concat(String(self.serverId))) {
                        item.ws.handleUpgrade(request, socket, head, function done(ws) {
                            item.ws.emit("connection", ws, request);
                            console.log("new %s: %s", item.chatroom, self.getClients(item.chatroom));
                            ws.on("message", function incoming(message) {
                                console.log("received: %s through websocket %s", message, item.chatroom);
                                self.rabbitService.publisher(message, item.chatroom)
                                    .then(function (msg) { return console.info(msg); })
                                    .catch(function (error) { return console.error(error); });
                            });
                            ws.on("close", function () {
                                console.info("ended connection");
                                //if (item.clients.size === 0) self.wss.splice(index, 1);
                                console.log("closed %s: %s", item.chatroom, self.getClients(item.chatroom));
                            });
                        });
                    }
                });
                resolve();
            });
            server.on("error", function (err) { return reject(err); });
        });
    };
    /**
     * Method use to start up the websocket server
     */
    _ChatService.prototype.start = function () {
        var self = this;
        self.CreateSockets().then(function () {
            self
                .createHttpServer(self.port)
                .then(function (server) {
                /* console.info("server start on port %d", self.port); */
                self
                    .httpServerListen(server)
                    .then()
                    .catch(function (error) { return console.error(error); });
            })
                .catch(function (error) { return console.error(error); });
            self.wss.forEach(function (item) {
                self.rabbitService.subscriber(item.chatroom)
                    .then(function (values) {
                    var channel = values[0];
                    var que = values[1];
                    channel.consume(que, function (msg) {
                        if (msg.content) {
                            var message = msg.content.toString();
                            console.log(" [x] Received %s through rabbit", message);
                            self.sendBroadcast(message, item.chatroom).catch(function (err) {
                                console.error(Error(err).message);
                            });
                        }
                    }, {
                        noAck: true,
                    });
                })
                    .catch(function (error) { return console.error(error); });
            });
        });
    };
    return _ChatService;
}());
exports.default = _ChatService;
//# sourceMappingURL=ChatService.js.map