export declare class _FBService {
    private userRepo;
    private jwtService;
    static getService(): _FBService;
    private constructor();
    private static readonly FB_GET_ACCESS_TOKEN;
    private static readonly FB_GET_SELF_INFO;
    private static readonly FB_GET_FRIENDS;
    disconnectFB(sollicitatorToken: string): Promise<boolean>;
    connectFB(sollicitatorToken: string, fbCode: string): Promise<boolean>;
    handleFBLogin(code: string): Promise<string>;
    getFriends(userID: string): Promise<string[]>;
}
//# sourceMappingURL=FBService.d.ts.map