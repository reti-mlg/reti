"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports._FBService = void 0;
var applicationContext_1 = require("../applicationContext");
var axios_1 = __importDefault(require("axios"));
var appError_1 = require("../errors/appError");
var authErrors_1 = require("../errors/authErrors");
var _FBService = /** @class */ (function () {
    function _FBService(userRepo, jwtService) {
        this.userRepo = userRepo;
        this.jwtService = jwtService;
    }
    _FBService.getService = function () {
        return new _FBService(applicationContext_1.UserRepository, applicationContext_1.JWTService);
    };
    _FBService.prototype.disconnectFB = function (sollicitatorToken) {
        return __awaiter(this, void 0, void 0, function () {
            var sollicitator, user;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        sollicitator = this.jwtService.verify(sollicitatorToken);
                        return [4 /*yield*/, this.userRepo.findOne({
                                where: {
                                    id: sollicitator.uid
                                }
                            })];
                    case 1:
                        user = _a.sent();
                        user.facebook_id = null;
                        user.facebook_token = null;
                        return [4 /*yield*/, this.userRepo.save(user)];
                    case 2:
                        _a.sent();
                        return [2 /*return*/, true];
                }
            });
        });
    };
    _FBService.prototype.connectFB = function (sollicitatorToken, fbCode) {
        return __awaiter(this, void 0, void 0, function () {
            var sollicitator, reqUri, res, _token, sReqUti, sres, user, _user;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        sollicitator = this.jwtService.verify(sollicitatorToken);
                        reqUri = _FBService.FB_GET_ACCESS_TOKEN
                            + ("?client_id=" + encodeURI(process.env.FB_APP_ID))
                            + ("&redirect_uri=" + encodeURI(process.env.FB_CONNECT_REDIRECT_URI))
                            + ("&client_secret=" + encodeURI(process.env.FB_SECRET))
                            + ("&code=" + fbCode)
                            + "&scope=email,user_friends";
                        return [4 /*yield*/, axios_1.default.request({
                                method: "GET",
                                url: reqUri,
                                validateStatus: function () { return true; }
                            })];
                    case 1:
                        res = _a.sent();
                        if (res.status != 200) {
                            throw new authErrors_1.UnauthorizedError("Invalid code");
                        }
                        _token = res.data.access_token;
                        sReqUti = _FBService.FB_GET_SELF_INFO
                            + ("&access_token=" + encodeURI(_token));
                        return [4 /*yield*/, axios_1.default.request({
                                method: "GET",
                                url: sReqUti,
                                validateStatus: function () { return true; }
                            })];
                    case 2:
                        sres = _a.sent();
                        if (sres.status != 200) {
                            throw new appError_1.AppError("Cannot get data");
                        }
                        return [4 /*yield*/, this.userRepo.findOne({
                                where: {
                                    facebook_id: sres.data.id
                                }
                            })];
                    case 3:
                        user = _a.sent();
                        if (user != null) {
                            throw new authErrors_1.ConflictError("FB User already registerd");
                        }
                        return [4 /*yield*/, this.userRepo.findOne({
                                where: {
                                    id: sollicitator.uid
                                }
                            })];
                    case 4:
                        _user = _a.sent();
                        if (_user.facebook_token || _user.facebook_id) {
                            throw new authErrors_1.ConflictError("User already has a fb account registerd");
                        }
                        _user.facebook_id = sres.data.id;
                        _user.facebook_token = _token;
                        return [4 /*yield*/, this.userRepo.save(_user)];
                    case 5:
                        _a.sent();
                        return [2 /*return*/, true];
                }
            });
        });
    };
    _FBService.prototype.handleFBLogin = function (code) {
        return __awaiter(this, void 0, void 0, function () {
            var reqUri, res, _token, sReqUti, sres, user, tokenContent, nUser, usr, tokenContent;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        reqUri = _FBService.FB_GET_ACCESS_TOKEN
                            + ("?client_id=" + encodeURI(process.env.FB_APP_ID))
                            + ("&redirect_uri=" + encodeURI(process.env.FB_REDIRECT_URI))
                            + ("&client_secret=" + encodeURI(process.env.FB_SECRET))
                            + ("&code=" + code)
                            + "&scope=email,user_friends";
                        return [4 /*yield*/, axios_1.default.request({
                                method: "GET",
                                url: reqUri,
                                validateStatus: function () { return true; }
                            })];
                    case 1:
                        res = _a.sent();
                        if (res.status != 200) {
                            throw new authErrors_1.UnauthorizedError("Invalid code");
                        }
                        _token = res.data.access_token;
                        sReqUti = _FBService.FB_GET_SELF_INFO
                            + ("&access_token=" + encodeURI(_token));
                        return [4 /*yield*/, axios_1.default.request({
                                method: "GET",
                                url: sReqUti,
                                validateStatus: function () { return true; }
                            })];
                    case 2:
                        sres = _a.sent();
                        if (sres.status != 200) {
                            throw new appError_1.AppError("Cannot get data");
                        }
                        return [4 /*yield*/, this.userRepo.findOne({
                                where: {
                                    facebook_id: sres.data.id
                                }
                            })];
                    case 3:
                        user = _a.sent();
                        if (!user) return [3 /*break*/, 5];
                        tokenContent = {
                            email: user.email,
                            uid: user.id,
                            friendly_name: user.friendly_name
                        };
                        user.facebook_token = _token;
                        return [4 /*yield*/, this.userRepo.save(user)];
                    case 4:
                        _a.sent();
                        return [2 /*return*/, this.jwtService.sign(tokenContent)];
                    case 5:
                        nUser = {
                            email: sres.data.email,
                            facebook_id: sres.data.id,
                            facebook_token: _token,
                            friendly_name: sres.data.name
                        };
                        return [4 /*yield*/, this.userRepo.save(nUser)];
                    case 6:
                        usr = _a.sent();
                        tokenContent = {
                            email: usr.email,
                            uid: usr.id,
                            friendly_name: usr.friendly_name,
                        };
                        return [2 /*return*/, this.jwtService.sign(tokenContent)];
                }
            });
        });
    };
    _FBService.prototype.getFriends = function (userID) {
        return __awaiter(this, void 0, void 0, function () {
            var user, sReqUti, ret;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.userRepo.findOne({
                            where: {
                                id: userID
                            }
                        })];
                    case 1:
                        user = _a.sent();
                        if (!user.facebook_id || !user.facebook_token) {
                            return [2 /*return*/, []];
                        }
                        sReqUti = _FBService.FB_GET_FRIENDS
                            + ("&access_token=" + encodeURI(user.facebook_token));
                        return [4 /*yield*/, axios_1.default.request({
                                method: "GET",
                                url: sReqUti,
                                validateStatus: function () { return true; }
                            })];
                    case 2:
                        ret = _a.sent();
                        console.log(sReqUti);
                        if (ret.status == 200) {
                            return [2 /*return*/, ret.data.friends.data.map(function (val) { return val.id; })];
                        }
                        else {
                            throw new authErrors_1.ForbiddenError("Cannot get fb friends");
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    _FBService.FB_GET_ACCESS_TOKEN = "https://graph.facebook.com/v10.0/oauth/access_token";
    _FBService.FB_GET_SELF_INFO = "https://graph.facebook.com/v10.0/me?fields=name%2Cid%2Cemail";
    _FBService.FB_GET_FRIENDS = "https://graph.facebook.com/v10.0/me?fields=id%2Cfriends";
    return _FBService;
}());
exports._FBService = _FBService;
//# sourceMappingURL=FBService.js.map