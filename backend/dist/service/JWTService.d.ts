export interface TokenPayload {
    uid: string;
    email: string;
    friendly_name: string;
}
export declare class _JWTService {
    static getService(): _JWTService;
    secret: string;
    expiration?: number;
    private constructor();
    sign(payload: TokenPayload): string;
    verify(token: string): TokenPayload;
}
//# sourceMappingURL=JWTService.d.ts.map