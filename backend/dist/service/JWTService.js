"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports._JWTService = void 0;
var crypto_1 = __importDefault(require("crypto"));
var jsonwebtoken_1 = __importStar(require("jsonwebtoken"));
var authErrors_1 = require("../errors/authErrors");
var _JWTService = /** @class */ (function () {
    function _JWTService() {
        if (process.env.JWT_SECRET) {
            this.secret = process.env.JWT_SECRET;
        }
        else {
            this.secret = crypto_1.default.randomBytes(30).toString("hex");
        }
        if (process.env.JWT_TTL) {
            this.expiration = parseInt(process.env.JWT_TTL);
        }
    }
    _JWTService.getService = function () {
        return new _JWTService();
    };
    _JWTService.prototype.sign = function (payload) {
        var opt = {};
        if (this.expiration) {
            opt.expiresIn = this.expiration;
        }
        return jsonwebtoken_1.default.sign(payload, this.secret, opt);
    };
    _JWTService.prototype.verify = function (token) {
        try {
            return jsonwebtoken_1.default.verify(token, this.secret);
        }
        catch (err) {
            if (err instanceof jsonwebtoken_1.TokenExpiredError) {
                throw new authErrors_1.UnauthorizedError("Token expired");
            }
            if (err instanceof jsonwebtoken_1.JsonWebTokenError) {
                throw new authErrors_1.UnauthorizedError("Invalid token");
            }
            throw err;
        }
    };
    return _JWTService;
}());
exports._JWTService = _JWTService;
//# sourceMappingURL=JWTService.js.map