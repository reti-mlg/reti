#!/usr/bin/env node
export declare class _RabbitService {
    static getService(): _RabbitService;
    /**
    * The publisher sends a message to the rabbitmq server
    * @param {*} msg string containing the message to send
    * @param {*} exc strign containing the exchange
    * @returns Promise<string> string containing message sent to the server through a specific exchange
    */
    publisher(msg: string, exc: string): Promise<string>;
    /**
     * The susbscriber creates an instance of a channel and a queue given a specific exchange
     * @param exc strign containing the exchange
     * @returns Array containing a channel object and the queue to get messages from
     */
    subscriber(exc: string): Promise<any>;
}
//# sourceMappingURL=RabbitService.d.ts.map