#!/usr/bin/env node
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports._RabbitService = void 0;
var amqp = require("amqplib/callback_api");
var _RabbitService = /** @class */ (function () {
    function _RabbitService() {
    }
    _RabbitService.getService = function () {
        return new _RabbitService();
    };
    /**
    * The publisher sends a message to the rabbitmq server
    * @param {*} msg string containing the message to send
    * @param {*} exc strign containing the exchange
    * @returns Promise<string> string containing message sent to the server through a specific exchange
    */
    _RabbitService.prototype.publisher = function (msg, exc) {
        return new Promise(function (resolve, reject) {
            amqp.connect(process.env.RABBITMQ_SERVER_URL, function (error0, connection) {
                if (error0) {
                    reject(error0);
                }
                connection.createChannel(function (error1, channel) {
                    if (error1) {
                        reject(error1);
                    }
                    var exchange = exc;
                    var message = msg;
                    channel.assertExchange(exchange, "fanout", {
                        durable: false,
                    });
                    channel.publish(exchange, "", Buffer.from(message));
                    resolve(" [x] Sent "
                        .concat(message)
                        .concat(" through rabbit ")
                        .concat(exchange));
                });
                setTimeout(function () {
                    connection.close();
                }, 500);
            });
        });
    };
    /**
     * The susbscriber creates an instance of a channel and a queue given a specific exchange
     * @param exc strign containing the exchange
     * @returns Array containing a channel object and the queue to get messages from
     */
    _RabbitService.prototype.subscriber = function (exc) {
        return new Promise(function (resolve, reject) {
            amqp.connect(process.env.RABBITMQ_SERVER_URL, function (error0, connection) {
                if (error0) {
                    reject(error0);
                }
                connection.createChannel(function (error1, channel) {
                    if (error1) {
                        reject(error1);
                    }
                    var exchange = exc;
                    channel.assertExchange(exchange, "fanout", {
                        durable: false,
                    });
                    channel.assertQueue("", {
                        exclusive: true,
                    }, function (error2, q) {
                        if (error2) {
                            reject(error2);
                        }
                        /* console.log(
                            " [*] Waiting for messages in %s. To exit press CTRL+C",
                            exchange
                        ); */
                        channel.bindQueue(q.queue, exchange, "");
                        resolve([channel, q.queue]);
                    });
                });
            });
        });
    };
    return _RabbitService;
}());
exports._RabbitService = _RabbitService;
//# sourceMappingURL=RabbitService.js.map