export declare class _TwitterService {
    private userRepo;
    private jwtService;
    private twtCredRepo;
    static getService(): _TwitterService;
    private consumer_key;
    private consumer_key_secert;
    private constructor();
    private trueEncode;
    private oauthRequest;
    private oauthSign;
    private requireToken;
    private getCredentials;
    private accessToken;
    createToken(): Promise<string>;
    createTokenConnect(): Promise<string>;
    twitterLogin(token: string, verifier: string): Promise<string>;
    twitterDisconnect(sollicitatorToken: string): Promise<boolean>;
    twitterConnect(sollicitatorToken: string, token: string, verifier: string): Promise<boolean>;
    getFriends(userID: string): Promise<number[]>;
}
//# sourceMappingURL=TwitterService.d.ts.map