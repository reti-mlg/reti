"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports._TwitterService = void 0;
var applicationContext_1 = require("../applicationContext");
var util_1 = require("../util");
var crypto_1 = __importDefault(require("crypto"));
var axios_1 = __importDefault(require("axios"));
var authErrors_1 = require("../errors/authErrors");
var appError_1 = require("../errors/appError");
var querystring_1 = __importDefault(require("querystring"));
var _TwitterService = /** @class */ (function () {
    function _TwitterService(userRepo, jwtService, twtCredRepo) {
        this.userRepo = userRepo;
        this.jwtService = jwtService;
        this.twtCredRepo = twtCredRepo;
        this.consumer_key = process.env.TWITTER_CONSUMER_KEY;
        this.consumer_key_secert = process.env.TWITTER_CONSUMER_SECRET;
    }
    _TwitterService.getService = function () {
        return new _TwitterService(applicationContext_1.UserRepository, applicationContext_1.JWTService, applicationContext_1.TwitterCredentialsRepository);
    };
    _TwitterService.prototype.trueEncode = function (str) {
        var allowed = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-._~";
        var dst = "";
        for (var i = 0; i < str.length; i++) {
            var c = str.charAt(i);
            if (allowed.includes(c)) {
                dst += c;
            }
            else {
                dst += "%" + str.charCodeAt(i).toString(16).toUpperCase();
            }
        }
        return dst;
    };
    _TwitterService.prototype.oauthRequest = function (params) {
        var oauth_timestamp = Math.floor(Date.now() / 1000).toString();
        var oauth_nonce = util_1.getRandomString();
        var oauth_signature_method = "HMAC-SHA1";
        var oauth_consumer_key = params.consumerKey;
        var oauth_version = "1.0";
        if (!params.params) {
            params.params = {};
        }
        params.params["oauth_timestamp"] = oauth_timestamp;
        params.params["oauth_nonce"] = oauth_nonce;
        params.params["oauth_signature_method"] = oauth_signature_method;
        params.params["oauth_consumer_key"] = oauth_consumer_key;
        params.params["oauth_version"] = oauth_version;
        if (params.callback) {
            params.params["oauth_callback"] = params.callback;
        }
        if (params.tokenKey) {
            params.params["oauth_token"] = params.tokenKey;
        }
        if (params.verifier) {
            params.params["oauth_verifier"] = params.verifier;
        }
        var oauth_signature = this.oauthSign(params);
        var dst = "OAuth ";
        dst += this.trueEncode("oauth_timestamp") + "=\"" + this.trueEncode(oauth_timestamp) + "\",";
        dst += this.trueEncode("oauth_nonce") + "=\"" + this.trueEncode(oauth_nonce) + "\",";
        dst += this.trueEncode("oauth_signature_method") + "=\"" + this.trueEncode(oauth_signature_method) + "\",";
        dst += this.trueEncode("oauth_consumer_key") + "=\"" + this.trueEncode(oauth_consumer_key) + "\",";
        dst += this.trueEncode("oauth_version") + "=\"" + this.trueEncode(oauth_version) + "\",";
        if (params.verifier) {
            dst += this.trueEncode("oauth_verifier") + "=\"" + this.trueEncode(params.verifier) + "\",";
        }
        if (params.tokenKey) {
            dst += this.trueEncode("oauth_token") + "=\"" + this.trueEncode(params.tokenKey) + "\",";
        }
        dst += this.trueEncode("oauth_signature") + "=\"" + this.trueEncode(oauth_signature) + "\"";
        if (params.callback) {
            dst += "," + this.trueEncode("oauth_callback") + "=\"" + this.trueEncode(params.callback) + "\"";
        }
        return dst;
    };
    _TwitterService.prototype.oauthSign = function (params) {
        var ordered = {};
        Object.keys(params.params).sort().forEach(function (key) {
            ordered[key] = params.params[key];
        });
        var encodedParams = '';
        for (var k in ordered) {
            var encodedValue = this.trueEncode(ordered[k]);
            var encodedKey = this.trueEncode(k);
            if (encodedParams === '') {
                encodedParams += encodedKey + "=" + encodedValue;
            }
            else {
                encodedParams += "&" + encodedKey + "=" + encodedValue;
            }
        }
        var base_string = params.method.toUpperCase() + "&" + this.trueEncode(params.baseURL) + "&" + this.trueEncode(encodedParams);
        var sign_key = params.consumerSecret + "&" + (params.tokenSecret || "");
        return crypto_1.default.createHmac("sha1", sign_key).update(base_string).digest().toString('base64');
    };
    _TwitterService.prototype.requireToken = function (callback) {
        return __awaiter(this, void 0, void 0, function () {
            var method, baseString, oauthAuth, ret;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        method = "POST";
                        baseString = "https://api.twitter.com/oauth/request_token";
                        oauthAuth = this.oauthRequest({
                            method: method,
                            baseURL: baseString,
                            callback: callback || process.env.TWITTER_CALLBACK_URI,
                            consumerKey: process.env.TWITTER_CONSUMER_KEY,
                            consumerSecret: process.env.TWITTER_CONSUMER_SECRET
                        });
                        return [4 /*yield*/, axios_1.default.request({
                                method: method,
                                url: baseString,
                                headers: {
                                    "Authorization": oauthAuth
                                },
                                validateStatus: function () { return true; }
                            })];
                    case 1:
                        ret = _a.sent();
                        if (ret.status === 200) {
                            /*const kVal = {}
                            ret.data.split("&").forEach((val)=>{
                                const t = val.split("=")
                                kVal[t[0]] = t[1]
                            })
                            kVal["oauth_callback_confirmed"] = kVal["oauth_callback_confirmed"] === "true"*/
                            return [2 /*return*/, querystring_1.default.parse(ret.data)
                                //return kVal as oauth_token_req_ret
                            ];
                            //return kVal as oauth_token_req_ret
                        }
                        else {
                            throw new appError_1.AppError("Cannot gather oauth token");
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    _TwitterService.prototype.getCredentials = function (token_key, token_secret) {
        return __awaiter(this, void 0, void 0, function () {
            var method, baseURL, urlParams, params, oauthAuth, ret;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        method = "GET";
                        baseURL = "https://api.twitter.com/1.1/account/verify_credentials.json";
                        urlParams = "?include_email=true";
                        params = {
                            include_email: "true"
                        };
                        oauthAuth = this.oauthRequest({
                            method: method,
                            baseURL: baseURL,
                            params: params,
                            tokenKey: token_key,
                            tokenSecret: token_secret,
                            consumerKey: process.env.TWITTER_CONSUMER_KEY,
                            consumerSecret: process.env.TWITTER_CONSUMER_SECRET
                        });
                        return [4 /*yield*/, axios_1.default.request({
                                method: method,
                                url: baseURL + urlParams,
                                headers: {
                                    "Authorization": oauthAuth
                                },
                                validateStatus: function () { return true; }
                            })];
                    case 1:
                        ret = _a.sent();
                        if (ret.status === 200) {
                            return [2 /*return*/, ret.data];
                        }
                        else {
                            throw new authErrors_1.UnauthorizedError("Cannot gather access token");
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    _TwitterService.prototype.accessToken = function (token_key, token_secret, verifier) {
        return __awaiter(this, void 0, void 0, function () {
            var method, baseString, oauthAuth, ret;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        method = "POST";
                        baseString = "https://api.twitter.com/oauth/access_token";
                        oauthAuth = this.oauthRequest({
                            method: method,
                            baseURL: baseString,
                            consumerKey: process.env.TWITTER_CONSUMER_KEY,
                            consumerSecret: process.env.TWITTER_CONSUMER_SECRET,
                            tokenKey: token_key,
                            tokenSecret: token_secret,
                            verifier: verifier
                        });
                        return [4 /*yield*/, axios_1.default.request({
                                method: method,
                                url: baseString,
                                headers: {
                                    "Authorization": oauthAuth,
                                    "Content-type": "application/x-www-form-urlencode"
                                },
                                data: "oauth_verifier=" + verifier,
                                validateStatus: function () { return true; }
                            })];
                    case 1:
                        ret = _a.sent();
                        if (ret.status === 200) {
                            return [2 /*return*/, querystring_1.default.parse(ret.data)];
                        }
                        else {
                            throw new authErrors_1.UnauthorizedError("Cannot gather access token");
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    _TwitterService.prototype.createToken = function () {
        return __awaiter(this, void 0, void 0, function () {
            var token, tct, h;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.requireToken()];
                    case 1:
                        token = _a.sent();
                        tct = {
                            token_key: token.oauth_token,
                            token_secret: token.oauth_token_secret
                        };
                        return [4 /*yield*/, this.twtCredRepo.save(tct)];
                    case 2:
                        h = _a.sent();
                        return [2 /*return*/, h.token_key];
                }
            });
        });
    };
    _TwitterService.prototype.createTokenConnect = function () {
        return __awaiter(this, void 0, void 0, function () {
            var token, tct, h;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.requireToken(process.env.TWITTER_CONNECT_CALLBACK_URI)];
                    case 1:
                        token = _a.sent();
                        tct = {
                            token_key: token.oauth_token,
                            token_secret: token.oauth_token_secret
                        };
                        return [4 /*yield*/, this.twtCredRepo.save(tct)];
                    case 2:
                        h = _a.sent();
                        return [2 /*return*/, h.token_key];
                }
            });
        });
    };
    _TwitterService.prototype.twitterLogin = function (token, verifier) {
        return __awaiter(this, void 0, void 0, function () {
            var tct, responce, user, creds, nUser, tokenContent;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.twtCredRepo.findOne({
                            where: {
                                token_key: token,
                                verified: false
                            }
                        })];
                    case 1:
                        tct = _a.sent();
                        if (!tct) {
                            throw new authErrors_1.UnauthorizedError("Invalid token or verifier");
                        }
                        return [4 /*yield*/, this.accessToken(tct.token_key, tct.token_secret, verifier)];
                    case 2:
                        responce = _a.sent();
                        return [4 /*yield*/, this.userRepo.findOne({
                                where: {
                                    twitter_id: responce.user_id
                                }
                            })];
                    case 3:
                        user = _a.sent();
                        if (!!user) return [3 /*break*/, 6];
                        return [4 /*yield*/, this.getCredentials(responce.oauth_token, responce.oauth_token_secret)];
                    case 4:
                        creds = _a.sent();
                        nUser = {
                            email: creds.email,
                            friendly_name: responce.screen_name,
                            twitter_id: responce.user_id,
                        };
                        return [4 /*yield*/, this.userRepo.save(nUser)];
                    case 5:
                        user = _a.sent();
                        return [3 /*break*/, 7];
                    case 6:
                        this.twtCredRepo.delete({
                            userId: user.id
                        });
                        _a.label = 7;
                    case 7:
                        tct.token_key = responce.oauth_token;
                        tct.token_secret = responce.oauth_token_secret;
                        tct.verified = true;
                        tct.userId = user.id;
                        return [4 /*yield*/, this.twtCredRepo.save(tct)];
                    case 8:
                        _a.sent();
                        tokenContent = {
                            email: user.email,
                            uid: user.id,
                            friendly_name: user.friendly_name,
                        };
                        return [2 /*return*/, this.jwtService.sign(tokenContent)];
                }
            });
        });
    };
    _TwitterService.prototype.twitterDisconnect = function (sollicitatorToken) {
        return __awaiter(this, void 0, void 0, function () {
            var sollicitator, user;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        sollicitator = this.jwtService.verify(sollicitatorToken);
                        return [4 /*yield*/, this.userRepo.findOne({
                                where: {
                                    id: sollicitator.uid
                                }
                            })];
                    case 1:
                        user = _a.sent();
                        user.twitter_id = null;
                        return [4 /*yield*/, this.userRepo.save(user)];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, this.twtCredRepo.delete({
                                userId: user.id
                            })];
                    case 3:
                        _a.sent();
                        return [2 /*return*/, true];
                }
            });
        });
    };
    _TwitterService.prototype.twitterConnect = function (sollicitatorToken, token, verifier) {
        return __awaiter(this, void 0, void 0, function () {
            var sollicitator, user, tct, responce, _user;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        sollicitator = this.jwtService.verify(sollicitatorToken);
                        return [4 /*yield*/, this.userRepo.findOne({
                                where: {
                                    id: sollicitator.uid
                                }
                            })];
                    case 1:
                        user = _a.sent();
                        if (user.twitter_id) {
                            throw new authErrors_1.ConflictError("User already has twitter connected");
                        }
                        return [4 /*yield*/, this.twtCredRepo.findOne({
                                where: {
                                    token_key: token,
                                    verified: false
                                }
                            })];
                    case 2:
                        tct = _a.sent();
                        if (!tct) {
                            throw new authErrors_1.UnauthorizedError("Invalid token or verifier");
                        }
                        return [4 /*yield*/, this.accessToken(tct.token_key, tct.token_secret, verifier)];
                    case 3:
                        responce = _a.sent();
                        return [4 /*yield*/, this.userRepo.findOne({
                                where: {
                                    twitter_id: responce.user_id
                                }
                            })];
                    case 4:
                        _user = _a.sent();
                        if (_user) {
                            throw new authErrors_1.ConflictError("Twitter user is already registerd");
                        }
                        tct.token_key = responce.oauth_token;
                        tct.token_secret = responce.oauth_token_secret;
                        tct.verified = true;
                        tct.userId = user.id;
                        user.twitter_id = responce.user_id;
                        return [4 /*yield*/, this.twtCredRepo.save(tct)];
                    case 5:
                        _a.sent();
                        return [4 /*yield*/, this.userRepo.save(user)];
                    case 6:
                        _a.sent();
                        return [2 /*return*/, true];
                }
            });
        });
    };
    _TwitterService.prototype.getFriends = function (userID) {
        return __awaiter(this, void 0, void 0, function () {
            var creds, method, baseURL, oauthAuth, ret;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.twtCredRepo.findOne({
                            where: {
                                userId: userID
                            }
                        })];
                    case 1:
                        creds = _a.sent();
                        if (!creds) {
                            return [2 /*return*/, []];
                        }
                        method = "GET";
                        baseURL = "https://api.twitter.com/1.1/friends/ids.json";
                        oauthAuth = this.oauthRequest({
                            method: method,
                            baseURL: baseURL,
                            tokenKey: creds.token_key,
                            tokenSecret: creds.token_secret,
                            consumerKey: process.env.TWITTER_CONSUMER_KEY,
                            consumerSecret: process.env.TWITTER_CONSUMER_SECRET
                        });
                        return [4 /*yield*/, axios_1.default.request({
                                method: method,
                                url: baseURL,
                                headers: {
                                    "Authorization": oauthAuth
                                }
                            })];
                    case 2:
                        ret = _a.sent();
                        if (ret.status == 200) {
                            return [2 /*return*/, ret.data.ids];
                        }
                        else {
                            throw new authErrors_1.ForbiddenError("Cannot get friends");
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    return _TwitterService;
}());
exports._TwitterService = _TwitterService;
//# sourceMappingURL=TwitterService.js.map