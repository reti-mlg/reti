/// <reference types="node" />
import { Preferito } from "../persistence/entities/preferito.entity";
export interface UserPublic {
    id: string;
    icon?: string;
    friendly_name?: string;
    facebook_id?: string;
    twitter_id?: string;
    prefs: Preferito[];
}
export declare enum FriendSource {
    facebook = 1,
    twitter = 2
}
export interface Friend {
    user: UserPublic;
    source: FriendSource;
}
export interface UserPrivate extends UserPublic {
    email?: string;
    friends: Friend[];
}
export declare class _UserService {
    private userRepo;
    private prefRepo;
    private jwtService;
    private twitterService;
    private fbService;
    static getService(): _UserService;
    private constructor();
    private mapUserToPublicView;
    private mapUserToPrivateView;
    getFriendList(sollicitatorToken: string): Promise<Friend[]>;
    getUser(sollicitatorToken: string, id: string): Promise<UserPublic>;
    getSelf(sollicitatorToken: string): Promise<UserPrivate>;
    updateFName(sollicitatorToken: string, userid: string, fName: string): Promise<UserPrivate>;
    uploadAvatar(sollicitatorToken: string, fileName: string, file: Buffer): Promise<boolean>;
}
//# sourceMappingURL=UserService.d.ts.map