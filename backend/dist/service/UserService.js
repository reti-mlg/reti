"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports._UserService = exports.FriendSource = void 0;
var typeorm_1 = require("typeorm");
var applicationContext_1 = require("../applicationContext");
var appError_1 = require("../errors/appError");
var authErrors_1 = require("../errors/authErrors");
var fs_1 = __importDefault(require("fs"));
var util_1 = require("../util");
var FriendSource;
(function (FriendSource) {
    FriendSource[FriendSource["facebook"] = 1] = "facebook";
    FriendSource[FriendSource["twitter"] = 2] = "twitter";
})(FriendSource = exports.FriendSource || (exports.FriendSource = {}));
var _UserService = /** @class */ (function () {
    function _UserService(userRepo, prefRepo, jwtService, twitterService, fbService) {
        this.userRepo = userRepo;
        this.prefRepo = prefRepo;
        this.jwtService = jwtService;
        this.twitterService = twitterService;
        this.fbService = fbService;
    }
    _UserService.getService = function () {
        return new _UserService(applicationContext_1.UserRepository, applicationContext_1.PreferitoRepository, applicationContext_1.JWTService, applicationContext_1.TwitterService, applicationContext_1.FBService);
    };
    _UserService.prototype.mapUserToPublicView = function (user, prefs) {
        var t = {
            id: user.id,
            friendly_name: user.friendly_name,
            icon: user.icon,
            facebook_id: user.facebook_id,
            twitter_id: user.twitter_id,
            prefs: prefs
        };
        return t;
    };
    _UserService.prototype.mapUserToPrivateView = function (user, prefs, firends) {
        var t = this.mapUserToPublicView(user, prefs);
        t.email = user.email;
        t.friends = firends;
        return t;
    };
    _UserService.prototype.getFriendList = function (sollicitatorToken) {
        return __awaiter(this, void 0, void 0, function () {
            var sollicitator, twitterFirends, ret, users, users_1, users_1_1, user, prefs, e_1_1, fbFirends, users, users_2, users_2_1, user, prefs, e_2_1;
            var e_1, _a, e_2, _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        sollicitator = this.jwtService.verify(sollicitatorToken);
                        return [4 /*yield*/, this.twitterService.getFriends(sollicitator.uid)];
                    case 1:
                        twitterFirends = _c.sent();
                        ret = [];
                        if (!(twitterFirends.length > 0)) return [3 /*break*/, 10];
                        return [4 /*yield*/, this.userRepo.find({
                                where: {
                                    twitter_id: typeorm_1.In(twitterFirends.map(function (v) { return String(v); }))
                                }
                            })];
                    case 2:
                        users = _c.sent();
                        _c.label = 3;
                    case 3:
                        _c.trys.push([3, 8, 9, 10]);
                        users_1 = __values(users), users_1_1 = users_1.next();
                        _c.label = 4;
                    case 4:
                        if (!!users_1_1.done) return [3 /*break*/, 7];
                        user = users_1_1.value;
                        return [4 /*yield*/, this.prefRepo.find({
                                where: {
                                    userId: user.id
                                }
                            })];
                    case 5:
                        prefs = _c.sent();
                        ret.push({
                            source: FriendSource.twitter,
                            user: this.mapUserToPublicView(user, prefs)
                        });
                        _c.label = 6;
                    case 6:
                        users_1_1 = users_1.next();
                        return [3 /*break*/, 4];
                    case 7: return [3 /*break*/, 10];
                    case 8:
                        e_1_1 = _c.sent();
                        e_1 = { error: e_1_1 };
                        return [3 /*break*/, 10];
                    case 9:
                        try {
                            if (users_1_1 && !users_1_1.done && (_a = users_1.return)) _a.call(users_1);
                        }
                        finally { if (e_1) throw e_1.error; }
                        return [7 /*endfinally*/];
                    case 10: return [4 /*yield*/, this.fbService.getFriends(sollicitator.uid)];
                    case 11:
                        fbFirends = _c.sent();
                        if (!(fbFirends.length > 0)) return [3 /*break*/, 20];
                        return [4 /*yield*/, this.userRepo.find({
                                where: {
                                    facebook_id: typeorm_1.In(fbFirends)
                                }
                            })];
                    case 12:
                        users = _c.sent();
                        _c.label = 13;
                    case 13:
                        _c.trys.push([13, 18, 19, 20]);
                        users_2 = __values(users), users_2_1 = users_2.next();
                        _c.label = 14;
                    case 14:
                        if (!!users_2_1.done) return [3 /*break*/, 17];
                        user = users_2_1.value;
                        return [4 /*yield*/, this.prefRepo.find({
                                where: {
                                    userId: user.id
                                }
                            })];
                    case 15:
                        prefs = _c.sent();
                        ret.push({
                            source: FriendSource.facebook,
                            user: this.mapUserToPublicView(user, prefs)
                        });
                        _c.label = 16;
                    case 16:
                        users_2_1 = users_2.next();
                        return [3 /*break*/, 14];
                    case 17: return [3 /*break*/, 20];
                    case 18:
                        e_2_1 = _c.sent();
                        e_2 = { error: e_2_1 };
                        return [3 /*break*/, 20];
                    case 19:
                        try {
                            if (users_2_1 && !users_2_1.done && (_b = users_2.return)) _b.call(users_2);
                        }
                        finally { if (e_2) throw e_2.error; }
                        return [7 /*endfinally*/];
                    case 20: return [2 /*return*/, ret];
                }
            });
        });
    };
    _UserService.prototype.getUser = function (sollicitatorToken, id) {
        return __awaiter(this, void 0, void 0, function () {
            var sollicitator, tUser, prefs, _a, _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        sollicitator = this.jwtService.verify(sollicitatorToken);
                        return [4 /*yield*/, this.userRepo.findOne({
                                where: {
                                    id: id
                                }
                            })];
                    case 1:
                        tUser = _c.sent();
                        if (!tUser) {
                            throw new appError_1.NotFoundError("User not found");
                        }
                        return [4 /*yield*/, this.prefRepo.find({
                                where: {
                                    userId: id
                                }
                            })];
                    case 2:
                        prefs = _c.sent();
                        if (!(sollicitator.uid != tUser.id)) return [3 /*break*/, 3];
                        return [2 /*return*/, this.mapUserToPublicView(tUser, prefs)];
                    case 3:
                        _a = this.mapUserToPrivateView;
                        _b = [tUser, prefs];
                        return [4 /*yield*/, this.getFriendList(sollicitatorToken)];
                    case 4: return [2 /*return*/, _a.apply(this, _b.concat([_c.sent()]))];
                }
            });
        });
    };
    _UserService.prototype.getSelf = function (sollicitatorToken) {
        return __awaiter(this, void 0, void 0, function () {
            var sollicitator;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        sollicitator = this.jwtService.verify(sollicitatorToken);
                        return [4 /*yield*/, this.getUser(sollicitatorToken, sollicitator.uid)];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    _UserService.prototype.updateFName = function (sollicitatorToken, userid, fName) {
        return __awaiter(this, void 0, void 0, function () {
            var sollicitator;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        sollicitator = this.jwtService.verify(sollicitatorToken);
                        if (sollicitator.uid != userid) {
                            throw new authErrors_1.ForbiddenError("Canno't update name of another user");
                        }
                        return [4 /*yield*/, this.userRepo.update({
                                id: userid
                            }, {
                                friendly_name: fName
                            })];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.getSelf(sollicitatorToken)];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    _UserService.prototype.uploadAvatar = function (sollicitatorToken, fileName, file) {
        return __awaiter(this, void 0, void 0, function () {
            var sollicitator, user, h, nFileName;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        sollicitator = this.jwtService.verify(sollicitatorToken);
                        return [4 /*yield*/, this.userRepo.findOne({
                                where: {
                                    id: sollicitator.uid
                                }
                            })];
                    case 1:
                        user = _a.sent();
                        if (user.icon && fs_1.default.existsSync(process.env.IMAGES_DIR + user.icon)) {
                            fs_1.default.unlinkSync(process.env.IMAGES_DIR + user.icon);
                        }
                        h = fileName.split(".");
                        nFileName = Date.now() + util_1.getRandomString(5) + ("." + h[h.length - 1]);
                        fs_1.default.writeFileSync(process.env.IMAGES_DIR + nFileName, file);
                        user.icon = nFileName;
                        return [4 /*yield*/, this.userRepo.save(user)];
                    case 2:
                        _a.sent();
                        return [2 /*return*/, true];
                }
            });
        });
    };
    return _UserService;
}());
exports._UserService = _UserService;
//# sourceMappingURL=UserService.js.map