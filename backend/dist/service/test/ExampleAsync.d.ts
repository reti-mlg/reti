export declare class _ExampleService {
    private asyncDep;
    static getService(): Promise<_ExampleService>;
    private constructor();
    getSomething(): string;
}
//# sourceMappingURL=ExampleAsync.d.ts.map