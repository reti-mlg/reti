"use strict";
/*
 * Created on Wed May 12 2021
 *
 * Author: Gioele Zacchia
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports._ExampleService = void 0;
var _ExampleService = /** @class */ (function () {
    function _ExampleService(asyncDep) {
        this.asyncDep = asyncDep;
    }
    _ExampleService.getService = function () {
        //Service init function. We can use initilized dependecy from Application Context
        return new Promise(function (resolve, reject) {
            setTimeout(function () {
                resolve(new _ExampleService("Una dipendenza asincrona")); //Simulate async dependency creation
            }, 3000);
        });
    };
    _ExampleService.prototype.getSomething = function () { return this.asyncDep; };
    return _ExampleService;
}());
exports._ExampleService = _ExampleService;
//# sourceMappingURL=ExampleAsync.js.map