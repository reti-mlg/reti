export declare class _ExampleNestedService {
    private exampleService;
    static getService(): Promise<_ExampleNestedService>;
    private constructor();
    getSomething(): string;
}
//# sourceMappingURL=ExampleNested.d.ts.map