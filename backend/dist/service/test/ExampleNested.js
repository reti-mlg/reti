"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports._ExampleNestedService = void 0;
var applicationContext_1 = require("../../applicationContext");
var _ExampleNestedService = /** @class */ (function () {
    //We couild also use the example service directly without embedding it in the object but i think this is a cleaner solution
    function _ExampleNestedService(exampleService) {
        this.exampleService = exampleService;
    }
    _ExampleNestedService.getService = function () {
        //Service init function. We can use initilized dependecy from Application Context
        return new Promise(function (resolve, reject) {
            if (!applicationContext_1.ExampleService) {
                //Probably a good idea to check if the required service is actualy initilized correctly
                reject("Example Service not initilized");
            }
            setTimeout(function () {
                resolve(new _ExampleNestedService(applicationContext_1.ExampleService)); //We use initilized service from application context
            }, 1000);
        });
    };
    _ExampleNestedService.prototype.getSomething = function () { return this.exampleService.getSomething() + " e sono anche annidato!"; };
    return _ExampleNestedService;
}());
exports._ExampleNestedService = _ExampleNestedService;
//# sourceMappingURL=ExampleNested.js.map