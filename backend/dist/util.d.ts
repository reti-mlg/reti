/// <reference types="node" />
import express from 'express';
export declare const getRandomString: (len?: number) => string;
export declare const buffersEqual: (A: Buffer, B: Buffer) => boolean;
export declare const getTokenFromRequest: (req: express.Request) => string;
//# sourceMappingURL=util.d.ts.map