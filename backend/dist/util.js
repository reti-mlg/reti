"use strict";
/*
 * Created on Tue May 18 2021
 *
 * Author: Gioele Zacchia
 */
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getTokenFromRequest = exports.buffersEqual = exports.getRandomString = void 0;
var crypto_1 = __importDefault(require("crypto"));
var authErrors_1 = require("./errors/authErrors");
var alphabet = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890";
var getRandomString = function (len) {
    if (len === void 0) { len = 30; }
    var seed = crypto_1.default.randomBytes(len);
    var ret = "";
    for (var i = 0; i < len; i++) {
        var index = Math.round((seed[i] / 0xFF) * (alphabet.length - 1));
        ret += alphabet[index];
    }
    return ret;
};
exports.getRandomString = getRandomString;
var buffersEqual = function (A, B) {
    if (A.length != B.length) {
        return false;
    }
    for (var i = 0; i < A.length; i++) {
        if (A[i] != B[i]) {
            return false;
        }
    }
    return true;
};
exports.buffersEqual = buffersEqual;
var getTokenFromRequest = function (req) {
    var auth = req.headers.authorization;
    if (!auth || !auth.startsWith("Bearer ")) {
        throw new authErrors_1.UnauthorizedError("Invalid authorization header");
    }
    return auth.substring("Bearer ".length);
};
exports.getTokenFromRequest = getTokenFromRequest;
//# sourceMappingURL=util.js.map