/*
 * Created on Wed May 12 2021
 *
 * Author: Gioele Zacchia
 */

import express from 'express'
import dotenv from 'dotenv'
import morgan from 'morgan'
import cors from 'cors'
import { ProvideContext } from './applicationContext'
import { AuthTestEndpointProvider } from './endpoint/test/authTest'
import { AppErrorHandler } from './errors/errorHandlers'
import { EPAuthEndpointRegister } from './endpoint/auth/epAuthEndpoint'
import { SocialAuthEndpointRegister } from './endpoint/auth/socialLoginEndpoints'
import { UserEndpointRegister } from './endpoint/user/userEndpoint'
import { ChatEndPoint } from './endpoint/chat/chat'
import fs from 'fs'
import { BookEndpointRegister } from './endpoint/book/bookEndpoint'
import https from 'https'
import helmet from 'helmet'


export let useHTTPS:boolean

type ExpressEndpointRegister = (app:express.Application)=>void

enum ConsoleColorEscape{
    RESET = "\x1b[0m",
    BLACK = "\x1b[30m",
    YELLOW = "\x1b[33m",
    BLUE = "\x1b[34m",
    MAGENTA = "\x1b[35m",
    RED = "\x1b[31m",
    GREEN = "\x1b[32m",
    CYAN = "\x1b[36m",
    WHITE = "\x1b[37m",

}

dotenv.config()
process.env.NODE_ENV || 'development';

const logFormat = `${ConsoleColorEscape.BLUE}:date ${ConsoleColorEscape.MAGENTA}:method ${ConsoleColorEscape.RESET}:url --${ConsoleColorEscape.MAGENTA} :status ${ConsoleColorEscape.RESET} Total time: :total-time ms`




//Add endpoints register here
const registers:ExpressEndpointRegister[] = [
//    ExampleEndpointRegister,
    AuthTestEndpointProvider,
    EPAuthEndpointRegister,
    SocialAuthEndpointRegister,
    UserEndpointRegister,
    ChatEndPoint,
    BookEndpointRegister

]

const isValid = (val:any)=>val?true:false
const isTrue = (val:string)=>{
    if(val){
        val = val.toLocaleLowerCase()
        if(val === "true" || val === "yes" || val === "y"){
            return true
        }
    }
    return false
}

ProvideContext().then(()=>{
    const app = express()
    const port = process.env.PORT || 8080
    const httpsPort = process.env.HTTPS_PORT || 8081

    useHTTPS = isTrue(process.env.USE_HTTPS) && isValid(process.env.HTTPS_KEY_FILE) && isValid(process.env.HTTPS_CERT_FILE)

    if(useHTTPS){
        app.use(helmet())
    }

    app.use(express.json())
    app.use(cors())
    app.use(morgan(logFormat))
    if(!fs.existsSync(process.env.IMAGES_DIR)){
        fs.mkdirSync(process.env.IMAGES_DIR)
    }
    app.use("/static",express.static(process.env.IMAGES_DIR))
    
    for(let register of registers){
        console.log("Registring endpoint: ",register)
        register(app)
    }
    app.use(AppErrorHandler)
    app.listen(port,()=>{
        console.log(`Server listening at ${ConsoleColorEscape.GREEN}http://localhost:${port} 😎${ConsoleColorEscape.RESET}`)
    })
    if(useHTTPS){
        https.createServer({
            key: fs.readFileSync(process.env.HTTPS_KEY_FILE),
            cert: fs.readFileSync(process.env.HTTPS_CERT_FILE),
            dhparam: process.env.HTTPS_DHPARAM_FILE?fs.readFileSync(process.env.HTTPS_DHPARAM_FILE):undefined
        },app).listen(httpsPort)
    }
})
