/*
 * Created on Wed May 12 2021
 *
 * Author: Gioele Zacchia
 */

import { Repository } from "typeorm"
import { Commento } from "./persistence/entities/commento.entity"
import { Preferito } from "./persistence/entities/preferito.entity"
import { TwitterCredentials } from "./persistence/entities/twitterCreds.entity"
import { User } from "./persistence/entities/user.entity"
import { _PersistenceProvider } from "./persistence/Persistence"
import { _AuthService } from "./service/AuthService"
import { _BookService } from "./service/BookService"
import { _FBService } from "./service/FBService"
import { _JWTService } from "./service/JWTService"
import { _RabbitService } from "./service/RabbitService"
import { _ExampleService } from "./service/test/ExampleAsync"
import { _ExampleNestedService } from "./service/test/ExampleNested"
import { _TwitterService } from "./service/TwitterService"
import { _UserService } from "./service/UserService"


//List of exported dependencies, they need to be initialized in the ProvideContext function
export let ExampleService:_ExampleService
export let ExampleNestedService:_ExampleNestedService

export let PersistenceProvider:_PersistenceProvider
export let UserRepository:Repository<User>
export let CommentoRepository:Repository<Commento>
export let PreferitoRepository:Repository<Preferito>
export let TwitterCredentialsRepository:Repository<TwitterCredentials>

export let JWTService:_JWTService
export let AuthService:_AuthService
export let UserService:_UserService
export let FBService:_FBService
export let TwitterService:_TwitterService

export let RabbitService:_RabbitService

export let BookService:_BookService

export const ProvideContext = async ()=>{
    //We initialize each dependency explicitly
    //Sequence is important, if service B depends on service A then A needs to be initialized before B
//    console.log("Initilizing Example Service...")
//    ExampleService = await _ExampleService.getService()
//    console.log("Initilizing Example Nested Service...")
//    ExampleNestedService = await _ExampleNestedService.getService()

    console.log("Initilizing Persistence level dependencies...")
    PersistenceProvider = await _PersistenceProvider.getService(_PersistenceProvider.getConfigFromEnv())
    UserRepository = PersistenceProvider.getConnection().getRepository(User)
    CommentoRepository = PersistenceProvider.getConnection().getRepository(Commento)
    PreferitoRepository = PersistenceProvider.getConnection().getRepository(Preferito)
    TwitterCredentialsRepository = PersistenceProvider.getConnection().getRepository(TwitterCredentials)

    console.log("Initilizing servicies...")
    
    JWTService = _JWTService.getService()
    FBService = _FBService.getService()
    TwitterService = _TwitterService.getService()
    AuthService = await _AuthService.getService()
    UserService = _UserService.getService();

    RabbitService = await _RabbitService.getService()

    BookService = _BookService.getService()
    
    //console.log((await TwitterService.requireToken()))
    //console.log(await TwitterService.requireToken())
    
}