import express from 'express'
import {body, validationResult} from 'express-validator'
import { AuthService } from '../../applicationContext'
import { HttpStatus, RequestValidationError } from '../../errors/appError'
import { CombineWithErrorHandler } from '../../errors/errorHandlers'


export const EPAuthEndpointRegister = (app:express.Application) => {
    /**
     * @api {post} /login Login with email and password
     * @apiName EPLogin
     * @apiGroup Auth
     *
     *
     * @apiParam {String} email User email
     * @apiParam {String} password User password
     * 
     * @apiParamExample {json} Request-example:
     *      {
     *          "email":"test@example.com",
     *          "password":"1234"
     *      }
     * 
     * @apiError {Status} 401 Invalid login credentials
     * 
     * @apiSuccess {String} token Session token
     */
    app.post("/login",
        body("email").isString().normalizeEmail(),
        body("password").isString(),
        CombineWithErrorHandler(async (req,res)=>{
            const errors = validationResult(req)
            if(!errors.isEmpty()){
                throw new RequestValidationError(errors)
            }
            const token = await AuthService.loginEP(req.body.email,req.body.password) 
            return res
                .status(HttpStatus.HTTP_OK)
                .json({token})
        })
    )

    /**
     * @api {post} /register Create a new Email and Password user open sassion
     * @apiName EPRegister
     * @apiGroup Auth
     *
     *
     * @apiParam {String} email User email
     * @apiParam {String} password User password(min 8 char)
     * @apiParam {String} friendly_name User friendly name
     * 
     * @apiParamExample {json} Request-example:
     *      {
     *          "email":"test@example.com",
     *          "password":"password",
     *          "friendly_name": "Mario Rossi"
     *      }
     * 
     * @apiError {Status} 409 Email already registerd
     * 
     * @apiSuccess {String} token Session token
     */
    app.post("/register",
        body("email").isEmail().normalizeEmail(),
        body("password").isString().isLength({min:8}),
        body("friendly_name").isString().notEmpty().trim().escape(),
        CombineWithErrorHandler(async (req,res)=>{
            const errors = validationResult(req)
            if(!errors.isEmpty()){
                throw new RequestValidationError(errors)
            }
            const user = await AuthService.createEPUser(req.body.email,req.body.password,req.body.friendly_name)
            return res
                .status(HttpStatus.HTTP_CREATED)
                .json({token:user})
        }))
}