import express from 'express'
import {body, validationResult} from 'express-validator'
import { AuthService, FBService, TwitterService } from '../../applicationContext'
import { HttpStatus, RequestValidationError } from '../../errors/appError'
import { CombineWithErrorHandler } from '../../errors/errorHandlers'


export const SocialAuthEndpointRegister = (app:express.Application) => {
    /**
     * @api {post} /fbLogin Handle FB login, will create a new user it does not exists
     * @apiName FBLogin
     * @apiGroup Auth
     *
     *
     * @apiParam {String} code Code gathered from FB login
     * 
     * 
     * 
     * @apiSuccess {String} token Session token
     */
    app.post("/fbLogin",
        body("code").isString().notEmpty(),
        CombineWithErrorHandler(async (req,res)=>{
            const errors = validationResult(req)
            if(!errors.isEmpty()){
                throw new RequestValidationError(errors)
            }
            const token = await FBService.handleFBLogin(req.body.code)
            return res
                .status(HttpStatus.HTTP_OK)
                .json({token})
        })
    )


    /**
     * @api {get} /twitterToken Requesta a new OAuth1 token for Twitter login
     * @apiName TwitterToken
     * @apiGroup Auth
     *
     * @apiParam {Boolean} connect (Query param) if set it will generate a token with the redirect url set to the connect landing
     * 
     * 
     * 
     * @apiSuccess {String} token token key
     */
    app.get("/twitterToken",
        CombineWithErrorHandler(async (req,res)=>{
            let token
            if(req.query.connect === "true"){
                token = await TwitterService.createTokenConnect()
            }else{
                token = await TwitterService.createToken()
            }
            return res
                .status(HttpStatus.HTTP_OK)
                .json({token})
        }))

    
    
    /**
     * @api {get} /twitterLogin login with twitter
     * @apiName TwitterLogin
     * @apiGroup Auth
     *
     *
     * @apiParam {String} token token as returned by TwitterToken endpoint
     * @apiParam {String} verifier verifier token returned by twitter login 
     * 
     * 
     * @apiSuccess {String} token Session token
     */
    app.post("/twitterLogin",
         body("token").isString().notEmpty(),
         body("verifier").isString().notEmpty(),
         CombineWithErrorHandler(async (req,res)=>{
             const token = await TwitterService.twitterLogin(req.body.token,req.body.verifier)
             return res
                 .status(HttpStatus.HTTP_OK)
                 .json({token})
         }))
}