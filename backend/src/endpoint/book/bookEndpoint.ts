import express from 'express'
import { CombineWithErrorHandler } from '../../errors/errorHandlers'
import { getTokenFromRequest } from '../../util'
import { query,body, validationResult } from 'express-validator'
import { BookService } from '../../applicationContext'
import { HttpStatus, RequestValidationError } from '../../errors/appError'

export const BookEndpointRegister =(app:express.Application) =>{

    /**
     * @api {get} /book Search for a book in Google Books DB
     * @apiName BookQuery
     * @apiGroup Book
     *
     * @apiParam {String} q Query, minimum lenght: 4 charachters
     * 
     * 
     * @apiSuccess {GAPI_Book[]} result List of books from Google Books api 
     */
    app.get("/book",
        query("q").isString().isLength({min:4}),
        CombineWithErrorHandler(async (req,res)=>{
            const errors = validationResult(req)
            if(!errors.isEmpty()){
                throw new RequestValidationError(errors)
            }
            const authToken = getTokenFromRequest(req)
            const bookRes = await BookService.doQuery(authToken,req.query["q"] as string)
            return res.status(HttpStatus.HTTP_OK).json({result:bookRes})
    }))

    /**
    * @api {get} /book/:bookId Get details about book
    * @apiName BookDetails
    * @apiGroup Book
    *
    * @apiParam {String} bookId Google book id of book
    * 
    * @apiError 404 Book was not found on google books db
    * 
    * @apiSuccess {BookInfo} result Book with details
    */
    app.get("/book/:bookId",
    CombineWithErrorHandler(async (req,res)=>{
          const authToken = getTokenFromRequest(req)
          const book = await BookService.getBook(authToken,req.params.bookId)
          return res.status(HttpStatus.HTTP_OK).json({result:book})
    }))

    /**
    * @api {get} /pref/:bookId update pref for book
    * @apiName BookPrefUpd
    * @apiGroup Book
    *
    * @apiParam {String} bookId Google book id of book
    * @apiParam {Number} prefType value to update to (-1:none,0:interessato,1:sto_leggendo,2:ho_letto)
    * 
    * 
    * @apiSuccess {Preferito} result Updated prefeirto
    */
    app.post("/pref/:bookId",
        body("prefType").isNumeric(),
        CombineWithErrorHandler(async (req,res)=>{
            const errors = validationResult(req)
            if(!errors.isEmpty()){
                throw new RequestValidationError(errors)
            }
            const authToken = getTokenFromRequest(req)
            const prefRet = await BookService.setPref(authToken,req.params.bookId,req.body.prefType)
            return res.status(HttpStatus.HTTP_OK).json({result:prefRet})
    }))
}