import express from 'express'
import { CombineWithErrorHandler } from '../../errors/errorHandlers'
import { getTokenFromRequest } from '../../util'
import { HttpStatus } from '../../errors/appError'
import { _JWTService } from "../../service/JWTService";
import _ChatService from "../../service/ChatService";


export const ChatEndPoint = (app:express.Application) => {

    const jwtservice= _JWTService.getService();

    const socketList = new Array();

    const startPort:number = Number(process.env.CHAT_STARTING_PORT)
    const numberOfServers:number = Number(process.env.NUMBER_OF_OPEN_SERVERS)
    const chatRooms:string[] = String(process.env.CHAT_SECTIONS).split(",")
    const serverAddress:string = process.env.CHAT_SERVER_HOST

    for(var i=0; i<numberOfServers;i++){
        socketList.push(new _ChatService(startPort+i, chatRooms, i))
    }

    socketList.map((item) => {
        item.start();
    });

    /**
     * @api {get} /chat/:section  Returns a websocket URL which the client will use to connect to the chatroom
     * @apiName chat
     * @apiGroup Chat
     *
     *@apiParam {String} section  name of the chatroom the client wants to connect to
     * 
     * 
     * @apiSuccess {String} weboscket section to connect to a chatroom
     */
     app.get("/chat/:section",
     CombineWithErrorHandler(async (req,res)=>{
        const authToken = getTokenFromRequest(req)         
        const sollicitator = jwtservice.verify(authToken)

    
        if(sollicitator) await giveASocket(req.params.section)
        .then((socketToConnectTo) => {
            return res.status(HttpStatus.HTTP_OK).json(serverAddress.concat(":").concat(socketToConnectTo))
        })
        .catch((err) => {
            console.error(Error(err).message);
            return res.status(HttpStatus.HTTP_NOT_FOUND).json("This chat is unavailable right now")
        });
        
     }))
    
    
    
     /**
     * @api {get} /chatrooms Get a list containing all the available chatrooms in a string form
     * @apiName chatrooms
     * @apiGroup Chat
     *
     * 
     * @apiSuccess {String} chatrooms list
     */
     app.get("/chatrooms",
     CombineWithErrorHandler(async (req,res)=>{
        const authToken = getTokenFromRequest(req)         
        const sollicitator = jwtservice.verify(authToken)

        if(sollicitator) return res.status(HttpStatus.HTTP_OK).json(process.env.CHAT_SECTIONS)
        
     }))


    /**
     * Function that given a websocket finds an open websocket to the requested chat section the client wants to join to,
     * then retruns a promise to find an available open websocket
     * @param {*} ws websocket
     * @returns A promise returning the path clients will use to connect to the chat
     * @errors No more websockets available or connection issues with websockets
     */
    function giveASocket(chatroom:string): Promise<any> {
        return new Promise(function (resolve, reject) {

                socketList.map(item=>{
                    
                    if(item.getCanReceiveClients(chatroom)){
                        var socketToConnectTo = String(item.getPort())
                        .concat("/")
                        .concat(String(chatroom).concat(item.getServerId()));

                        console.log(socketToConnectTo)
                        resolve(socketToConnectTo);
                    }
                })

                reject(new Error("No more sockets Available"));
            });  
    }

}

