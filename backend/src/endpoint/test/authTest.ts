/*
 * Created on Mon May 17 2021
 *
 * Author: Gioele Zacchia
 */


import express from "express"
import {body, validationResult} from "express-validator"
import { AuthService, FBService, UserRepository } from "../../applicationContext"
import { User } from "../../persistence/entities/user.entity"
import { getTokenFromRequest } from "../../util"
import { AppError, HttpStatus, RequestValidationError } from "../../errors/appError"
import { CombineWithErrorHandler } from "../../errors/errorHandlers"
import { UnauthorizedError } from "../../errors/authErrors"


const mapDataUserToView = (usr:User)=>{
    return {
        id:usr.id,
        email:usr.email,
        icon:usr.icon,
        friendly_name:usr.friendly_name,
        fb_id:usr.facebook_id,
        tw_id:usr.twitter_id,
    }
}


export const AuthTestEndpointProvider = (app:express.Application) =>{
    if(process.env.NODE_ENV == "production"){
        return
    }

    app.post("/test/fb",
        body("code").isString().notEmpty(),
        CombineWithErrorHandler(async (req,res)=>{
            const errors = validationResult(req)
            if(!errors.isEmpty()){
                throw new RequestValidationError(errors)
            }
            const ao = await FBService.handleFBLogin(req.body.code)
            return res.status(HttpStatus.HTTP_OK).send("Ao")
        }))


    app.post("/test/login",
        body("email").isEmail(),
        body("password").isString(),
        CombineWithErrorHandler(async (req,res) => {
            const errors = validationResult(req)
            if(!errors.isEmpty()){
                throw new RequestValidationError(errors)
            }
            const token = await AuthService.loginEP(req.body.email,req.body.password) 
            return res.status(HttpStatus.HTTP_OK).json({token})
    }))

    app.post("/test/register",
        body("email").isEmail(),
        body("password").isLength({min:6}),
        body("friendly_name").isString(),
        CombineWithErrorHandler(async (req,res)=>{
        const errors = validationResult(req)
        if(!errors.isEmpty()){
            throw new RequestValidationError(errors)
        }
        const user = await AuthService.createEPUser(req.body.email,req.body.password,req.body.friendly_name)
        //return res.status(HttpStatus.HTTP_CREATED).json(mapDataUserToView(user))
    }))

    /*app.get("/test/whoami",
        CombineWithErrorHandler(async (req,res)=>{
            const authToken = getTokenFromRequest(req)
            const tokenPayload = AuthService.verifyToken(authToken)
            return res.status(200).json(tokenPayload)
        }))*/
}