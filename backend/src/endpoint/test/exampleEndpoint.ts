/*
 * Created on Wed May 12 2021
 *
 * Author: Gioele Zacchia
 */


import express from "express"
import { ExampleNestedService, ExampleService } from "../../applicationContext"




export const ExampleEndpointRegister = (app:express.Application) =>{
    /**
     * @api {get} / Get software version
     * @apiName GetVersion
     * @apiGroup Test
     *
     *
     * @apiSuccess {String} version Software version
     */
    app.get("/",(req,res)=>{
        const version = process.env.VERSION || "UNKNOWN"
        const responce = {
            version
        }
        res.send(JSON.stringify(responce))
    })

    /**
     * @api {get} /test Test example service
     * @apiName GetTest
     * @apiGroup Test
     *
     *
     * @apiSuccess {String} ao Eample service result
     */
    app.get("/test",(req,res)=>{
        const responce = {
            ao: ExampleService.getSomething()
        }
        res.send(JSON.stringify(responce))
    })

    /**
     * @api {get} /nested Test example nested service
     * @apiName GetNested
     * @apiGroup Test
     *
     *
     * @apiSuccess {String} ao Example nested result
     */
    app.get("/nested",(req,res)=>{
        const responce = {
            ao: ExampleNestedService.getSomething()
        }
        res.send(JSON.stringify(responce))
    })
}