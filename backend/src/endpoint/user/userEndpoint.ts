import express from 'express'
import { body } from 'express-validator'
import multer from 'multer'
import { FBService, TwitterService, UserService } from '../../applicationContext'
import { HttpStatus } from '../../errors/appError'
import { CombineWithErrorHandler } from '../../errors/errorHandlers'
import { getRandomString, getTokenFromRequest } from '../../util'

export const UserEndpointRegister = (app:express.Application) => {

    /**
     * @api {get} /user Get information about current user
     * @apiName UserSelf
     * @apiGroup User
     *
     *
     * 
     * 
     * @apiSuccess {UserPrivate} User informations
     */
    app.get("/user",
        CombineWithErrorHandler(async (req,res)=>{
            const authToken = getTokenFromRequest(req)
            const user = await UserService.getSelf(authToken)
            return res.status(HttpStatus.HTTP_OK).json(user)
    }))

    /**
     * @api {get} /user/:userId Get information about user by id
     * @apiName User
     * @apiGroup User
     *
     *@apiParam {String} userId userid to search for
     * 
     * 
     * @apiSuccess {UserPublic} User informations
     */
    app.get("/user/:userId",
        CombineWithErrorHandler(async (req,res)=>{
            const authToken = getTokenFromRequest(req)
            const user = await UserService.getUser(authToken,req.params.userId)
            return res.status(HttpStatus.HTTP_OK).json(user)
        }))

    /**
     * @api {put} /user/:userId Update informations about user (Only usable with own id)
     * @apiName PutUser
     * @apiGroup User
     *
     *@apiParam {String} friendly_name update the name parameter
     * 
     * 
     * @apiSuccess {UserPublic} User informations
     */
    app.put("/user/:userId",
        body("friendly_name").optional().isString(),
        CombineWithErrorHandler(async (req,res)=>{
            const authToken = getTokenFromRequest(req)
            const nName = req.body.friendly_name
            const user = await UserService.updateFName(authToken,req.params.userId,nName)
            return res.status(HttpStatus.HTTP_OK).json(user)
        }))


    /**
     * @api {post} /fbConnect Connect FB account to user
     * @apiName FbConnect
     * @apiGroup User
     *
     *@apiParam {String} code as returned by facebook login
     * 
     * 
     * @apiSuccess {boolean} Ok true
     */
    app.post("/fbConnect",
        body("code").isString().notEmpty(),
        CombineWithErrorHandler(async (req,res)=>{
            const authToken = getTokenFromRequest(req)
            const code = req.body.code
            const ok = await FBService.connectFB(authToken,code)
            return res.status(HttpStatus.HTTP_OK).json({ok:true})
        }))

    
    /**
     * @api {post} /fbConnect Disconnect FB from current user
     * @apiName FbDisconnect
     * @apiGroup User
     *
     * 
     * 
     * @apiSuccess {boolean} Ok true
     */
    app.post("/fbDisconnect",
        CombineWithErrorHandler(async (req,res)=>{
            const authToken = getTokenFromRequest(req)
            const ok = await FBService.disconnectFB(authToken)
            return res.status(HttpStatus.HTTP_OK).json({ok:true})
        }))

    
    /**
     * @api {post} /twitterDisconnect Connect twitter account to user
     * @apiName TwitterConnect
     * @apiGroup User
     *
     * 
     * 
     * @apiSuccess {boolean} Ok true
     */
    app.post("/twitterDisconnect",
        CombineWithErrorHandler(async (req,res)=>{
            const authToken = getTokenFromRequest(req)
            await TwitterService.twitterDisconnect(authToken)
            return res.status(HttpStatus.HTTP_OK).json({ok:true})
        }))


    /**
     * @api {post} /twitterConnect Connect FB account to user
     * @apiName TwitterConnect
     * @apiGroup User
     * 
     * @apiParam {String} token token as returned by TwitterToken endpoint
     * @apiParam {String} verifier verifier token returned by twitter login 
     * 
     * 
     * @apiSuccess {boolean} Ok true
     */
    app.post("/twitterConnect",
         body("token").isString().notEmpty(),
         body("verifier").isString().notEmpty(),
         CombineWithErrorHandler(async (req,res)=>{
            const authToken = getTokenFromRequest(req)
            await TwitterService.twitterConnect(authToken,req.body.token,req.body.verifier)
             return res
                 .status(HttpStatus.HTTP_OK)
                 .json({ok:true})
         }))

    const upload = multer({
        storage:multer.memoryStorage(),
        limits:{
            fileSize:Number(process.env.MAX_FILE_SIZE)
        },
        fileFilter:(req,file,cb)=>{
            if(file.mimetype === "image/png" || file.mimetype === "image/jpeg" || file.mimetype === "image/gif"){
                cb(null,true)
            }else{
                cb(null,false)
            }
        }
    })

    /**
     * @api {post} /avatar Upload new avatar
     * @apiName AvatarUpload
     * @apiGroup User
     * 
     * @apiParam {File} avatar Image to use as avatar (Max MAX_FILE_SIZE byte, allowed type png,jpeg, gif)
     * 
     * 
     * @apiSuccess {boolean} Ok true
     */
    app.post("/avatar",
        upload.single("avatar"),
        CombineWithErrorHandler(async (req,res)=>{
            const authToken = getTokenFromRequest(req)
            if(req.file.mimetype !== "image/png" && req.file.mimetype !== "image/jpeg" && req.file.mimetype !== "image/gif"){
                res.status(HttpStatus.HTTP_NOT_ACCEPTABLE).send({message:"Invalid file type"})
            }
            await UserService.uploadAvatar(authToken,req.file.originalname,req.file.buffer)
            return res.status(HttpStatus.HTTP_OK).send({ok:true})
        }))
}