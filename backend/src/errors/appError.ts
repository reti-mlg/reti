/*
 * Created on Tue May 18 2021
 *
 * Author: Gioele Zacchia
 */

import { Result, ValidationError } from "express-validator";

export enum HttpStatus{
    HTTP_OK = 200,
    HTTP_CREATED = 201,
    HTTP_NO_CONTENT = 204,

    HTTP_BAD_REQUEST = 400,
    HTTP_UNAUTHORIZED = 401,
    HTTP_FORBIDDEN = 403,
    HTTP_NOT_FOUND = 404,
    HTTP_METHOD_NOT_ALLOWED = 405,
    HTTP_NOT_ACCEPTABLE = 406,
    HTTP_CONFLICT = 409,
    HTTP_GONE = 410,

    HTTP_INTERNAL_SERVER_ERROR = 500,
    HTTP_NOT_IMPLEMENTED = 501

}


export class AppError{
    
    public constructor(public info:any, public code=HttpStatus.HTTP_INTERNAL_SERVER_ERROR){}
}

export class RequestValidationError extends AppError{
    public constructor(error:Result<ValidationError>){
        super({message:"Invalid request",errors:error.array()},HttpStatus.HTTP_BAD_REQUEST)
    }
}

export class NotFoundError extends AppError{
    constructor(message="Not Found"){
        super(message,HttpStatus.HTTP_NOT_FOUND)
    }
}