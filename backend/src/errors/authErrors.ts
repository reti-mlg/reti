/*
 * Created on Tue May 18 2021
 *
 * Author: Gioele Zacchia
 */

import { AppError, HttpStatus } from "./appError";


export class UnauthorizedError extends AppError{
    constructor(message="Unauthorized"){
        super(message,HttpStatus.HTTP_UNAUTHORIZED)
    }
}

export class ForbiddenError extends AppError{
    constructor(message="Forbidden"){
        super(message,HttpStatus.HTTP_FORBIDDEN)
    }
}

export class ConflictError extends AppError{
    constructor(message="Conflict"){
        super(message,HttpStatus.HTTP_CONFLICT)
    }
}