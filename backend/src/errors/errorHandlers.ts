/*
 * Created on Tue May 18 2021
 *
 * Author: Gioele Zacchia
 */

import express, { Handler } from 'express'
import { AppError, HttpStatus } from './appError'
import { UnauthorizedError } from './authErrors'

export const AppErrorHandler = (error, req:express.Request, res:express.Response, next:express.NextFunction) => {

    if (res.headersSent) {
        return next(error)
    }

    if(error instanceof AppError){
        return res.status(error.code).json({status:error.code,info:error.info})
    }
    console.error(error)
    res.status(HttpStatus.HTTP_INTERNAL_SERVER_ERROR).send({status:HttpStatus.HTTP_INTERNAL_SERVER_ERROR,info:"Unknown error",error:error})
  }

type asyncHandler = (req:express.Request,res:express.Response)=>Promise<any>|any

export const CombineWithErrorHandler = (t:asyncHandler) => {
    return (req:express.Request,res:express.Response,next:express.NextFunction) => {
        const rt = t(req,res)
        if(rt instanceof Promise){
            rt.catch((err)=>next(err))
        }
    }
}