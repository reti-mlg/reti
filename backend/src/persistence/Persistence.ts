/*
 * Created on Mon May 17 2021
 *
 * Author: Gioele Zacchia
 */

import { createConnection,ConnectionOptions, Connection } from "typeorm"
import { Commento } from "./entities/commento.entity"
import { Preferito } from "./entities/preferito.entity"
import { TwitterCredentials } from "./entities/twitterCreds.entity"
import { User } from "./entities/user.entity"

//Nb. List not complete
export enum DBErrorCodes{
    INTEGRITY_CONSTRAINT_VIOLATION = "23000",
    RESTRICT_VIOLATION = "23001",
    NOT_NULL_VIOLATION = "23502",
    FOREIGN_KEY_VIOLATION = "23503",
    UNIQUE_VIOLATION = "23505",
    CHECK_VIOLATION = "23514",
    EXCLUSION_VIOLATION = "23P01"
}


export class _PersistenceProvider{

    public static getService(config:ConnectionOptions){
        return new Promise<_PersistenceProvider>((resolve,reject)=>{
            console.log(config)
            createConnection({
                ...config,
                entities:[
                    User,
                    Commento,
                    Preferito,
                    TwitterCredentials
                ]
            }).then(conn=>resolve(new _PersistenceProvider(conn)))
            .catch(err=>{
                console.log(err)
                reject(err)
            })
        })
    }

    /*public static async getService(config:ConnectionOptions){
        try{
            const conn = await createConnection({
                ...config,
                entities: [
                    User,
                    Commento,
                    Preferito,
                    TwitterCredentials
                ]
            })
            return new _PersistenceProvider(conn)
        }catch(ex){
            console.log(ex)
            throw(ex)
        }
        
    }*/

    public static getConfigFromEnv(){
        const t:ConnectionOptions = {
            type: "postgres",
            url: process.env.DB_URL,
            ssl: process.env.DB_SSL?true:false,
            synchronize: process.env.DB_SYNC?true:false,
            logging: process.env.DB_LOG?true:false
        }
        return t
    }

    private constructor(private dbConn:Connection){}

    public getConnection(){return this.dbConn}

}