/*
 * Created on Mon May 17 2021
 *
 * Author: Gioele Zacchia
 */

import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { User } from "./user.entity";

@Entity()
export class Commento{
    @PrimaryGeneratedColumn("uuid")
    id?:string

    @Column()
    senderId?:string

    @ManyToOne(()=>User,{})
    sender?:User

    @Column({
        nullable:false
    })
    book_id:string

    @Column({
        nullable:false
    })
    date:Date

    @Column({
        nullable:false
    })
    content:string
}