/*
 * Created on Mon May 17 2021
 *
 * Author: Gioele Zacchia
 */

import { Column, Entity, ManyToOne } from "typeorm";
import { User } from "./user.entity";

export enum PrefType{
    none = -1,
    interessato = 0,
    sto_leggendo = 1,
    ho_letto = 2
}

@Entity()
export class Preferito{
    @Column({
        primary:true
    })
    userId:string

    @ManyToOne(()=>User,{})
    user?:User

    @Column({
        primary:true
    })
    bookId:string

    @Column({
        nullable:false
    })
    date:Date

    @Column({
        nullable:false,
        type:'int'
    })
    type:PrefType
}