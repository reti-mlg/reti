import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { User } from "./user.entity";



@Entity()
export class TwitterCredentials{
    @PrimaryGeneratedColumn("uuid")
    id?:string

    @Column({
        nullable:false
    })
    token_key:string

    @Column({
        nullable:false
    })
    token_secret:string

    @Column({
        nullable:false,
        default:false
    })
    verified?:boolean

    @OneToOne(()=>User)
    user?:User

    @Column({
        unique:true,
        nullable:true
    })
    userId?:string
}