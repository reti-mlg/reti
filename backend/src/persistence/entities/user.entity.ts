/*
 * Created on Mon May 17 2021
 *
 * Author: Gioele Zacchia
 */

import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class User{
    @PrimaryGeneratedColumn("uuid")
    id?:string

    @Column({
        nullable:true,
        unique:true
    })
    email?:string

    @Column({
        type:"bytea",
        nullable:true
    })
    password?:Buffer

    @Column({
        nullable:true
    })
    salt?:string

    @Column({
        nullable:true
    })
    icon?:string

    @Column({
        nullable:true
    })
    friendly_name?:string

    @Column({
        nullable:true,
        unique:true
    })
    facebook_id?:string

    @Column({
        nullable:true
    })
    facebook_token?:string

    @Column({
        nullable:true,
        unique:true
    })
    twitter_id?:string


}