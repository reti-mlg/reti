/*
 * Created on Tue May 18 2021
 *
 * Author: Gioele Zacchia
 */

import crypto from 'crypto'
import { QueryFailedError, Repository } from "typeorm";
import { JWTService, UserRepository, UserService } from "../applicationContext";
import { User } from "../persistence/entities/user.entity";
import { buffersEqual, getRandomString } from "../util";
import { ConflictError, UnauthorizedError } from "../errors/authErrors";
import { DBErrorCodes } from "../persistence/Persistence";
import { TokenPayload, _JWTService } from "./JWTService";

export class _AuthService{
    public static async getService(){
        return new _AuthService(UserRepository,JWTService)
    }

    secret!:string  
    expiration?:number  

    private constructor(private userRepo:Repository<User>,private jwtService:_JWTService){
        if(process.env.JWT_SECRET){
            this.secret = process.env.JWT_SECRET
        }else{
            this.secret = crypto.randomBytes(30).toString("hex")
        }
        if(process.env.JWT_TTL){
            this.expiration = parseInt(process.env.JWT_TTL)
        }
    }

    public async createEPUser(email:string,password:string,friendly_name:string){
        const salt = getRandomString()
        const encPass = crypto.createHash("sha256").update(password + salt).digest()
        const usr:User = {
            email,
            password:encPass,
            salt,
            friendly_name
        }
        try{
            const user =  await this.userRepo.save(usr)
            const tokenContent:TokenPayload = {
                email:user.email,
                uid:user.id,
                friendly_name:user.friendly_name
            }
            return  this.jwtService.sign(tokenContent)
        }catch(err){
            if(err instanceof QueryFailedError){
                const t = err as any //Looking at the source of typeorm it looks like this field is dependent on the DB driver
                if(t.code == DBErrorCodes.UNIQUE_VIOLATION){ //This works for postgres, not sure for other DBMS
                    throw new ConflictError("Email already exists")
                }
            }
            throw err
        }
    }

    public async loginEP(email:string, password:string){
        const usr = await this.userRepo.findOne({
            where:{
                email:email
            }
        })
        if(!usr || !usr.email || !usr.password || !usr.salt){
            throw new UnauthorizedError("Invalid login");
        }
        const encPass = crypto.createHash("sha256").update(password + usr.salt).digest()
        if(!buffersEqual(encPass,usr.password)){
            throw new UnauthorizedError("Invalid login");
        }
        const tokenContent:TokenPayload = {
            email:usr.email,
            uid:usr.id,
            friendly_name:usr.friendly_name
        }
        return this.jwtService.sign(tokenContent)
    }
}