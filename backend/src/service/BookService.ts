import axios from "axios";
import { Repository } from "typeorm";
import { JWTService, PreferitoRepository, UserService } from "../applicationContext";
import { AppError, HttpStatus, NotFoundError } from "../errors/appError";
import { UnauthorizedError } from "../errors/authErrors";
import { Preferito, PrefType } from "../persistence/entities/preferito.entity";
import { _JWTService } from "./JWTService";
import { _UserService } from "./UserService";

interface GAPI_Inds_ids{
    type:string,
    identifier:string
}

interface GAPI_Book{
    kind:string,
    id:string,
    etag:string,
    selfLink:string,
    searchInfo?:{
        textSnippet:string
    },
    volumeInfo:{
        title:string,
        subtitle:string,
        authors:string[],
        publisher:string,
        publishedDate:string,
        description:string,
        industryIdentifiers:GAPI_Inds_ids[],
        readingModes:{
            text:boolean,
            image:boolean
        },
        pageCount:number,
        printType:string,
        categories:string[],
        averageRating:number,
        ratingsCount:number,
        maturityRating:string,
        allowAnonLogging:boolean,
        contentVersion:string,
        panelizationSummary: {
            containsEpubBubbles: false,
            containsImageBubbles: false
        },
        imageLinks: {
            smallThumbnail: string,
            thumbnail: string
        },
        language: string,
        previewLink: string,
        infoLink:string,
        canonicalVolumeLink: string
    }
}

interface GAPI_Query_Responce{
    kind:string,
    totalItems:number,
    items:GAPI_Book[]
}

interface BookInfo{
    book:GAPI_Book,
    ownPref:Preferito,
    friendPrefs:Preferito[]
}


export class _BookService{

    private static readonly GAPI_BOOK_URL="https://www.googleapis.com/books/v1/volumes"

    public static getService(){
        return new _BookService(PreferitoRepository,UserService,JWTService)
    }


    private constructor(
        private prefRepo:Repository<Preferito>,
        private userService:_UserService,
        private jwtService:_JWTService
    ){}

    public async doQuery(sollicitatorToken:string, query:string){
        const t = this.jwtService.verify(sollicitatorToken)
        if(!t){
            throw new UnauthorizedError()
        }
        const url = _BookService.GAPI_BOOK_URL 
            + `?key=${encodeURI(process.env.GOOGLE_API_KEY)}`
            + `&q=${encodeURI(query)}`

        const ret = await axios.request<GAPI_Query_Responce>({
            method:"GET",
            url:url,
            validateStatus:()=>true
        })


        if(ret.status != 200){
            throw new AppError("Cannot connect to Google Api",HttpStatus.HTTP_INTERNAL_SERVER_ERROR)
        }

        return ret.data.items
    }

    public async getBook(sollicitatorToken:string, bookId:string){
        const t = this.jwtService.verify(sollicitatorToken)
        if(!t){
            throw new UnauthorizedError()
        }
        const url = _BookService.GAPI_BOOK_URL
            + `/${encodeURI(bookId)}`
            + `?key=${encodeURI(process.env.GOOGLE_API_KEY)}`

        const ret = await axios.request<GAPI_Book>({
            method:"GET",
            url:url,
            validateStatus:()=>true
        })

        if(ret.status == 404){
            throw new NotFoundError("BookID not found")
        }

        if(ret.status != 200){
            throw new AppError("Cannot connect to Google Api",HttpStatus.HTTP_INTERNAL_SERVER_ERROR)
        }

        const friends = await this.userService.getFriendList(sollicitatorToken)
        
        let prefs = []
        if(friends.length > 0){
            prefs = await this.prefRepo.createQueryBuilder("pref")
            .where("pref.bookId = :bookId",{bookId})
            .andWhere("pref.userId IN (:...friends)",{friends:friends.map(f=>f.user.id)})
            .getMany()
        }


        const ownPref = await this.prefRepo.findOne({
            where:{
                bookId:bookId,
                userId:t.uid
            }
        })

        const h:BookInfo = {
            book:ret.data,
            ownPref,
            friendPrefs:prefs
        }

        return h
    }

    public async setPref(sollicitatorToken:string,bookId:string,prefType:PrefType){
        const sollicitator = this.jwtService.verify(sollicitatorToken)
        const url = _BookService.GAPI_BOOK_URL
            + `/${encodeURI(bookId)}`
            + `?key=${encodeURI(process.env.GOOGLE_API_KEY)}`

        const ret = await axios.request<GAPI_Book>({
            method:"GET",
            url:url,
            validateStatus:()=>true
        })

        if(ret.status == 404){
            throw new NotFoundError("BookID not found")
        }

        const t:Preferito = {
            bookId:bookId,
            userId:sollicitator.uid,
            type:prefType,
            date:new Date()
        }

        if(prefType == PrefType.none){
            await this.prefRepo.delete({
                userId:sollicitator.uid,
                bookId:bookId
            })
            return t
        }else{
            return await this.prefRepo.save(t)
        }




    }
}