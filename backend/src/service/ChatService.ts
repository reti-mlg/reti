import http from "http";
import https from "https";
import ws from "ws";
import url from "url";

import fs from "fs"

import {_RabbitService} from "./RabbitService";
import { RabbitService } from "../applicationContext";
import { useHTTPS } from "../app";

//webSocket list element
interface WSLE{
    "chatroom":string,
    "ws":ws.Server
}


export default class _ChatService {
    port: Number;
    chatRooms: string[];
    serverId: Number;
    wss: Map<string,WSLE>;
    maxClientInSocket:Number;
    rabbitService:_RabbitService;

    constructor(port: Number, chatRooms: string[], serverId: Number) {
        this.port = port;
        this.chatRooms=chatRooms
        this.serverId=serverId
        this.wss = new Map<string, WSLE>();
        this.maxClientInSocket=Number(process.env.MAX_CLIENT_CONNECTED_IN_SOCKET)||1;
        
        //GZ: Modificato per utilizzare il servizio dal contesto
        this.rabbitService=RabbitService;
    }
    
    /**
     * Method that returns a websockets port
     * @returns   {String}         Value of the port
     */
    getPort(): Number {
        return this.port;
    }

    /**
     * Returns number of clients connected to the socket of a designated chatroom
     * @param ChatRoom String containing the value of the chatroom
     * @returns  the number of connected clients to the websocket
     */
    getClients(ChatRoom:string){
        return this.wss.get(ChatRoom).ws.clients.size
    }

    /**
     * 
     * @returns Websocket server id number
     */
    getServerId():Number{
        return this.serverId
    }

    /**
     * Method used to check the availability of free socket for a specific chatroom
     * @param ChatRoom String containing the value of the chatroom
     * @returns true if new clients can connect to the selected chatroom, false otherwise
     */
    getCanReceiveClients(ChatRoom:string):boolean{
        if(this.getClients(ChatRoom) < this.maxClientInSocket) return true
        else return false
    }

    
    /**
     * method used to instatiantiate websockets
     */
    CreateSockets(){
        var self = this;
        return new Promise<void>(function (resolve) {
            self.chatRooms.map((item)=>{
            
                var temp:WSLE={
                    "chatroom":item,
                    "ws":new ws.Server({ noServer: true })
                }
    
                self.wss.set(item, temp)            
                self.wss.get(item).ws.setMaxListeners(self.maxClientInSocket)
                //console.log(self.wss.get(item).ws)
            })
            resolve()
        });
    }

    /**
     * Method that returns a promise to create an http server
     * @param {*} port Port the server will use to listen
     * @returns A promise
     */
    createHttpServer(port: Number) {
        return new Promise(function (resolve, reject) {
            //GZ: Fix per WSS, è una cosa obrobriosa ma toccherebbe riscrivere l'intero modulo chat per risolverlo per bene
            let server:any;
            if(useHTTPS){
                server = https.createServer({
                    key: fs.readFileSync(process.env.HTTPS_KEY_FILE),
                    cert: fs.readFileSync(process.env.HTTPS_CERT_FILE),
                    dhparam: process.env.HTTPS_DHPARAM_FILE?fs.readFileSync(process.env.HTTPS_DHPARAM_FILE):undefined
                })
            }else{
                server = http.createServer();
            }
            server.listen(port);

            server.on("listening", () => {
                resolve(server);
            });
            server.on("error", (err) => {
                reject(err);
            });
        });
    }

    /**
     * Methods that send a message to all the clients connected to the websockets
     * @param {*} message
     * @returns A promise
     */
    sendBroadcast(data: any, ChatRoom:string) {
        var self = this;
        return new Promise<void>(function (resolve, reject) {
            self.wss.get(ChatRoom).ws.clients.forEach(client => {
                if (client !== ws /* && client.readyState === WebSocket.OPEN */) {
                    client.send(data);
                    client.on("error", (err: any) => {
                        reject(err);
                    });
                  }
            });
            resolve();
        });
    }

    /**
     * Method that returns a promise to make websockets start listening for events
     * @param {*} server
     * @returns A promise
     */
    httpServerListen(server: any) {
        var self = this;
        return new Promise<void>(function (resolve, reject) {
            server.on("upgrade", function upgrade(request: { url: string; }, socket: any, head: any) {
                const pathname = url.parse(request.url).pathname;

                

                self.wss.forEach((item:WSLE)=>{
                    if (pathname === "/".concat(item.chatroom).concat(String(self.serverId))) {
                        item.ws.handleUpgrade(request, socket, head, function done(ws: ws.Server) {
                            item.ws.emit("connection", ws, request);

                            console.log("new %s: %s", item.chatroom, self.getClients(item.chatroom));

                            ws.on("message", function incoming(message: string) {
                                console.log(
                                    "received: %s through websocket %s",
                                    message,
                                    item.chatroom
                                );
                                self.rabbitService.publisher(message, item.chatroom)
                                    .then((msg: string) => console.info(msg))
                                    .catch((error: Error) => console.error(error));
                            });

                            ws.on("close", function () {
                                console.info("ended connection");
                                //if (item.clients.size === 0) self.wss.splice(index, 1);
                                console.log("closed %s: %s", item.chatroom, self.getClients(item.chatroom));
                            });
                        })
                    }
                    
                })
                resolve();
            });

            server.on("error", (err: Error) => reject(err));
        });
    }


    /**
     * Method use to start up the websocket server
     */
    start() {
        var self = this;
        self.CreateSockets().then(()=>{
            self
            .createHttpServer(self.port)
            .then((server) => {
                /* console.info("server start on port %d", self.port); */
                self
                    .httpServerListen(server)
                    .then()
                    .catch((error: Error) => console.error(error));
            })
            .catch((error: Error) => console.error(error));

            self.wss.forEach(item =>{
                self.rabbitService.subscriber(item.chatroom)
                .then((values: Array<any>) => {
                    var channel: any = values[0]
                    var que: string = values[1]
                    channel.consume(
                        que,
                        function (msg: { content: { toString: () => any; }; }) {
                            if (msg.content) {
                                var message: String = msg.content.toString();
                                console.log(" [x] Received %s through rabbit", message);

                                self.sendBroadcast(message,item.chatroom).catch((err) => {
                                    console.error(Error(err).message);
                                });
                            }
                        },
                        {
                            noAck: true,
                        }
                    );
                })
                .catch((error: Error) => console.error(error));
            })
        })
        
        
    }
}
