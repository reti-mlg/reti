import { Repository } from "typeorm";
import { JWTService, UserRepository } from "../applicationContext";
import { User } from "../persistence/entities/user.entity";
import axios from "axios"
import { AppError } from "../errors/appError";
import { TokenPayload, _JWTService } from "./JWTService";
import { ConflictError, ForbiddenError, UnauthorizedError } from "../errors/authErrors";


interface FBTokenResponce{
    access_token:string,
    token_type:string,
    expires_in:number
}

interface FBSelfInfoResponce{
    id:string,
    name:string,
    email:string
}
interface FBFriendItem{
    name:string,
    id:string
}

interface FBFriendsResponce{
    id: string,
    friends: {
      data: FBFriendItem[],
      paging: {
        cursors: {
          before: string,
          after: string
        }
      },
      summary: {
        total_count: number
      }
    }
}

export class _FBService{
    public static getService(){
        return new _FBService(UserRepository,JWTService)
    }

    private constructor(private userRepo:Repository<User>,private jwtService:_JWTService){}

    private static readonly FB_GET_ACCESS_TOKEN = "https://graph.facebook.com/v10.0/oauth/access_token"
    private static readonly FB_GET_SELF_INFO = "https://graph.facebook.com/v10.0/me?fields=name%2Cid%2Cemail"
    private static readonly FB_GET_FRIENDS = "https://graph.facebook.com/v10.0/me?fields=id%2Cfriends"

    public async disconnectFB(sollicitatorToken:string){
        const sollicitator = this.jwtService.verify(sollicitatorToken)
        const user = await this.userRepo.findOne({
            where:{
                id:sollicitator.uid
            }
        })
        user.facebook_id = null;
        user.facebook_token = null;
        await this.userRepo.save(user)
        return true
    }

    public async connectFB(sollicitatorToken:string, fbCode:string){
        const sollicitator = this.jwtService.verify(sollicitatorToken)
        const reqUri = _FBService.FB_GET_ACCESS_TOKEN
            + `?client_id=${encodeURI(process.env.FB_APP_ID)}`
            + `&redirect_uri=${encodeURI(process.env.FB_CONNECT_REDIRECT_URI)}`
            + `&client_secret=${encodeURI(process.env.FB_SECRET)}`
            + `&code=${fbCode}`
            + `&scope=email,user_friends`;
        const res = await axios.request<FBTokenResponce>({
            method:"GET",
            url:reqUri,
            validateStatus:()=>true
        })
        if(res.status != 200){
            throw new UnauthorizedError("Invalid code")
        }
        const _token = res.data.access_token
        const sReqUti = _FBService.FB_GET_SELF_INFO
            + `&access_token=${encodeURI(_token)}`
        const sres = await axios.request<FBSelfInfoResponce>({
                method:"GET",
                url:sReqUti,
                validateStatus:()=>true
            })
        if(sres.status != 200){
            throw new AppError("Cannot get data")
        }
        const user = await this.userRepo.findOne({
            where:{
                facebook_id:sres.data.id
            }
        })
        if(user != null){
            throw new ConflictError("FB User already registerd")
        }
        const _user = await this.userRepo.findOne({
            where:{
                id:sollicitator.uid
            }
        })
        if(_user.facebook_token || _user.facebook_id){
            throw new ConflictError("User already has a fb account registerd")
        }
        _user.facebook_id = sres.data.id
        _user.facebook_token = _token
        await this.userRepo.save(_user)
        return true
    }


    public async handleFBLogin(code:string){
        const reqUri = _FBService.FB_GET_ACCESS_TOKEN 
            + `?client_id=${encodeURI(process.env.FB_APP_ID)}`
            + `&redirect_uri=${encodeURI(process.env.FB_REDIRECT_URI)}`
            + `&client_secret=${encodeURI(process.env.FB_SECRET)}`
            + `&code=${code}`
            + `&scope=email,user_friends`;

        const res = await axios.request<FBTokenResponce>({
            method:"GET",
            url:reqUri,
            validateStatus:()=>true
        })
        if(res.status != 200){
            throw new UnauthorizedError("Invalid code")
        }
        const _token = res.data.access_token
        const sReqUti = _FBService.FB_GET_SELF_INFO
            + `&access_token=${encodeURI(_token)}`
        const sres = await axios.request<FBSelfInfoResponce>({
                method:"GET",
                url:sReqUti,
                validateStatus:()=>true
            })
        if(sres.status != 200){
            throw new AppError("Cannot get data")
        }
        const user = await this.userRepo.findOne({
            where:{
                facebook_id:sres.data.id
            }
        })
        if(user){
            const tokenContent:TokenPayload = {
                email:user.email,
                uid:user.id,
                friendly_name:user.friendly_name
            }
            user.facebook_token = _token
            await this.userRepo.save(user)
            return this.jwtService.sign(tokenContent)
        }else{
            const nUser:User = {
                email:sres.data.email,
                facebook_id:sres.data.id,
                facebook_token:_token,
                friendly_name:sres.data.name
            }
            const usr = await this.userRepo.save(nUser)
            const tokenContent:TokenPayload = {
                email:usr.email,
                uid:usr.id,
                friendly_name:usr.friendly_name,
            }
            return this.jwtService.sign(tokenContent)
        }

    }

    public async getFriends(userID:string){
        const user = await this.userRepo.findOne({
            where:{
                id:userID
            }
        })
        if(!user.facebook_id || !user.facebook_token){
            return []
        }
        const sReqUti = _FBService.FB_GET_FRIENDS
        + `&access_token=${encodeURI(user.facebook_token)}`
        const ret = await axios.request<FBFriendsResponce>({
            method: "GET",
            url:sReqUti,
            validateStatus:()=>true
        })
        console.log(sReqUti)
        if(ret.status == 200){
            return ret.data.friends.data.map(val=>val.id)
        }else{
            throw new ForbiddenError("Cannot get fb friends")
        }

    }
}