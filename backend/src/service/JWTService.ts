import crypto from 'crypto'
import jwt, { JsonWebTokenError, TokenExpiredError } from 'jsonwebtoken'
import { UnauthorizedError } from '../errors/authErrors'

export interface TokenPayload{
    uid:string,
    email:string,
    friendly_name:string
}

export class _JWTService{
    public static getService(){
        return new _JWTService()
    }

    
    secret!:string  
    expiration?:number

    private constructor(){
        if(process.env.JWT_SECRET){
            this.secret = process.env.JWT_SECRET
        }else{
            this.secret = crypto.randomBytes(30).toString("hex")
        }
        if(process.env.JWT_TTL){
            this.expiration = parseInt(process.env.JWT_TTL)
        }
    }

    public sign(payload:TokenPayload){
        const opt:jwt.SignOptions = {}
        if(this.expiration){
            opt.expiresIn = this.expiration
        }
        return jwt.sign(payload,this.secret,opt)
    }

    public verify(token:string){
        try{
            return jwt.verify(token,this.secret) as TokenPayload
        }catch(err){
            if(err instanceof TokenExpiredError){
                throw new UnauthorizedError("Token expired")
            }
            if(err instanceof JsonWebTokenError){
                throw new UnauthorizedError("Invalid token");
            }
            throw err
        }
    }

}