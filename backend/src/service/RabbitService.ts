#!/usr/bin/env node
var amqp = require("amqplib/callback_api");


const delay = (ms:number)=>{
    return new Promise<void>((resolve,reject)=>{
        setTimeout(resolve,ms)
    })
}

 export class _RabbitService{

    //GZ: Ho modificato il costruttore per utilizzare una singola connessione invece che ricrearla ad ogni richiesta
    // Inoltre ho gestito gli errori con un certo numero di retries
    public static async getService(retry=5):Promise<_RabbitService>{
        const t =  new Promise<_RabbitService>((resolve,reject)=>{
            console.log("Connecting to AMQP")
            amqp.connect(process.env.RABBITMQ_SERVER_URL, (err,conn)=>{
                console.log("Connected to AMQP ok")
                if(err){
                    reject(err)
                }
                resolve(new _RabbitService(conn))
            })
        })
        try{
            return await t
        }catch(err){
            if(retry === 0){
                throw err
            }else{
                await delay(Number(process.env.RABBIT_RETRY_DELAY))
                return await this.getService(retry-1)
            }
        }
    }

    private constructor(private conn:any){}
    
    /**
    * The publisher sends a message to the rabbitmq server
    * @param {*} msg string containing the message to send 
    * @param {*} exc strign containing the exchange
    * @returns Promise<string> string containing message sent to the server through a specific exchange
    */
    public publisher(msg: string, exc: string) {
        const conn = this.conn
        return new Promise<string>(function (resolve, reject) {
                conn.createChannel(function (error1: Error, channel: any) {
                    if (error1) {
                        reject(error1);
                    }
                    var exchange: string = exc;
                    var message: string = msg;

                    channel.assertExchange(exchange, "fanout", {
                        durable: false,
                    });
                    channel.publish(exchange, "", Buffer.from(message));

                    resolve(
                        " [x] Sent "
                            .concat(message)
                            .concat(" through rabbit ")
                            .concat(exchange)
                    );
                });
            });
    }

    /**
     * The susbscriber creates an instance of a channel and a queue given a specific exchange
     * @param exc strign containing the exchange
     * @returns Array containing a channel object and the queue to get messages from
     */
    public subscriber(exc: string) {
        const conn = this.conn
        return new Promise<any>(function (resolve, reject) {
                conn.createChannel(function (error1: Error, channel: any) {
                    if (error1) {
                        reject(error1);
                    }
                    var exchange = exc;

                    channel.assertExchange(exchange, "fanout", {
                        durable: false,
                    });

                    channel.assertQueue(
                        "",
                        {
                            exclusive: true,
                        },
                        function (error2: Error, q: any) {
                            if (error2) {
                                reject(error2);
                            }

                            /* console.log(
                                " [*] Waiting for messages in %s. To exit press CTRL+C",
                                exchange
                            ); */

                            channel.bindQueue(q.queue, exchange, "");

                            resolve([channel, q.queue]);
                        }
                    );
                });
            });
    }
}