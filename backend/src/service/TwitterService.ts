import { encode } from "querystring";
import { Repository } from "typeorm";
import { JWTService, TwitterCredentialsRepository, UserRepository } from "../applicationContext";
import { User } from "../persistence/entities/user.entity";
import { getRandomString } from "../util";
import { TokenPayload, _JWTService } from "./JWTService";
import crypto from 'crypto'
import axios from "axios";
import { TwitterCredentials } from "../persistence/entities/twitterCreds.entity";
import { ConflictError, ForbiddenError, UnauthorizedError } from "../errors/authErrors";
import { AppError } from "../errors/appError";
import qs from 'querystring'


interface oauth_token_req_ret{
    oauth_token:string
    oauth_token_secret:string,
    oauth_callback_confirmed:boolean
}

interface oauth_access_ret{
    oauth_token:string,
    oauth_token_secret:string,
    user_id:string,
    screen_name:string
}

interface twitter_friends_ret{
    "ids": number[],
    "next_cursor": number,
    "next_cursor_str": string,
    "previous_cursor": number,
    "previous_cursor_str": string,
    "total_count"?: number
}

interface oauth_params{
    [key:string]:string
}

interface oauth_signature_params{
    method:string,
    baseURL:string,
    consumerKey:string,
    consumerSecret:string,
    params?: oauth_params,
    callback?:string,
    tokenKey?:string,
    tokenSecret?:string,
    verifier?:string
}

export class _TwitterService{

    public static getService(){
        return new _TwitterService(UserRepository,JWTService,TwitterCredentialsRepository)
    }


    private consumer_key!:string
    private consumer_key_secert!:string
    private constructor(private userRepo:Repository<User>,private jwtService:_JWTService,private twtCredRepo:Repository<TwitterCredentials>){
        this.consumer_key = process.env.TWITTER_CONSUMER_KEY
        this.consumer_key_secert = process.env.TWITTER_CONSUMER_SECRET
    }

    private trueEncode(str:String){
        const allowed = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-._~"
        let dst = ""
        for(let i = 0; i < str.length; i++){
            const c = str.charAt(i)
            if(allowed.includes(c)){
                dst += c
            }else{
                dst += "%" +str.charCodeAt(i).toString(16).toUpperCase()
            }
        }
        return dst
    }

    private oauthRequest(params:oauth_signature_params){
        const oauth_timestamp = Math.floor(Date.now()/1000).toString()
        const oauth_nonce = getRandomString()
        const oauth_signature_method = "HMAC-SHA1"
        const oauth_consumer_key = params.consumerKey
        const oauth_version = "1.0"
        if(!params.params){
            params.params = {}
        }
        params.params["oauth_timestamp"] = oauth_timestamp
        params.params["oauth_nonce"] = oauth_nonce
        params.params["oauth_signature_method"] = oauth_signature_method
        params.params["oauth_consumer_key"] = oauth_consumer_key
        params.params["oauth_version"] = oauth_version
        if(params.callback){
            params.params["oauth_callback"] = params.callback
        }
        if(params.tokenKey){
            params.params["oauth_token"]= params.tokenKey
        }
        if(params.verifier){
            params.params["oauth_verifier"] = params.verifier
        }
        const oauth_signature = this.oauthSign(params)
        let dst =  "OAuth "
        dst += `${this.trueEncode("oauth_timestamp")}="${this.trueEncode(oauth_timestamp)}",`
        dst += `${this.trueEncode("oauth_nonce")}="${this.trueEncode(oauth_nonce)}",`
        dst += `${this.trueEncode("oauth_signature_method")}="${this.trueEncode(oauth_signature_method)}",`
        dst += `${this.trueEncode("oauth_consumer_key")}="${this.trueEncode(oauth_consumer_key)}",`
        dst += `${this.trueEncode("oauth_version")}="${this.trueEncode(oauth_version)}",`
        if(params.verifier){
            dst += `${this.trueEncode("oauth_verifier")}="${this.trueEncode(params.verifier)}",`
        }
        if(params.tokenKey){
            dst += `${this.trueEncode("oauth_token")}="${this.trueEncode(params.tokenKey)}",`
        }
        dst += `${this.trueEncode("oauth_signature")}="${this.trueEncode(oauth_signature)}"`
        if(params.callback){
            dst += `,${this.trueEncode("oauth_callback")}="${this.trueEncode(params.callback)}"`
        }
        return dst
    }

    private oauthSign(params:oauth_signature_params){
       let ordered = {}
       Object.keys(params.params).sort().forEach((key)=>{
           ordered[key] = params.params[key]
       }) 

       let encodedParams = ''

       for(let k in ordered){
            const encodedValue = this.trueEncode(ordered[k])
            const encodedKey = this.trueEncode(k)

            if(encodedParams === ''){
                encodedParams += `${encodedKey}=${encodedValue}`
            }else{
                encodedParams += `&${encodedKey}=${encodedValue}`
            }
       }


       const base_string = `${params.method.toUpperCase()}&${this.trueEncode(params.baseURL)}&${this.trueEncode(encodedParams)}`
       const sign_key = `${params.consumerSecret}&${params.tokenSecret || ""}`
       return crypto.createHmac("sha1",sign_key).update(base_string).digest().toString('base64')
    }

    private async requireToken(callback?:string){
        const method = "POST"
        const baseString = "https://api.twitter.com/oauth/request_token"
        const oauthAuth = this.oauthRequest({
            method,
            baseURL:baseString,
            callback: callback || process.env.TWITTER_CALLBACK_URI,
            consumerKey:process.env.TWITTER_CONSUMER_KEY,
            consumerSecret: process.env.TWITTER_CONSUMER_SECRET
        })

        const ret =  await axios.request<string>({
            method,
            url:baseString,
            headers:{
                "Authorization":oauthAuth
            },
            validateStatus:()=>true
        })
        if(ret.status === 200){
            /*const kVal = {}
            ret.data.split("&").forEach((val)=>{
                const t = val.split("=")
                kVal[t[0]] = t[1]
            })
            kVal["oauth_callback_confirmed"] = kVal["oauth_callback_confirmed"] === "true"*/
            return qs.parse(ret.data) as unknown as oauth_token_req_ret
            //return kVal as oauth_token_req_ret
        }else{
            throw new AppError("Cannot gather oauth token")
        }
    }

    private async getCredentials(token_key:string,token_secret:string){
        const method = "GET"
        const baseURL= "https://api.twitter.com/1.1/account/verify_credentials.json"
        const urlParams = "?include_email=true"
        const params = {
            include_email:"true"
        }
        const oauthAuth = this.oauthRequest({
            method,
            baseURL,
            params,
            tokenKey:token_key,
            tokenSecret:token_secret,
            consumerKey:process.env.TWITTER_CONSUMER_KEY,
            consumerSecret: process.env.TWITTER_CONSUMER_SECRET
        })
        const ret = await axios.request({
            method,
            url:baseURL + urlParams,
            headers:{
                "Authorization":oauthAuth
            },
            validateStatus:()=>true
        })
        if(ret.status === 200){
            return ret.data
        }else{
            throw new UnauthorizedError("Cannot gather access token")
        }
        
    }

    private async accessToken(token_key:string, token_secret:string, verifier:string){
        const method = "POST"
        const baseString = "https://api.twitter.com/oauth/access_token"
        const oauthAuth = this.oauthRequest({
            method,
            baseURL:baseString,
            consumerKey:process.env.TWITTER_CONSUMER_KEY,
            consumerSecret: process.env.TWITTER_CONSUMER_SECRET,
            tokenKey:token_key,
            tokenSecret:token_secret,
            verifier
        })
        const ret = await axios.request({
            method,
            url:baseString,
            headers:{
                "Authorization":oauthAuth,
                "Content-type":"application/x-www-form-urlencode"
            },
            data:`oauth_verifier=${verifier}`,
            validateStatus:()=>true
        })
        if(ret.status === 200){
            return qs.parse(ret.data) as unknown as oauth_access_ret
        }else{
            throw new UnauthorizedError("Cannot gather access token")
        }
    }
  
    public async createToken(){
        const token = await this.requireToken()
        const tct: TwitterCredentials = {
            token_key:token.oauth_token,
            token_secret:token.oauth_token_secret
        }
        const h = await this.twtCredRepo.save(tct)
        return h.token_key
    }

    public async createTokenConnect(){
        const token = await this.requireToken(process.env.TWITTER_CONNECT_CALLBACK_URI)
        const tct: TwitterCredentials = {
            token_key:token.oauth_token,
            token_secret:token.oauth_token_secret
        }
        const h = await this.twtCredRepo.save(tct)
        return h.token_key
    }

    public async twitterLogin(token:string, verifier:string){
        const tct = await this.twtCredRepo.findOne({
            where:{
                token_key:token,
                verified:false
            }
        })
        if(!tct){
            throw new UnauthorizedError("Invalid token or verifier")
        }
        const responce = await this.accessToken(tct.token_key,tct.token_secret,verifier)
        let user = await this.userRepo.findOne({
            where:{
                twitter_id:responce.user_id
            }
        })
        if(!user){
            const creds = await this.getCredentials(responce.oauth_token,responce.oauth_token_secret)
            const nUser:User = {
                email:creds.email,
                friendly_name:responce.screen_name,
                twitter_id:responce.user_id,
            }
            user = await this.userRepo.save(nUser)
        }else{
            this.twtCredRepo.delete({
                userId:user.id
            })
        }
        tct.token_key = responce.oauth_token
        tct.token_secret = responce.oauth_token_secret
        tct.verified = true
        tct.userId = user.id
        await this.twtCredRepo.save(tct)

        
        const tokenContent:TokenPayload = {
            email:user.email,
            uid:user.id,
            friendly_name:user.friendly_name,
        }
        return this.jwtService.sign(tokenContent)
    }

    public async twitterDisconnect(sollicitatorToken:string){
        const sollicitator = this.jwtService.verify(sollicitatorToken)
        const user = await this.userRepo.findOne({
            where:{
                id:sollicitator.uid
            }
        })
        user.twitter_id = null
        await this.userRepo.save(user)
        await this.twtCredRepo.delete({
            userId:user.id
        })
        return true
    }

    public async twitterConnect(sollicitatorToken:string, token:string,verifier:string){
        const sollicitator = this.jwtService.verify(sollicitatorToken)
        const user = await this.userRepo.findOne({
            where:{
                id:sollicitator.uid
            }
        })
        if(user.twitter_id){
            throw new ConflictError("User already has twitter connected")
        }
        const tct = await this.twtCredRepo.findOne({
            where:{
                token_key:token,
                verified:false
            }
        })
        if(!tct){
            throw new UnauthorizedError("Invalid token or verifier")
        }
        const responce = await this.accessToken(tct.token_key,tct.token_secret,verifier)
        const _user = await this.userRepo.findOne({
            where:{
                twitter_id:responce.user_id
            }
        })
        if(_user){
            throw new ConflictError("Twitter user is already registerd")
        }
        tct.token_key = responce.oauth_token
        tct.token_secret = responce.oauth_token_secret
        tct.verified = true
        tct.userId = user.id
        user.twitter_id = responce.user_id
        await this.twtCredRepo.save(tct)
        await this.userRepo.save(user)
        return true

    }

    public async getFriends(userID:string){
        const creds = await this.twtCredRepo.findOne({
            where:{
                userId:userID
            }
        })
        if(!creds){
            return []
        }
        const method = "GET"
        const baseURL = "https://api.twitter.com/1.1/friends/ids.json"
        const oauthAuth = this.oauthRequest({
            method,
            baseURL,
            tokenKey:creds.token_key,
            tokenSecret:creds.token_secret,
            consumerKey:process.env.TWITTER_CONSUMER_KEY,
            consumerSecret: process.env.TWITTER_CONSUMER_SECRET
        })
        const ret = await axios.request<twitter_friends_ret>({
            method,
            url:baseURL,
            headers:{
                "Authorization":oauthAuth
            }
        })
        if(ret.status == 200){
            return ret.data.ids
        }else{
            throw new ForbiddenError("Cannot get friends")
        }
    }
}