import { In, Repository } from "typeorm";
import { AuthService, FBService, JWTService, PreferitoRepository, TwitterService, UserRepository } from "../applicationContext";
import { NotFoundError } from "../errors/appError";
import { ForbiddenError } from "../errors/authErrors";
import { Preferito } from "../persistence/entities/preferito.entity";
import { User } from "../persistence/entities/user.entity";
import { _AuthService } from "./AuthService";
import { _FBService } from "./FBService";
import { _JWTService } from "./JWTService";
import { _TwitterService } from "./TwitterService";
import fs from 'fs'
import { getRandomString } from "../util";

export interface UserPublic{
    id:string,
    icon?:string,
    friendly_name?:string,
    facebook_id?:string,
    twitter_id?:string,
    prefs:Preferito[]
}

export enum FriendSource{
    facebook=1,
    twitter=2
}
export interface Friend{
    user:UserPublic,
    source:FriendSource
}

export interface UserPrivate extends UserPublic{
    email?:string,
    friends: Friend[]
}

export class _UserService{

    public static getService(){
        return new _UserService(UserRepository,PreferitoRepository,JWTService,TwitterService,FBService)
    }

    private constructor(
            private userRepo:Repository<User>,
            private prefRepo:Repository<Preferito>,
            private jwtService:_JWTService,
            private twitterService:_TwitterService,
            private fbService:_FBService){}

    private mapUserToPublicView(user:User,prefs:Preferito[]){
        const t:UserPublic = {
            id:user.id,
            friendly_name:user.friendly_name,
            icon:user.icon,
            facebook_id:user.facebook_id,
            twitter_id:user.twitter_id,
            prefs
        }
        return t
    }

    private mapUserToPrivateView(user:User,prefs:Preferito[], firends:Friend[]){
        const t = this.mapUserToPublicView(user,prefs) as UserPrivate
        t.email = user.email;
        t.friends = firends
        return t
    }

    public async getFriendList(sollicitatorToken:string):Promise<Friend[]>{
        const sollicitator = this.jwtService.verify(sollicitatorToken)
        const twitterFirends = await this.twitterService.getFriends(sollicitator.uid)
        const ret:Friend[] = []
        if(twitterFirends.length > 0 ){
            const users = await this.userRepo.find({
                where:{
                    twitter_id:In(twitterFirends.map(v=>String(v)))
                }
            })
            for(let user of users){
                const prefs = await this.prefRepo.find({
                    where:{
                        userId:user.id
                    }
                })
                ret.push({
                    source:FriendSource.twitter,
                    user:this.mapUserToPublicView(user,prefs)
                })
            }  
        }
        
        const fbFirends = await this.fbService.getFriends(sollicitator.uid)
        if(fbFirends.length > 0){
            const users = await this.userRepo.find({
                where:{
                    facebook_id: In(fbFirends)
                }
            })
            for(let user of users){
                const prefs = await this.prefRepo.find({
                    where:{
                        userId:user.id
                    }
                })
                ret.push({
                    source:FriendSource.facebook,
                    user:this.mapUserToPublicView(user,prefs)
                })
            }  
        }
        return ret
    }
    
    public async getUser(sollicitatorToken:string,id:string){
        const sollicitator = this.jwtService.verify(sollicitatorToken)
        const tUser = await this.userRepo.findOne({
            where:{
                id:id
            }
        })
        if(!tUser){throw new NotFoundError("User not found")}
        const prefs = await this.prefRepo.find({
            where:{
                userId:id
            }
        })
        if(sollicitator.uid != tUser.id){
            return this.mapUserToPublicView(tUser,prefs)
        }else{
            return this.mapUserToPrivateView(tUser,prefs, await this.getFriendList(sollicitatorToken))
        }
        
    }

    public async getSelf(sollicitatorToken:string){
        const sollicitator = this.jwtService.verify(sollicitatorToken)
        return await this.getUser(sollicitatorToken,sollicitator.uid) as UserPrivate
    }

    public async updateFName(sollicitatorToken:string, userid:string, fName:string){
        const sollicitator = this.jwtService.verify(sollicitatorToken)
        if(sollicitator.uid != userid){
            throw new ForbiddenError("Canno't update name of another user")
        }
        await this.userRepo.update({
            id:userid
        },{
            friendly_name:fName
        })
        return await this.getSelf(sollicitatorToken)
    }

    public async uploadAvatar(sollicitatorToken:string, fileName:string, file:Buffer){
        const sollicitator = this.jwtService.verify(sollicitatorToken)
        const user = await this.userRepo.findOne({
            where:{
                id:sollicitator.uid
            }
        })
        if(user.icon && fs.existsSync(process.env.IMAGES_DIR + user.icon)){
            fs.unlinkSync(process.env.IMAGES_DIR+user.icon)
        }
        const h = fileName.split(".")
        const nFileName = Date.now() + getRandomString(5) + `.${h[h.length-1]}`
        fs.writeFileSync(process.env.IMAGES_DIR+nFileName,file)
        user.icon = nFileName
        await this.userRepo.save(user)
        return true
    }

    
}