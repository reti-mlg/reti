/*
 * Created on Wed May 12 2021
 *
 * Author: Gioele Zacchia
 */

export class _ExampleService{

    public static getService(){
        //Service init function. We can use initilized dependecy from Application Context
        return new Promise<_ExampleService>((resolve,reject)=>{
            setTimeout(()=>{
                resolve(new _ExampleService("Una dipendenza asincrona")) //Simulate async dependency creation
            },3000)
        })
    }

    private constructor(private asyncDep:string){}

    public getSomething(){return this.asyncDep}

}