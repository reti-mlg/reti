/*
 * Created on Wed May 12 2021
 *
 * Author: Gioele Zacchia
 */
import { assert } from "console"
import { ExampleService } from "../../applicationContext"
import { _ExampleService } from "./ExampleAsync"

export class _ExampleNestedService{


    public static getService(){
        //Service init function. We can use initilized dependecy from Application Context
        return new Promise<_ExampleNestedService>((resolve,reject)=>{
            if(!ExampleService){
                //Probably a good idea to check if the required service is actualy initilized correctly
                reject("Example Service not initilized")
            } 
            setTimeout(()=>{
                resolve(new _ExampleNestedService(ExampleService)) //We use initilized service from application context
            },1000)
        })
    }

    //We couild also use the example service directly without embedding it in the object but i think this is a cleaner solution
    private constructor(private exampleService:_ExampleService){} 

    public getSomething(){return this.exampleService.getSomething() + " e sono anche annidato!"}

}