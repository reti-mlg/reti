/*
 * Created on Tue May 18 2021
 *
 * Author: Gioele Zacchia
 */

import crypto from 'crypto'
import express from 'express'
import { UnauthorizedError } from './errors/authErrors'


const alphabet = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890"

export const getRandomString = (len=30) => {
    const seed = crypto.randomBytes(len)
    let ret = ""
    for(let i = 0; i < len; i++){
        const index = Math.round((seed[i]/0xFF)*(alphabet.length-1))
        ret += alphabet[index]
    }
    return ret

}


export const buffersEqual = (A:Buffer, B:Buffer) => {
    if(A.length != B.length){
        return false
    }
    for(let i = 0; i < A.length; i++){
        if(A[i] != B[i]){
            return false
        }
    }
    return true
}

export const getTokenFromRequest = (req:express.Request) => {
    const auth = req.headers.authorization
    if(!auth || !auth.startsWith("Bearer ")){
        throw new UnauthorizedError("Invalid authorization header")
    }
    return auth.substring("Bearer ".length)
    
}