import React from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { getStore } from './state/StoreManager';
import {BrowserRouter as Router,Switch,Route} from "react-router-dom";
import { Home } from './pages/home';
import { AuthGuard } from './components/authGuard';
import { TestPage } from './pages/test';
import { Login } from './pages/login';
import { LoginPage } from './pages/loginPage';
import { FBLanding } from './pages/fbLanding';
import { TwitterLogin } from './pages/twitterLogin';
import { TwitterLanding } from './pages/twitterLanding';
import { FBConnectLanding } from './pages/fbConnectLanding';
import { TwitterLandingConnect } from './pages/twitterConnectLanding';
import { ChatPage } from './pages/chatPage';
import { RegisterPage } from './pages/registerPage';



const [store,persistor] = getStore()

function App(){
  return(
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        
        <Router>
          <Switch>
            <Route path="/register">
              <RegisterPage/>
            </Route>
            <Route path="/login">
              <LoginPage/>
            </Route>
            <Route path="/test">
              <AuthGuard redirect="/login">
                <TestPage/>
              </AuthGuard>
            </Route>
            <Route path="/fbLanding">
              <FBLanding/>
            </Route>
            <Route path="/fbLandingConnect">
              <FBConnectLanding/>
            </Route>
            <Route path="/twitterLogin">
              <TwitterLogin isConnect={false}/>
            </Route>
            <Route path="/twitterConnect">
              <TwitterLogin isConnect={true}/>
            </Route>
            <Route path="/twitterLanding">
              <TwitterLanding/>
            </Route>
            <Route path="/twitterLandingConnect">
              <TwitterLandingConnect/>
            </Route>
            <Route path="/chat">
              <ChatPage/>
            </Route>
            <Route path="/">
              <Home/>
            </Route>
          </Switch>
        </Router>

      </PersistGate>
    </Provider>
  )
}






/*import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}*/

export default App;
