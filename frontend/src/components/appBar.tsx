import React, { useEffect, useState } from 'react'
import { Container, Navbar, Image, Form, Button, FormControl} from 'react-bootstrap'
import { useHistory } from 'react-router-dom'
import { createUpdaterFunc } from '../utils'

interface AppBarProps{
    userName:string,
    userIcon?:string
    fromQuery?:string
}

export const AppBar:React.FunctionComponent<AppBarProps> = (props:AppBarProps)=>{

    const [query, setQuery] = useState("")

    const history = useHistory()
    const updQuery = createUpdaterFunc(setQuery)


    const doSubmit = (evt:React.FormEvent) => {
        evt.preventDefault()
        history.push(`/search?q=${query}`)
    }

    useEffect(()=>setQuery(props.fromQuery||""),
    [props.fromQuery])

    return (
        <Navbar bg="dark" variant="dark" expand="sm">
            <Container>
                <Navbar.Brand href="/">
                    {props.userIcon&&props.userIcon.length>0&&<Image
                        alt=""
                        src={props.userIcon}
                        width="40"
                        height="40"
                        roundedCircle 
                        className="d-inline-block align-top"
                    />}
                    {'  '}
                    {props.userName}
                </Navbar.Brand>
                <Button onClick={()=>history.push("/userPage")}>Profilo</Button>
                <Form className="d-flex" onSubmit={doSubmit}>
                    <FormControl
                        type="search"
                        placeholder="Search"
                        className="mr-2"
                        aria-label="Search"
                        value={query}
                        onChange={updQuery}
                    />
                    <Button variant="outline-success" type="submit">Search</Button>
                </Form>
            </Container>
        </Navbar>
    )
}