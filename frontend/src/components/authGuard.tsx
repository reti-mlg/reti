import React, { PropsWithChildren, useEffect } from 'react'
import { useHistory } from 'react-router'
import {useAuthService } from '../services/AuthService'

export interface AuthGuardProps{
    redirect:string
}

export const AuthGuard:React.FunctionComponent<AuthGuardProps> = (props:PropsWithChildren<AuthGuardProps>) => {
    const authService = useAuthService()
    const history = useHistory()
    useEffect(()=>{
        if(authService.isLogged()){
            return
        }
        const timeout = setTimeout(()=>{
            history.push(props.redirect)
            return ()=>timeout.unref()
        },1500)
    },[props.redirect,authService,history])
    if(authService.isLogged()){
        return <>{props.children}</>
    }else{
        return <h1>Per visualizzare questa pagina è necessario autenticarsi</h1>
    }
}