import React, { useState } from 'react'
import { useUserService } from '../services/UserService'


export const AvatarUploadForm:React.FunctionComponent = (props)=>{

    const [file,setFile] = useState<File>()
    const userService = useUserService()

    const updFile = (ev:React.ChangeEvent<HTMLInputElement>)=>{
        if(ev.target.files){
            setFile(ev.target.files[0])
        }
    }

    const doUpload = ()=>{
        const formData = new FormData()
        if(!file){
            return
        }
        formData.append("avatar",file,file.name)
        userService.uploadAvatar(formData).then(()=>{
            alert("Uploaded")
            
        })
    }

    return (<div>
                <input type="file" onChange={updFile}/>
                <button onClick={doUpload}>Upload</button>
            </div>)

}