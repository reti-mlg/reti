import React,{useState} from 'react'
import { Button, Navbar } from 'react-bootstrap'
import { Config } from '../config'
import { ChatPage } from '../pages/chatPage'


export const ChatBar:React.FunctionComponent = ()=>{
    const [showChat,setShowChat] = useState(false)

    const toggleShowChat = ()=>setShowChat(!showChat)

    return (
        <>
            <div style={{
                position:"fixed",
                borderRadius: "10px",
                borderWidth:"2px",
                borderStyle:"solid",
                zIndex:1000,
                width:"18rem",
                right:0,
                left:0,
                bottom:"3.3rem",
                visibility:showChat?"visible":"hidden"}}>
                    <ChatPage/>
            </div>
            <Navbar fixed="bottom"  bg="dark" variant="dark" expand="sm">
                <Button onClick={toggleShowChat}>Chat</Button>
                <div style={{
                        color:"white",
                        right:"0.5rem",
                        position:"absolute"}}>{Config.CLIENT_VERSION}</div>
            </Navbar>
        </>
    )
}