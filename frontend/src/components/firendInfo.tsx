import React from 'react'
import { Card, Col, Container, Row, Image, Button, ListGroup } from 'react-bootstrap'
import { Config } from '../config'
import { FriendWithPrefs } from '../pages/homePage'
import { Friend, PrefType } from '../services/UserService'

interface FriendListProps{
    firends: FriendWithPrefs[]
}

const prefToString = (pref:PrefType)=>{
    if(pref === PrefType.none){
        return "Nessuno"
    }else if(pref === PrefType.interessato){
        return "Interessato"
    }else if(pref === PrefType.ho_letto){
        return "Ho letto"
    }else if (pref === PrefType.sto_leggendo){
        return "Sto leggendo"
    }else{
        return "INVALIDO"
    }
}

const prefToVariant = (pref:PrefType)=>{
    if(pref === PrefType.none){
        return "secondary"
    }else if(pref === PrefType.interessato){
        return "primary"
    }else if(pref === PrefType.ho_letto){
        return "success"
    }else if (pref === PrefType.sto_leggendo){
        return "warning"
    }else{
        return "secondary"
    }
}


export const FriendList:React.FunctionComponent<FriendListProps> = (props:FriendListProps)=>{
    return (<Container>
        {props.firends.map((friend,id)=>(
            <Row key={id}>
                <Col>
                    <Card>
                        <Row>
                            <Col>
                                <Image
                                    alt=""
                                    src={Config.SERVER + Config.IMAGES_DIR + friend.friend.user.icon}
                                    width="70"
                                    height="70"
                                    roundedCircle 
                                    className="d-inline-block align-top"
                                />
                                <br/>
                                <h4>{friend.friend.user.friendly_name}</h4>
                            </Col>
                            <Col xs={10}>
                                <ListGroup horizontal>
                                    {friend.prefs.map((pref,id)=>(<ListGroup.Item key={id}>
                                        <a href={`/book?id=${encodeURI(pref.book.id)}`} style={{color:"black",textDecoration:"none"}} className="stretched-link">{pref.book.volumeInfo.title}</a>
                                        <br/>
                                        {pref.book.volumeInfo.imageLinks&&<Image
                                            src={pref.book.volumeInfo.imageLinks.smallThumbnail}
                                            width={50}
                                        />}
                                        <br/>
                                        {<Button variant={prefToVariant(pref.pref.type)} disabled>{prefToString(pref.pref.type)}</Button>}
                                    </ListGroup.Item>))}
                                </ListGroup>
                            </Col>
                        </Row>
                    </Card>
                </Col>
            </Row>
        ))}
    </Container>)
}