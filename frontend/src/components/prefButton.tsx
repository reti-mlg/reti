import React from 'react'
import { Dropdown, DropdownButton } from 'react-bootstrap'
import { PrefType } from '../services/UserService'


interface PrefButtonProps{
    value:PrefType,
    onChange?:(newPref:PrefType)=>void
}

const prefToString = (pref:PrefType)=>{
    if(pref === PrefType.none){
        return "Nessuno"
    }else if(pref === PrefType.interessato){
        return "Interessato"
    }else if(pref === PrefType.ho_letto){
        return "Ho letto"
    }else if (pref === PrefType.sto_leggendo){
        return "Sto leggendo"
    }else{
        return "INVALIDO"
    }
}

const prefToVariant = (pref:PrefType)=>{
    if(pref === PrefType.none){
        return "secondary"
    }else if(pref === PrefType.interessato){
        return "primary"
    }else if(pref === PrefType.ho_letto){
        return "success"
    }else if (pref === PrefType.sto_leggendo){
        return "warning"
    }else{
        return "secondary"
    }
}

const prefs = [PrefType.none,PrefType.interessato,PrefType.sto_leggendo,PrefType.ho_letto]

export const PrefButton:React.FunctionComponent<PrefButtonProps> = (props:PrefButtonProps) => {
    const onClickItem = (pref:PrefType)=>props.onChange&&props.onChange(pref)

    return (<DropdownButton title = {prefToString(props.value)} variant = {prefToVariant(props.value)}>
            {prefs.filter(t=>t!=props.value).map((val,id)=>(<Dropdown.Item 
                key={id} 
                variant={prefToVariant(val)}
                onClick={()=>onClickItem(val)}>
                    {prefToString(val)}
                </Dropdown.Item>))}
        </DropdownButton>)
}