import React, { useEffect, useState } from 'react'
import { BookInfo, GAPI_Book, useBookService } from '../services/BookService'
import { cancellablePromise, createUpdaterFunc } from '../utils'

export const BookSearchBox:React.FunctionComponent = ()=>{
    const [query,setQuery] = useState("")
    const [loading,setLoading]  = useState(false)
    const [results,setResult] = useState<GAPI_Book[]>([])

    const bookService = useBookService()

    const updQuery = createUpdaterFunc(setQuery)

    useEffect(()=>{
        if(query.length > 4){
            const wrapped = cancellablePromise(bookService.doQuery(query))
            wrapped.promise.then(_books=>{
                setLoading(false)
                if(_books){
                    setResult(_books as GAPI_Book[])
                }else{
                    setResult([])
                }
                
            })
            setLoading(true)
            return wrapped.cancel
        }else{
            setLoading(false)
            setResult([])
        }
        
    },[query,bookService.getState()])

    return (<div>
        <input type="text" placeholder="Cerca..." onChange={updQuery} value={query}/>
        <br/>
        {loading&&<h1>LOADING</h1>}
        <br/>
        {results.map((val,id)=>{
            return (<div key={id} style={{backgroundColor:"#fca103",borderStyle:"solid"}}>
                <h4>{val.volumeInfo.title}</h4>
                <br/>
                {val.volumeInfo.authors&&val.volumeInfo.authors.map((val,id)=>{return <p key={id}>{val}</p>})}
                <br/>
                {val.searchInfo&&val.searchInfo.textSnippet}
            </div>)
        })}
    </div>)

}