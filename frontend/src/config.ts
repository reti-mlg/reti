
export namespace Config{
    export const SERVER = "http://localhost:8080"

    export const LOGIN_ENDPOINT = "/login"
    export const REGISTER_ENDPOINT = "/register"

    export const FB_LOGIN_ENDPOINT = "/fbLogin"
    export const FB_CONNECT_ENDPOINT = "/fbConnect"
    export const FB_DISCONNECT_ENDPOINT = "/fbDisconnect"

    export const TWITTER_TOKEN = "/twitterToken"
    export const TWITTER_LOGIN = "/twitterLogin"
    export const TWITTER_DISCONNECT = "/twitterDisconnect"
    export const TWITTER_CONNECCT = "/twitterConnect"

    export const IMAGES_DIR = "/static/"

    export const USER_ENDPOINT = "/user"

    export const CHAT_ENDPOINT = "/chat"
    export const CHAT_ROOM_ENDPOINT = "/chatrooms "

    export const AVATAR_UPD_ENDPOINT = "/avatar"

    export const FACEBOOK_APP_ID = "<FB APP IP>"
    export const FACEBOOK_REDIRECT = "http://localhost/fbLanding"
    export const FACEBOOK_CONNECT_REDIRECT = "http://localhost/fbLandingConnect"

    export const BOOK_GET_ENDPOINT = "/book"
    export const BOOK_QUERY_ENDPOINT = "/book"
    export const BOOK_PREF_ENDPOINT= "/pref"

    export const CLIENT_VERSION = "1.0"

    export const IS_HTTPS = ()=>SERVER.startsWith("https")

}
