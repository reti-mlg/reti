

export class AppError{
    constructor(public info:any){}
}

export class AuthError extends AppError{
    constructor(message="Invalid email or password"){
        super(message)
    }
}

export class NotLoggedError extends AppError{
    constructor(message="Not logged in"){
        super(message)
    }
}

export class NotFoundError extends AppError{
    constructor(message="Not found"){
        super(message)
    }
}

export class AlreadyExistsError extends AppError{
    constructor(message="Email already exists"){
        super(message)
    }
}