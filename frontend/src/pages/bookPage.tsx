import qs from "qs";
import React, { useEffect, useState } from "react";
import { Col, Container, Row, Image, Table } from "react-bootstrap";
import { useLocation } from "react-router";
import { PrefButton } from "../components/prefButton";
import { BookInfo, useBookService } from "../services/BookService";
import { PrefType, UserPublic, useUserService } from "../services/UserService";

interface FriendMap_t{
    [key:string]:UserPublic
}
const prefToString = (pref:PrefType)=>{
    if(pref === PrefType.none){
        return "Nessuno"
    }else if(pref === PrefType.interessato){
        return "Interessato"
    }else if(pref === PrefType.ho_letto){
        return "Ho letto"
    }else if (pref === PrefType.sto_leggendo){
        return "Sto leggendo"
    }else{
        return "INVALIDO"
    }
}

export const BookPage:React.FunctionComponent = ()=>{
    const [book,setBook] = useState<BookInfo>()
    const [pref,setPref] = useState<PrefType>(PrefType.none)
    const [friendMap,setFriendMap] =useState<FriendMap_t>({})


    const bookService = useBookService()
    const userServie = useUserService()

    const location = useLocation()
    const _qs = qs.parse(location.search,{ignoreQueryPrefix:true})
    const id = _qs.id as string

    const updatePref = (nPref:PrefType) => {
        bookService.setPref(id,nPref).then(val=>{
            setPref(val.type)
        })
    }


    useEffect(()=>{
        bookService.getBook(id).then(book=>{
            setBook(book)
            if(book.ownPref){
                setPref(book.ownPref.type)
            }else{
                setPref(PrefType.none)
            }
            const t = book.friendPrefs.map(pref=>userServie.getUser(pref.userId))
            Promise.all(t).then(vals=>{
                const ret:FriendMap_t = {}
                for(let val of vals){
                    ret[val.id] = val
                }
                console.log("FriendMap:",ret)
                console.log("Book:",book)
                setFriendMap(ret)
            })
        })
    },[id,bookService.getState()])

    return (!book)?<h1>LOADING</h1>:(
        <Container>
            <Row>
                <Col>
                    <h1>{book.book.volumeInfo.title}</h1>
                    <br/>
                    Di {book.book.volumeInfo.authors&&book.book.volumeInfo.authors.map((auth,id)=><span key={id}>{auth + " "}</span>)}
                    · {book.book.volumeInfo.publishedDate}
                </Col>
                <Col>
                    {book.book.volumeInfo.imageLinks&&<Image src={book.book.volumeInfo.imageLinks.thumbnail}/>}
                </Col>
                <Col>
                    <PrefButton value={pref} onChange={updatePref}/>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Table size="sm">
                        <tbody>
                            <tr>
                                <td>
                                    ISBN: {book.book.volumeInfo.industryIdentifiers[0].identifier}
                                </td>
                                <td>
                                    Numero di pagine: {book.book.volumeInfo.pageCount}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Publicazione: {book.book.volumeInfo.publishedDate}
                                </td>
                                <td>
                                    Lingua: {book.book.volumeInfo.language}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Editore: {book.book.volumeInfo.publisher}
                                </td>
                                <td>
                                    Autori: {book.book.volumeInfo.authors&&book.book.volumeInfo.authors.map((auth,id)=><span key={id}>{auth+" "}</span>)}
                                </td>
                            </tr>
                        </tbody>
                    </Table>
                </Col>
            </Row>
            <Row>
                {book.book.volumeInfo.description&&<p>{book.book.volumeInfo.description}</p>}
            </Row>
            <Row>
                <h4>Amici che hanno aggiunto questo libro ai preferiti</h4>
                {book.friendPrefs.filter(pref=>friendMap[pref.userId]!==undefined).map((pref,id)=>(<div key={id}>
                    {friendMap[pref.userId].friendly_name}: {prefToString(pref.type)}

                </div>))}
            </Row>
        </Container>
    )
}