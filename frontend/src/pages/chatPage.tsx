import React, { useEffect, useState } from 'react'
import { useUserService} from '../services/UserService'
import { useChatService } from '../services/ChatService'
import { Config } from '../config';


const listen = (socket:any, fName:string) => {
    console.log(socket);
    socket.onmessage = function (evt:any) {
        var output = document.getElementById("output");

        var received_msg = JSON.parse(evt.data);
        console.log(received_msg);

        const div = document.createElement("div");
        //if (received_msg.userName === fName) div.style.color = "red";

        div.innerHTML = received_msg.userName + ": " + received_msg.text;

        if(output) output.appendChild(div);
    };
};

const waitForOpenSocket = (socket:any) => {
    return new Promise<void>((resolve) => {
        if (socket.readyState !== socket.OPEN) {
            socket.onopen = function () {
                resolve();
            };
        } else {
            resolve();
        }
    });
};

const sendMessage = async (socket:any, msg:string) => {
    if (socket.readyState !== socket.OPEN) {
        try {
            await waitForOpenSocket(socket);
            socket.send(msg);
        } catch (err) {
            console.error(err);
        }
    } else {
        socket.send(msg);
    }
};

const listenOnSocket = async (socket:any) => {
    if (socket.readyState !== socket.OPEN) {
        try {
            await waitForOpenSocket(socket);
            return(socket);
        } catch (err) {
            console.error(err);
        }
    } else {
        return(null);
    }
};





export const ChatPage:React.FunctionComponent = (props)=>{
    const [fName,setFName] = useState("")
    const [section,setSection] = useState("")
    const [ws, setWs] = useState<any>()
    const [Connected, setConnected] = useState(false)
    //const [ReceivedSection, setReceivedSection] = useState(false) 
    const [ChatRoom, setChatRoom] = useState("")
    //const [ChatRoomReceived, setChatRoomReceived] = useState(false)

    const userService = useUserService()
    const chatService = useChatService()




    //get user
    useEffect(()=>{
        userService.getSelf().then(me=>{
            setFName(me.friendly_name || "No name")
        }).catch((err)=>console.error(err))  

        chatService.getChatRooms().then((res:String)=>{
            //setChatRoomReceived(true)
            var output = document.getElementById("chatrooms");
            if(output)output.innerHTML="";

            var receivedChatrooms=  res.split(",")
            setChatRoom(receivedChatrooms[0])

            receivedChatrooms.forEach((item)=>{
                
                const option = document.createElement("option");
                        
                option.innerHTML =  item;
                option.value =  item;
        
                if(output) output.appendChild(option);
            })
            
        })
    },[userService.getState(),chatService.getState()])

    //get chatrooms
    /* useEffect(()=>{
        
    },[]) */


    //get section
    useEffect(()=>{        
        if(ChatRoom!==""){
            if(ws){
                ws.close()
                setConnected(false)
                setWs(null)
                var output = document.getElementById("output");
                if(output) output.innerHTML="";
            }
            chatService.getSection(ChatRoom).then((val:String)=>{
                console.log(val)
                setSection(val.toString())                
                //setReceivedSection(true)
                
            }).catch((err)=>console.error(err))
        }

        
                
    // eslint-disable-next-line
    },[ChatRoom])


    //connect to websocket
    useEffect(()=>{
        if(!Connected && section!==""){
            console.log(section)
            if(!ws)setWs(new WebSocket((Config.IS_HTTPS()?"wss://":"ws://").concat(section)))
            if(ws?.OPEN)
                listenOnSocket(ws).then((ws)=>{
                listen(ws,fName)
                setConnected(true)
                
            }).catch((err)=>console.error(err))    
        }
    },[section, ws, fName, Connected])


    

    window.onbeforeunload = function (event:any) {
        if(ws)ws.close();
        //wst.close();
    };

    const handleSubmit = (event:any) => {
        event.preventDefault();
        //console.log(event)
        var msg:string=""
        if(event){
            msg = event.target[0].value || "hello";
            console.log(msg);
            //listenOnSocket(ws)
            WebSocketTest(msg);
            event.target[0].value = "";
        }
    };
    
    const handleChatChange = (event:any) => {
        //event.preventDefault();

        const chatrm = event.target.selectedOptions[0].value;
        console.log(chatrm);
        setChatRoom(chatrm)
        //setReceivedSection(false)   
        setSection("")     
        
    };

    const WebSocketTest = (messageToSend:any) => {
        if (ws != null) {
            var msg = {
                text: messageToSend,
                userName: fName,
            };
            if (msg != null) {
                var msgToSend = JSON.stringify(msg);
                //ws.send(msgToSend);
                sendMessage(ws, msgToSend);
            }
        }
    };


    return (          		
        <div>
            
            <div
                id="chat-window"
                style={{
                    height: "100px",
                    overflowY: "scroll",
                    display: "flex",
                    flexDirection: "column-reverse",
                    marginTop: "100px",
                }}>
                <div id="output"></div>
            </div>
            
            <form onSubmit={handleSubmit}>
                <input type="text" id="message" name="message"/>                    
                <input type="submit" value="Send"/>
            </form> 
            
                <select id="chatrooms" onChange={handleChatChange}>
                
                </select>
            
        </div>
    )
}