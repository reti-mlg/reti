import React, { useEffect } from "react";
import { useHistory, useLocation } from "react-router";
import qs from 'qs'
import { useUserService } from "../services/UserService";


export const FBConnectLanding:React.FunctionComponent = (props) => {
    const location = useLocation()
    const history = useHistory()
    const userSerivce = useUserService()
    const code = qs.parse(location.search,{ignoreQueryPrefix:true}).code as string

    
    useEffect(()=>{
        userSerivce.connectFB(code).then(()=>{
            history.push("/")
        }).catch((err)=>{
            console.log(err)
            alert("Unexpected error")
        })
    // eslint-disable-next-line
    },[])

    return (<p>Logging in...</p>)
}