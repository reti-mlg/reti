import React, { useEffect } from "react";
import { useHistory, useLocation } from "react-router";
import qs from 'qs'
import { useAuthService } from "../services/AuthService";


export const FBLanding:React.FunctionComponent = (props) => {
    const location = useLocation()
    const history = useHistory()
    const authService = useAuthService()
    const code = qs.parse(location.search,{ignoreQueryPrefix:true}).code as string

    
    useEffect(()=>{
        authService.fbLogin(code).then(()=>{
            history.push("/")
        }).catch((err)=>{
            console.log(err)
            alert("Unexpected error")
        })
    // eslint-disable-next-line
    },[])

    return (<p>Logging in...</p>)
}