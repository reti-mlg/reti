import React from 'react'
import { Link, Redirect } from 'react-router-dom'
import { useAuthService } from '../services/AuthService'
import { HomePage } from './homePage'
import { UserPage } from './userPage'

export const Home:React.FunctionComponent = (props)=>{
    const authService = useAuthService()
    return authService.isLogged()?(
        <HomePage/>
    ):(
        <Redirect to="/login"/>
    )
}