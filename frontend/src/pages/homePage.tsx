import qs from 'qs'
import React, { useEffect, useState } from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import { Route, Switch, useLocation } from 'react-router-dom'
import { AppBar } from '../components/appBar'
import { ChatBar } from '../components/chatBar'
import { FriendList } from '../components/firendInfo'
import { Config } from '../config'
import { useAuthService } from '../services/AuthService'
import { BookInfo, GAPI_Book, useBookService } from '../services/BookService'
import { Friend, Preferito, useUserService } from '../services/UserService'
import { BookPage } from './bookPage'
import { PersonalPage } from './presonalPage'
import { SearchPage } from './searchPage'

interface PrefWithBooks{
    pref:Preferito,
    book:GAPI_Book
}

export interface FriendWithPrefs{
    friend:Friend,
    prefs: PrefWithBooks[]
}


export const HomePage:React.FunctionComponent = ()=>{
    const [fName,setFName] = useState("")
    const [email,setEmail] = useState("")
    const [id,setUID] = useState("")
    const [hasFB,setHasFB] = useState(false)
    const [hasTwitter,setHasTwitter] = useState(false)
    const [icon,setIcon] = useState("")
    const [fPref,setFPref] = useState<FriendWithPrefs[]>([])

    const [friends,setFriends] = useState<Friend[]>([])

    const userService = useUserService()
    const authService = useAuthService()
    const bookService = useBookService()

    const location = useLocation()
    const _qs = qs.parse(location.search,{ignoreQueryPrefix:true})
    const query = _qs.q as string

    useEffect(()=>{
        userService.getSelf().then(me=>{
            setFName(me.friendly_name || "No name")
            setUID(me.id)
            setEmail(me.email)
            setHasFB(me.facebook_id != null)
            setHasTwitter(me.twitter_id != null)
            setFriends(me.friends)
            setIcon(me.icon || "")
            const t = me.friends.map(async firend=>{
                const books = await Promise.all(firend.user.prefs.map(pref=>bookService.getBook(pref.bookId)))
                return {
                    books,
                    firend
                }
            })
            Promise.all(t).then(info=>{
                const ret:FriendWithPrefs[] = []
                for(let firendBook of info){
                    const tMap:{[key:string]:Preferito} = {}
                    for(let pref of firendBook.firend.user.prefs){
                        tMap[pref.bookId] = pref
                    }
                    ret.push({
                        friend:firendBook.firend,
                        prefs:firendBook.books.map(book=>{
                            return {
                                pref: tMap[book.book.id],
                                book:book.book
                            }
                        })
                    })
                }
                setFPref(ret)
            })
            
        })
    // eslint-disable-next-line
    },[userService.getState()])
    
    let hPage:JSX.Element
    if(fName){
        hPage = (<>
            <AppBar userName={fName} userIcon={Config.SERVER + Config.IMAGES_DIR + icon} fromQuery={query&&query.length>0?query:""}/>
            
            <Switch>
                <Route path="/search">
                    <SearchPage/>
                </Route>
                <Route path="/book">
                    <BookPage/>
                </Route>
                <Route path="/userPage">
                    <PersonalPage/>
                </Route>
                <Route path="/" exact>
                    <Container>
                        <Row className="justify-content-md-center">
                            <Col xs lg="2">
                            </Col>
                            <Col md="auto"><h2>I tuoi amici</h2></Col>
                            <Col xs lg="2">
                            </Col>
                        </Row>
                    </Container>
                    <FriendList firends = {fPref}/>
                </Route>
                
            </Switch>
            <ChatBar/>
        </>)
    }else{
        hPage = <h1>Loading</h1>
    }
     
    
    return hPage
}