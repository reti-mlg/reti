import React, { useState } from 'react'
import { useHistory } from 'react-router'
import { Link } from 'react-router-dom'
import { Config } from '../config'
import { AppError, AuthError } from '../errors/appError'
import { useAuthService } from '../services/AuthService'
import { createUpdaterFunc } from '../utils'

/**
 * @deprecated Use LoginPage
 */
export const Login:React.FunctionComponent = (props)=>{
    const authService = useAuthService()
    const history = useHistory()

    const [email,setEmail] = useState("")
    const [password,setPassword] = useState("")

    const updEmail = createUpdaterFunc(setEmail)
    const updPassword = createUpdaterFunc(setPassword)

    const doLogin = ()=>{
        authService.login(email,password).then(val=>{
            alert("Login ok")
            history.push("/")
        }).catch(err=>{
            if(err instanceof AuthError){
                alert("Invalid username or password")
                return;
            }
            if(err instanceof AppError){
                console.log(err)
                alert("Errore")
                return;
            }
            throw err;
        })
    }
    const facebookLoginUrl = `https://www.facebook.com/v10.0/dialog/oauth?client_id=${Config.FACEBOOK_APP_ID}&redirect_uri=${encodeURI(Config.FACEBOOK_REDIRECT)}&scope=email,user_friends`

    return (
        <div>
            <form>
                <input type="email" onChange={updEmail} value={email}/>
                <input type="password" onChange={updPassword} value={password}/>
            </form>
            <button onClick={doLogin}>Login</button>
            <a href={facebookLoginUrl}>Login with facebook</a>
            <Link to="/twitterLogin">Login with twitter </Link>
        </div>
    )
}