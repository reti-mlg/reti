import React,{useState} from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import './Login.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Config } from '../config';
import { useAuthService } from '../services/AuthService';
import { Link, useHistory } from 'react-router-dom';
import { AppError, AuthError } from '../errors/appError';

export const LoginPage: React.FunctionComponent = ()=>{
    const [password, setPassword] = useState("")
    const [email, setEmail] = useState("")

    const authService = useAuthService()
    const history = useHistory()

    const onSubmit= (event:React.FormEvent)=>{
        event.preventDefault()
        authService.login(email,password).then(val=>{
            alert("Login ok")
            history.push("/")
        }).catch(err=>{
            if(err instanceof AuthError){
                alert("Invalid username or password")
                return;
            }
            if(err instanceof AppError){
                console.log(err)
                alert("Errore")
                return;
            }
            throw err;
        })
    }

    const validateForm = ()=>password.length>0 && email.length>0
    
    const facebookLoginUrl = `https://www.facebook.com/v10.0/dialog/oauth?client_id=${Config.FACEBOOK_APP_ID}&redirect_uri=${encodeURI(Config.FACEBOOK_REDIRECT)}&scope=email,user_friends`

    return (
        <div className="Login">
          <Form onSubmit={onSubmit}>
            <Form.Group  controlId="email">
              <Form.Label>Email</Form.Label>
              <Form.Control
                autoFocus
                type="email"
                value={email}
                size="lg"
                onChange={(e) => setEmail(e.target.value)}
              />
            </Form.Group>
            <Form.Group  controlId="password">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                value={password}
                size="lg"
                onChange={(e) => setPassword(e.target.value)}
              />
            </Form.Group>
            <Button size="lg" type="submit" disabled={!validateForm()}>
              Login
            </Button>
            <a className="btn btn-lg btn-social btn-facebook" href={facebookLoginUrl}>
                Accedi con Facebook
            </a>
            <a className="btn btn-lg btn-social btn-twitter" href="/twitterLogin">
                Accedi con Twitter
            </a>
            <br/>
            <Link to="/register">Registrati con email e password</Link>
          </Form>
        </div>
      );
}