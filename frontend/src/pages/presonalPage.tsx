import React, { useEffect, useState } from 'react'
import { Button, Col, Container, Form, Row,Image, ListGroup } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { AvatarUploadForm } from '../components/avatarUpload'
import { PrefButton } from '../components/prefButton'
import { Config } from '../config'
import { useAuthService } from '../services/AuthService'
import { BookInfo, useBookService } from '../services/BookService'
import { PrefType, useUserService} from '../services/UserService'

const prefToString = (pref:PrefType)=>{
    if(pref === PrefType.none){
        return "Nessuno"
    }else if(pref === PrefType.interessato){
        return "Interessato"
    }else if(pref === PrefType.ho_letto){
        return "Ho letto"
    }else if (pref === PrefType.sto_leggendo){
        return "Sto leggendo"
    }else{
        return "INVALIDO"
    }
}

export const PersonalPage:React.FunctionComponent = (props)=>{
    const [fName,setFName] = useState("")
    const [email,setEmail] = useState("")
    const [id,setUID] = useState("")
    const [hasFB,setHasFB] = useState(false)
    const [hasTwitter,setHasTwitter] = useState(false)
    const [icon,setIcon] = useState("")
    const [prefs,setPrefs] = useState<BookInfo[]>([])

    const [editName,setEditName] = useState(false)

    const userService = useUserService()
    const authService = useAuthService()
    const bookService = useBookService()

    const disconnectFB = ()=>{
        userService.disconnectFB().then(()=>{
            userService.getSelf().then(me=>{
                setFName(me.friendly_name || "No name")
                setUID(me.id)
                setEmail(me.email)
                setHasFB(me.facebook_id != null)
                setHasTwitter(me.twitter_id != null)
                setIcon(me.icon || "")
                const t = me.prefs.map(pref=>{
                    return bookService.getBook(pref.bookId)
                })
                Promise.all(t).then(vals=>{
                    console.log("Vals:",vals)
                    setPrefs(vals)
                })
            })
        })
    }

    const disconnectTwitter = ()=>{
        userService.disconnectTwitter().then(()=>{
            userService.getSelf().then(me=>{
                setFName(me.friendly_name || "No name")
                setUID(me.id)
                setEmail(me.email)
                setHasFB(me.facebook_id != null)
                setHasTwitter(me.twitter_id != null)
                setIcon(me.icon || "")
                const t = me.prefs.map(pref=>{
                    return bookService.getBook(pref.bookId)
                })
                Promise.all(t).then(vals=>{
                    console.log("Vals:",vals)
                    setPrefs(vals)
                })
            })
        })
    }

    useEffect(()=>{
        userService.getSelf().then(me=>{
            setFName(me.friendly_name || "No name")
            setUID(me.id)
            setEmail(me.email)
            setHasFB(me.facebook_id != null)
            setHasTwitter(me.twitter_id != null)
            setIcon(me.icon || "")
            const t = me.prefs.map(pref=>{
                return bookService.getBook(pref.bookId)
            })
            Promise.all(t).then(vals=>{
                console.log("Vals:",vals)
                setPrefs(vals)
            })
        })
    // eslint-disable-next-line
    },[userService.getState()])

    const logout = ()=>{
        authService.logout()
    }

    const saveName = ()=>{
        if(!editName){
            return
        }
        setEditName(false)
        userService.updateFName(fName).then(val=>{
            userService.getSelf().then(me=>{
                setFName(me.friendly_name || "No name")
                setUID(me.id)
                setEmail(me.email)
                setHasFB(me.facebook_id != null)
                setHasTwitter(me.twitter_id != null)
                setIcon(me.icon || "")
                const t = me.prefs.map(pref=>{
                    return bookService.getBook(pref.bookId)
                })
                Promise.all(t).then(vals=>{
                    console.log("Vals:",vals)
                    setPrefs(vals)
                })
            })
        })
    }

    const goEditName = ()=>{
        setEditName(true)
    }

    const facebookConnectUrl = `https://www.facebook.com/v10.0/dialog/oauth?client_id=${Config.FACEBOOK_APP_ID}&redirect_uri=${encodeURI(Config.FACEBOOK_CONNECT_REDIRECT)}&scope=email,user_friends`

    let nameComponent
    let buttonComponent
    if(editName){
        nameComponent = (<Form.Control
            type="text"
            value={fName}
            size="lg"
            onChange={(e) => setFName(e.target.value)}
        />)

        buttonComponent = <Button onClick={saveName}>Salva</Button>
    }else{
        nameComponent = <span>{fName}</span>
        buttonComponent = <Button onClick={goEditName}>Modifica</Button>
    }

    const getEditPref = (bookId:string)=>{
        return ((nPref:PrefType)=>{
            bookService.setPref(bookId,nPref).then(val=>{
                const newPrefs = prefs.slice(0)
                for(let i = 0; i < newPrefs.length; i++){
                    if(newPrefs[i].book.id === bookId){
                        newPrefs[i].ownPref = val
                        setPrefs(newPrefs)
                        return;
                    }
                }
            })
        })
    }



    return (<Container>
        <Row>
            <Col>
            {icon.length>0?<img src={Config.SERVER + Config.IMAGES_DIR + icon} alt="AO" className="avatar" style={{maxWidth:"250px"}}/>:null}
            <br/>
            Modifica avatar <AvatarUploadForm/>
            </Col>
            <Col>
                Email: {email}
                <br/>
                Name: {nameComponent} {buttonComponent}
            </Col>
        </Row>
        <Row>
        {hasFB?<div>FB Connected <button onClick={disconnectFB}>Disconnect</button></div>:<a href={facebookConnectUrl}>Connect FB</a>}
        {hasTwitter?<div>Twitter Connected <button onClick={disconnectTwitter}>Disconnect</button></div>:<Link to="/twitterConnect">Connect Twitter</Link>}
        </Row>
        <Row>
            <Col>
                <ListGroup horizontal>
                    {prefs.map((pref,id)=>(<ListGroup.Item key={id}>
                        {pref.book.volumeInfo.title}
                        <a href={`/book?id=${encodeURI(pref.book.id)}`} style={{color:"white",textDecoration:"none"}} className="stretched-link">{pref.book.volumeInfo.title}</a>
                        {pref.book.volumeInfo.imageLinks&&<Image
                            src={pref.book.volumeInfo.imageLinks.smallThumbnail}
                            width={50}
                        />}

                        <br/>
                        {<PrefButton value={pref.ownPref.type} onChange={getEditPref(pref.book.id)}/>}
                    </ListGroup.Item>))}
                </ListGroup>
            </Col>
        </Row>
        <Row>
            <Button onClick={logout} variant="danger">Logout</Button>
        </Row>
    </Container>)
}