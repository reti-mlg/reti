import React,{useState} from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import './Login.css'
import { useAuthService } from '../services/AuthService';
import { useHistory } from 'react-router-dom';
import { AlreadyExistsError, AppError, AuthError } from '../errors/appError';

export const RegisterPage: React.FunctionComponent = ()=>{
    const [password, setPassword] = useState("")
    const [passwordConfim, setPasswordConfim] = useState("")
    const [fName, setFName] = useState("")
    const [email, setEmail] = useState("")

    const authService = useAuthService()
    const history = useHistory()

    const onSubmit= (event:React.FormEvent)=>{
        event.preventDefault()
        authService.register(email,password,fName).then(val=>{
            alert("Register ok")
            history.push("/")
        }).catch(err=>{
            if(err instanceof AlreadyExistsError){
                alert("Email già presente")
                return
            }else if(err instanceof AppError){
                console.log(err)
                alert("Errore")
                return;
            }
            throw err;
        })
    }

    const validateForm = ()=>password.length>0 && email.length>0 && passwordConfim.length > 0 && fName.length >0 && password === passwordConfim
    

    return (
        <div className="Login">
          <Form onSubmit={onSubmit}>
            <Form.Group  controlId="email">
              <Form.Label>Email</Form.Label>
              <Form.Control
                autoFocus
                type="email"
                value={email}
                size="lg"
                onChange={(e) => setEmail(e.target.value)}
              />
            </Form.Group>
            <Form.Group  controlId="password">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                value={password}
                size="lg"
                onChange={(e) => setPassword(e.target.value)}
              />
            </Form.Group>
            <Form.Group  controlId="passwordConf">
              <Form.Label>Conferma password</Form.Label>
              <Form.Control
                type="password"
                value={passwordConfim}
                size="lg"
                onChange={(e) => setPasswordConfim(e.target.value)}
              />
            </Form.Group>
            <Form.Group  controlId="name">
              <Form.Label>Nome</Form.Label>
              <Form.Control
                type="text"
                value={fName}
                size="lg"
                onChange={(e) => setFName(e.target.value)}
              />
            </Form.Group>
            <Button size="lg" type="submit" disabled={!validateForm()}>
              Crea utente
            </Button>
          </Form>
        </div>
      );
}