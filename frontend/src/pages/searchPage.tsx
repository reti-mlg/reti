import qs from "qs";
import React, { useEffect, useState } from "react";
import { Card, Col, Container, Row } from "react-bootstrap";
import { Redirect, useLocation } from "react-router";
import { GAPI_Book, useBookService } from "../services/BookService";

export const SearchPage:React.FunctionComponent = ()=>{
    const [books,setBooks] = useState<GAPI_Book[]>([])
    const [loading,setLoading] = useState(true)

    const location = useLocation()
    const _qs = qs.parse(location.search,{ignoreQueryPrefix:true})
    const query = _qs.q as string

    

    const bookService = useBookService()
    


    useEffect(()=>{
        setLoading(true)
        bookService.doQuery(query).then(books=>{
            setLoading(false)
            setBooks(books)
        })
    },[query,bookService.getState()])

    if(!query || query.length === 0){
        return <Redirect to="/"/>
    }

    return loading?<h2>LOADING</h2>:(
        <Container>
            <Row xs={1} md={2} className="g-4">
                {books.map((book,id)=>(
                    <Col key={id}>
                        <Card>
                            <Card.Img 
                                variant="top" 
                                style={{width:"150px"}}
                                src={book.volumeInfo.imageLinks?book.volumeInfo.imageLinks.thumbnail:"holder.js/100px160"}/>
                            <Card.Body>
                                <Card.Title>{book.volumeInfo.title}</Card.Title>
                                <Card.Text>
                                    {book.volumeInfo.authors&&<strong>{book.volumeInfo.authors.map((auth,id)=>(<span key={id}>{auth}</span>))}</strong>}
                                    {book.searchInfo&&<p>{book.searchInfo.textSnippet}</p>}
                                </Card.Text>
                                <a href={`/book?id=${encodeURI(book.id)}`} className="btn btn-primary stretched-link">Info</a>
                            </Card.Body>
                        </Card>
                    </Col>
                ))}
            </Row>
        </Container>
    )
}