import React, { useEffect } from "react";
import { useHistory, useLocation } from "react-router";
import qs from 'qs'
import { useUserService } from "../services/UserService";


export const TwitterLandingConnect:React.FunctionComponent = (props) => {
    const location = useLocation()
    const history = useHistory()
    const userService = useUserService()
    const _qs = qs.parse(location.search,{ignoreQueryPrefix:true})
    const token = _qs.oauth_token as string
    const verifier = _qs.oauth_verifier as string

    
    useEffect(()=>{
        userService.connectTwitter(token,verifier).then(()=>{
            history.push("/")
        }).catch((err)=>{
            console.log(err)
            alert("Unexpected error")
        })
    // eslint-disable-next-line
    },[])

    return (<p>Logging in...</p>)
}