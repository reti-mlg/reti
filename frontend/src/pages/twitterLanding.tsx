import React, { useEffect } from "react";
import { useHistory, useLocation } from "react-router";
import qs from 'qs'
import { useAuthService } from "../services/AuthService";


export const TwitterLanding:React.FunctionComponent = (props) => {
    const location = useLocation()
    const history = useHistory()
    const authService = useAuthService()
    const _qs = qs.parse(location.search,{ignoreQueryPrefix:true})
    const token = _qs.oauth_token as string
    const verifier = _qs.oauth_verifier as string

    
    useEffect(()=>{
        authService.twitterLogin(token,verifier).then(()=>{
            history.push("/")
        }).catch((err)=>{
            console.log(err)
            alert("Unexpected error")
        })
    // eslint-disable-next-line
    },[])

    return (<p>Logging in...</p>)
}