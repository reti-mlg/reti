import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { Config } from '../config'

interface tokenRes{
    token:string
}

interface TwitterLoginParams{
    isConnect:boolean
}

const TwitterLoginUrl = "https://api.twitter.com/oauth/authenticate"
export const TwitterLogin:React.FunctionComponent<TwitterLoginParams> = (props)=>{
    const [red,setRed] = useState("")
    const [error,setError] = useState(false)
    useEffect(()=>{
        let url
        if(props.isConnect){
            url = Config.SERVER+Config.TWITTER_TOKEN + "?connect=true"
        }else{
            url = Config.SERVER+Config.TWITTER_TOKEN
        }
        axios.request<tokenRes>({
            method:"GET",
            url,
            validateStatus:()=>true
        }).then(val=>{
            if(val.status !== 200){
                setError(true)
                return;
            }
            const token = val.data.token

            const loginUrl = `${TwitterLoginUrl}?oauth_token=${token}`
            setRed(loginUrl)
        })
    },[props.isConnect])
    if(error){
        return <h1>Error</h1>
    }
    if(red){
        window.location.href = red
        return <h1>Redirecting...</h1>
    }else{
        return <h1>Loading...</h1>
    }
}