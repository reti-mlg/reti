import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { AvatarUploadForm } from '../components/avatarUpload'
import { BookSearchBox } from '../components/searchBox'
import { Config } from '../config'
import { useAuthService } from '../services/AuthService'
import { BookInfo, useBookService } from '../services/BookService'
import { Friend, Preferito, useUserService } from '../services/UserService'
import './userPage.css';

/**
 * 
 * @deprecated Use HomePage
 */
export const UserPage:React.FunctionComponent = (props)=>{
    const [fName,setFName] = useState("")
    const [email,setEmail] = useState("")
    const [id,setUID] = useState("")
    const [hasFB,setHasFB] = useState(false)
    const [hasTwitter,setHasTwitter] = useState(false)
    const [icon,setIcon] = useState("")
    const [prefs,setPrefs] = useState<BookInfo[]>([])

    const [friends,setFriends] = useState<Friend[]>([])

    const userService = useUserService()
    const authService = useAuthService()
    const bookService = useBookService()

    const disconnectFB = ()=>{
        userService.disconnectFB().then(()=>{
            userService.getSelf().then(me=>{
                setFName(me.friendly_name || "No name")
                setUID(me.id)
                setEmail(me.email)
                setHasFB(me.facebook_id != null)
                setHasTwitter(me.twitter_id != null)
                setFriends(me.friends)
                setIcon(me.icon || "")
                const t = me.prefs.map(pref=>{
                    return bookService.getBook(pref.bookId)
                })
                Promise.all(t).then(vals=>{
                    console.log("Vals:",vals)
                    setPrefs(vals)
                })
            })
        })
    }

    const disconnectTwitter = ()=>{
        userService.disconnectTwitter().then(()=>{
            userService.getSelf().then(me=>{
                setFName(me.friendly_name || "No name")
                setUID(me.id)
                setEmail(me.email)
                setHasFB(me.facebook_id != null)
                setHasTwitter(me.twitter_id != null)
                setFriends(me.friends)
                setIcon(me.icon || "")
                const t = me.prefs.map(pref=>{
                    return bookService.getBook(pref.bookId)
                })
                Promise.all(t).then(vals=>{
                    console.log("Vals:",vals)
                    setPrefs(vals)
                })
            })
        })
    }

    useEffect(()=>{
        userService.getSelf().then(me=>{
            setFName(me.friendly_name || "No name")
            setUID(me.id)
            setEmail(me.email)
            setHasFB(me.facebook_id != null)
            setHasTwitter(me.twitter_id != null)
            setFriends(me.friends)
            setIcon(me.icon || "")
            const t = me.prefs.map(pref=>{
                return bookService.getBook(pref.bookId)
            })
            Promise.all(t).then(vals=>{
                console.log("Vals:",vals)
                setPrefs(vals)
            })
        })
    // eslint-disable-next-line
    },[userService.getState()])

    const prefTypeToText = (prefType:number)=>{
        if(prefType === -1){
            return "Non preferito, ao?"
        }else if(prefType === 0){
            return "Interessato"
        }else if(prefType === 1){
            return "Sto leggendo"
        }else if(prefType === 2){
            return "Ho letto"
        }else{
            return "Non valido, ao?"
        }
    }

    const logout = ()=>{
        authService.logout()
    }

    const facebookConnectUrl = `https://www.facebook.com/v10.0/dialog/oauth?client_id=${Config.FACEBOOK_APP_ID}&redirect_uri=${encodeURI(Config.FACEBOOK_CONNECT_REDIRECT)}&scope=email,user_friends`

    return (<div>
        Ciao: {fName}
        <br/>
        {icon.length>0?<img src={Config.SERVER + Config.IMAGES_DIR + icon} alt="AO" className="avatar"/>:null} 
        <br/>
        Email: {email}
        <br/>
        ID: {id}
        <br/>
        {hasFB?<div>FB Connected <button onClick={disconnectFB}>Disconnect</button></div>:<a href={facebookConnectUrl}>Connect FB</a>}
        {hasTwitter?<div>Twitter Connected <button onClick={disconnectTwitter}>Disconnect</button></div>:<Link to="/twitterConnect">Connect Twitter</Link>}
        <br/>
        Friends:
        {friends.map((f,id)=><div key={id}>{f.user.friendly_name}</div>)}
        <br/>
        <BookSearchBox/>
        <br/>
        {prefs.map((pf,id)=>(
            <div key={id}>
                <h4>{prefTypeToText(pf.ownPref.type)}</h4>
                <h3>{pf.book.volumeInfo.title}</h3>
                {pf.book.volumeInfo.imageLinks?<img src={pf.book.volumeInfo.imageLinks.smallThumbnail}/>:<></>}
            </div>))}
        <br/>
        <AvatarUploadForm/>
        <br/>
        <button onClick={logout}>Logout</button>
    </div>)
}