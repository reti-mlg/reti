import axios, { AxiosRequestConfig } from 'axios'
import jwt from 'jsonwebtoken'
import moment from 'moment'
import { Dispatch } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Config } from '../config'
import { AlreadyExistsError, AppError, AuthError, NotLoggedError } from '../errors/appError'

export interface TokenPayload{
    uid:string,
    email:string,
    friendly_name:string,
    iat:number,
    exp?:number
}

export interface AuthServiceState{
    logged:boolean,
    token?:string,
    uid?:string,
    email?:string,
    fName?:string,
    expiration?:Date
}

const AuthDefState:AuthServiceState = {
    logged:false
}

interface RegisterRequest{
    "email":string,
    "password":string,
    "friendly_name": string

}

const LOGIN = "LOGIN"
const LOGOUT = "LOGOUT"

interface LoginAction{
    type:typeof LOGIN,
    token:string,
    email:string,
    uid:string,
    fName:string,
    expire?:Date
}

interface LogoutAction{
    type:typeof LOGOUT
}

export type AuthActionTypes = LoginAction | LogoutAction

export const  useAuthService = () => {
    const dispatch = useDispatch()
    const state = useSelector((state:RootStateStub)=>state[AuthService.STATE_KEY])
    return new AuthService(state,dispatch)
}




interface LoginRequest{
    email:string,
    password:string
}
interface FBLoginRequest{
    code:string
}
interface LoginResponce{
    token:string
}

interface TwitterLoginRequest{
    token:string,
    verifier:string
}

export class AuthService{
    public static readonly STATE_KEY = "Auth_service"

    public static createLoginAction(token:string):LoginAction{
        const tPayload = jwt.decode(token) as TokenPayload
        let exp = null
        if(tPayload.exp){
            exp = moment(tPayload.exp*1000).toDate()
        }
        return {
            type:LOGIN,
            token,
            email:tPayload.email,
            fName:tPayload.friendly_name,
            uid:tPayload.uid,
            expire:exp || undefined
        }
    }

    public static createLogoutAction():LogoutAction{
        return {
            type:LOGOUT
        }
    }

    public static getReducer(){
        return (state=AuthDefState,action:AuthActionTypes) => {
            switch(action.type){
                case LOGIN:
                    return {
                        logged:true,
                        email:action.email,
                        uid:action.uid,
                        fName:action.fName,
                        token:action.token,
                        expiration:action.expire
                    } as AuthServiceState
                case LOGOUT:
                    return {logged:false} as AuthServiceState
                default:
                    return state
            }
        }
    }

    public constructor(private state:AuthServiceState,private dispatch:Dispatch<any>){}

    public getState(){return this.state}

    public isLogged(){
        if(this.state.expiration && moment().isAfter(this.state.expiration)){
            this.dispatch(AuthService.createLogoutAction())
            return false;
        }
        return this.state.logged
    }

    public authRequest<T>(config:AxiosRequestConfig){
        if(!this.isLogged()){
            throw new NotLoggedError()
        }
        if(config.headers){
            config.headers["Authorization"] = "Bearer " + this.state.token
        }else{
            config.headers = {
                "Authorization": "Bearer " + this.state.token
            }
        }
        return axios.request<T>(config)
    }

    public authPOST<T>(url:string, data?:any){
        return this.authRequest<T>({
            method:"POST",
            url,
            headers:{
                "Content-type":"application/json"
            },
            data:JSON.stringify(data),
            validateStatus:()=>true
        })
    }

    public authGET<T>(url:string){
        return this.authRequest<T>({
            method:"GET",
            url,
            validateStatus:()=>true
        })
    }

    public async fbLogin(code:string){
        const _body:FBLoginRequest = {code}
        const ret = await axios.request<LoginResponce>({
            method:"POST",
            url:Config.SERVER + Config.FB_LOGIN_ENDPOINT,
            headers:{
                "Content-type":"application/json"
            },
            data: JSON.stringify(_body),
            validateStatus:()=>true
        })
        if(ret.status === 200){
            const token = ret.data.token
            const loginAction = AuthService.createLoginAction(token)
            this.dispatch(loginAction)
            return true
        }else{
            throw new AppError(ret)
        }
    }

    public async twitterLogin(token:string,verifier:string){
        const _body:TwitterLoginRequest = {
            token,verifier
        }
        const ret = await axios.request({
            method:"POST",
            url:Config.SERVER + Config.TWITTER_LOGIN,
            headers:{
                "Content-type":"application/json"
            },
            data:JSON.stringify(_body),
            validateStatus:()=>true
        })
        if(ret.status === 200){
            const token = ret.data.token
            const loginAction = AuthService.createLoginAction(token)
            this.dispatch(loginAction)
            return true
        }else{
            throw new AppError(ret)
        }
        
    }

    public async register(email:string, password:string, name:string){
        const _body:RegisterRequest = {
            email,
            password,
            friendly_name:name
        }

        const ret = await axios.request<LoginResponce>({
            method:"POST",
            url:Config.SERVER + Config.REGISTER_ENDPOINT,
            headers:{
                "Content-type":"application/json"
            },
            data:JSON.stringify(_body),
            validateStatus:()=>true
        })
        if(ret.status === 201){
            const token = ret.data.token
            const loginAction = AuthService.createLoginAction(token)
            this.dispatch(loginAction)
            return true
        }else if(ret.status === 409){
            throw new AlreadyExistsError()
        }else{
            throw new AppError(ret)
        }
    }


    public async login(email:string, password:string){
        const _body:LoginRequest ={
            email,
            password
        }
        const ret = await axios.request<LoginResponce>({
            method:"POST",
            url:Config.SERVER + Config.LOGIN_ENDPOINT,
            headers:{
                "Content-type":"application/json"
            },
            data: JSON.stringify(_body),
            validateStatus:()=>true
        })
        if(ret.status === 200){
            const token = ret.data.token
            const loginAction = AuthService.createLoginAction(token)
            this.dispatch(loginAction)
            return true
        }else if(ret.status === 401){
            throw new AuthError()
        }else{
            throw new AppError(ret)
        }
    }

    public logout(){
        this.dispatch(AuthService.createLogoutAction())
    }
}

interface RootStateStub{
    [AuthService.STATE_KEY]:AuthServiceState
}
