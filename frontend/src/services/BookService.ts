import { Config } from "../config";
import { AppError, NotFoundError } from "../errors/appError";
import { AuthService, useAuthService } from "./AuthService";
import { Preferito, PrefType } from "./UserService";


interface GAPI_Inds_ids{
    type:string,
    identifier:string
}

export interface GAPI_Book{
    kind:string,
    id:string,
    etag:string,
    selfLink:string,
    searchInfo:{
        textSnippet:string
    },
    volumeInfo:{
        title:string,
        subtitle:string,
        authors:string[],
        publisher:string,
        publishedDate:string,
        description:string,
        industryIdentifiers:GAPI_Inds_ids[],
        readingModes:{
            text:boolean,
            image:boolean
        },
        pageCount:number,
        printType:string,
        categories:string[],
        averageRating:number,
        ratingsCount:number,
        maturityRating:string,
        allowAnonLogging:boolean,
        contentVersion:string,
        panelizationSummary: {
            containsEpubBubbles: false,
            containsImageBubbles: false
        },
        imageLinks: {
            smallThumbnail: string,
            thumbnail: string
        },
        language: string,
        previewLink: string,
        infoLink:string,
        canonicalVolumeLink: string
    }
}

interface GAPI_Query_Responce{
    kind:string,
    totalItems:number,
    items:GAPI_Book[]
}

export interface BookInfo{
    book:GAPI_Book,
    ownPref:Preferito,
    friendPrefs:Preferito[]
}

interface BookInfoResponce{
    result:BookInfo
}

interface BookQueryResponce{
    result:GAPI_Book[]
}

interface PrefSetResponce{
    result:Preferito
}

export const useBookService = ()=>{
    const authService = useAuthService()
    return new BookService(authService)
}


export class BookService{
    public constructor(private authService:AuthService){}
    public getState(){return this.authService.getState()}

    public async getBook(bookId:string){
        const res = await this.authService.authGET<BookInfoResponce>(Config.SERVER + Config.BOOK_GET_ENDPOINT + `/${encodeURI(bookId)}`)
        if(res.status === 404){
            throw new NotFoundError("Book not found")
        }else if(res.status === 200){
            return res.data.result
        }else{
            throw new AppError(res)
        }
    }

    public async doQuery(query:string){
        if(query.length < 4){
            return []
        }
        const res = await this.authService.authGET<BookQueryResponce>(Config.SERVER + Config.BOOK_QUERY_ENDPOINT + `?q=${encodeURI(query)}`)
        if(res.status === 200){
            return res.data.result
        }else{
            throw new AppError(res)
        }
    }

    public async setPref(bookId:string, pref:PrefType){
        const reqBody = {
            prefType:pref.valueOf()
        }
        const res = await this.authService.authPOST<PrefSetResponce>(
            Config.SERVER + `${Config.BOOK_PREF_ENDPOINT}/${encodeURI(bookId)}`,
            reqBody
        )
        if(res.status === 200){
            return res.data.result
        }else{
            throw new AppError(res)
        }
    }
}