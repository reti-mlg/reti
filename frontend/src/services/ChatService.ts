import { Config } from "../config";
import { AppError, NotFoundError } from "../errors/appError";
import { AuthService, useAuthService } from "./AuthService";


export const useChatService = ()=>{
    const authService = useAuthService()
    return new ChatService(authService)
}

export class ChatService{


    public constructor(private authService:AuthService){}

    public getState(){return this.authService.getState()}


    public async getSection(chatSection:string){
        const res = await this.authService.authGET<String>(Config.SERVER + Config.CHAT_ENDPOINT + `/${encodeURI(chatSection)}`)
        if(res.status === 404){
            throw new NotFoundError("Chat not found")
        }else if(res.status === 200){
            return res.data
        }else{
            throw new AppError(res)
        }
    }
    
    public async getChatRooms(){
        const res = await this.authService.authGET<String>(Config.SERVER + Config.CHAT_ROOM_ENDPOINT)
        if(res.status === 404){
            throw new NotFoundError("No chatromms found")
        }else if(res.status === 200){
            return res.data
        }else{
            throw new AppError(res)
        }
    }

}