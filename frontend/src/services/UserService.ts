import { Config } from "../config";
import { AppError, NotFoundError } from "../errors/appError";
import { AuthService, useAuthService } from "./AuthService";

export enum PrefType{
    none = -1,
    interessato = 0,
    sto_leggendo = 1,
    ho_letto = 2
}

export interface Preferito{
    userId:string
    bookId:string
    date:Date
    type:PrefType
}
export interface UserPublic{
    id:string,
    icon?:string,
    friendly_name?:string,
    facebook_id?:string,
    twitter_id?:string,
    prefs:Preferito[]
}

export enum FriendSource{
    facebook=1,
    twitter=2
}
export interface Friend{
    user:UserPublic,
    source:FriendSource
}

export interface UserPrivate extends UserPublic{
    email:string,
    friends: Friend[]
}


export const useUserService = ()=>{
    const authService = useAuthService()
    return new UserService(authService)
}


interface FBLoginRequest{
    code:string
}

interface TwitterLoginRequest{
    token:string,
    verifier:string
}

export class UserService{


    public constructor(private authService:AuthService){}

    public getState(){return this.authService.getState()}

    public async disconnectTwitter(){
        if(!this.authService.isLogged()){
            throw new AppError("Not logged in")
        }
        const ret = await this.authService.authPOST(Config.SERVER + Config.TWITTER_DISCONNECT)
        if(ret.status === 200){
            return true
        }else{
            throw new AppError(ret)
        }
    }

    public async connectTwitter(token:string,verifier:string){
        if(!this.authService.isLogged()){
            throw new AppError("Not logged in")
        }
        const _body:TwitterLoginRequest = {token,verifier}
        const ret = await this.authService.authPOST(Config.SERVER + Config.TWITTER_CONNECCT,_body)
        if(ret.status === 200){
            return true
        }else{
            throw new AppError(ret)
        }
    }

    public async disconnectFB(){
        if(!this.authService.isLogged()){
            throw new AppError("Not logged in")
        }
        const ret = await this.authService.authPOST(Config.SERVER + Config.FB_DISCONNECT_ENDPOINT)
        if(ret.status === 200){
            return true
        }else{
            throw new AppError(ret)
        }
    }

    public async connectFB(code:string){
        if(!this.authService.isLogged()){
            throw new AppError("Not logged in")
        }
        const _body:FBLoginRequest = {code}
        const ret = await this.authService.authPOST(Config.SERVER + Config.FB_CONNECT_ENDPOINT,_body)
        if(ret.status === 200){
            return true
        }else{
            throw new AppError(ret)
        }
    }


    public async getUser(userId:string){
        const res = await this.authService.authGET<UserPublic>(Config.SERVER + Config.USER_ENDPOINT + `/${encodeURI(userId)}`)
        if(res.status === 404){
            throw new NotFoundError("User not found")
        }else if(res.status === 200){
            return res.data
        }else{
            throw new AppError(res)
        }
    }

    public async getSelf(){
        const res = await this.authService.authGET<UserPrivate>(Config.SERVER + Config.USER_ENDPOINT)
        if(res.status === 404){
            throw new NotFoundError("User not found")
        }else if(res.status === 200){
            return res.data
        }else{
            throw new AppError(res)
        }
    }

    public async uploadAvatar(data:FormData){
        if(!this.authService.isLogged()){
            throw new AppError("Not logged in")
        }
        const ret = await this.authService.authRequest({
            method:"POST",
            url:Config.SERVER + Config.AVATAR_UPD_ENDPOINT,
            data
        })
        await this.getSelf()
        console.log(ret)
        return 0
    }

    public async updateFName(nname:string){
        const uid = this.authService.getState().uid
        if(!this.authService.isLogged() || !uid){
            throw new AppError("Not logged in")
        }

        const body = {
            friendly_name:nname
        }

        const ret = await this.authService.authRequest<UserPublic>({
            method:"PUT",
            url:Config.SERVER + Config.USER_ENDPOINT + `/${encodeURI(uid)}`,
            headers:{
                "Content-type":"application/json"
            },
            data:JSON.stringify(body)
        })
        if(ret.status === 200){
            return ret.data
        }else{
            throw new AppError(ret)
        }
    }
}