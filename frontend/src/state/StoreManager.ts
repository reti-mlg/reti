import { combineReducers, createStore } from "redux"
import { AuthService } from "../services/AuthService"
import storage from 'redux-persist/lib/storage'
import { persistReducer, persistStore } from "redux-persist"
import { composeWithDevTools } from "redux-devtools-extension"

export const getStore = (isDevelop=true) => {
    const rootReducer = combineReducers({
        [AuthService.STATE_KEY]:AuthService.getReducer()
    })

    const persistConfig = {
        key:"root",
        whitelist:[AuthService.STATE_KEY],
        storage
    }
    const reducer = persistReducer(persistConfig,rootReducer)
    const store = createStore(reducer,composeWithDevTools())
    const persistor = persistStore(store)
    return [store,persistor] as [typeof store, typeof persistor]
}