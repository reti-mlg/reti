export const createUpdaterFunc = (f:(s:string)=>void) => {
    return (t:React.ChangeEvent<HTMLInputElement>) => {f(t.target.value)}
}

export const cancellablePromise = (promise:Promise<any | void>) => {
    const isCancelled = {value:false}
    const wrappedPromise = new Promise((resolve,reject)=>{
        promise.then(d=>{
            return isCancelled.value? reject(isCancelled):resolve(d)
        }).catch(e=>{
            reject(isCancelled.value?isCancelled:e)
        })
    })

    return {
        promise:wrappedPromise,
        cancel: ()=>{
            isCancelled.value = true
        }
    }
}